package Model;
	
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.fxml.FXMLLoader;


public class CustomerModel extends Application {
	public static Stage customerStage = new Stage();
	public void start(Stage primaryStage) {
		try {
			VBox root = (VBox)FXMLLoader.load(getClass().getResource("/view/CustomerGUI.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/view/CustomerDesign.css").toExternalForm());
			scene.setCursor(Cursor.DEFAULT);
			primaryStage.setScene(scene);
			primaryStage.setTitle("CustomerDebtInformation");
			primaryStage.setResizable(false);
			primaryStage.initStyle(StageStyle.UNDECORATED);
			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			    @Override
			    public void handle(WindowEvent event) {
			        event.consume();
			    }
			});
			CustomerModel.customerStage = primaryStage;
			customerStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void closeCustomer()
	{
		customerStage.hide();
	}
	public void openCustomer()
	{
		customerStage.show();
	}
	
	
}
