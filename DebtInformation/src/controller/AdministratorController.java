package controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import Model.AdministratorModel;
import Model.LoginModel;
import Model.customer.Customer;
import Model.customer.CustomerDebt;
import Model.customer.CustomerList;
import database.SqliteConnection;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.StageStyle;

public class AdministratorController
{
	@FXML private AnchorPane loginPanel;
	@FXML private AnchorPane administratorloginPanel;
	@FXML private Button addNewCustomerButton;
	@FXML private Button administratorinformationbutton;
	@FXML private Button addnewcustomerbutton;
	@FXML private Button customerlistbutton;
	@FXML private Button  debtmanagementbutton;
	@FXML private Button  adminpaymenttransactionbutton;
	@FXML private Button  customerstatisticsbutton;
	@FXML private Button signoutbutton;
	@FXML private AnchorPane AdministratorInformationHeader;
	@FXML private AnchorPane AdministratorMenuTab;
	@FXML private AnchorPane AdminInformationPanel;
	@FXML private AnchorPane AddNewCustomerPanel;
	@FXML private AnchorPane  CustomerListPanel;
	@FXML private AnchorPane  DebtmanagementPanel;
	@FXML private AnchorPane  AdminPaymentTransactionPanel;
	@FXML private AnchorPane customerStatisticsPanel;
	@FXML private TableView<CustomerDebt>DebtManagementTable;
	@FXML private TableColumn DebtCustomerFirstnameColumn;
	@FXML private TableColumn DebtCustomerLastnameColumn;
	@FXML private TableColumn DebtCustomerTotalBalanceColumn;
	@FXML private TableView<CustomerList> customerlisttable;
	@FXML private TableColumn customerlistfirstnametablecolumn;
	@FXML private TableColumn customerlistlastnametablecolumn;
	@FXML private TableColumn customerlisttotalbalancetableview;
	@FXML private TextField customerfirstnametxtfield;
	@FXML private TextField customerlastnametxtfield;
	@FXML private TextField customerreligiontxtfield;
	@FXML private TextField customeraddresstxtfield;
	@FXML private TextField customeremailtxtfield;
	@FXML private TextField customercontactnumtxtfield;
	@FXML private TextField firstnamesearchtextfield;
	@FXML private TextField lastnamesearchtextfield;
	@FXML private TextField firstnamesearchtextfieldDebt;
	@FXML private TextField lastnamesearchtextfieldDebt;
	private Connection connection;
	private int counteradddatatable = 0;
	private String MissingInputforAddNewCustomer = "true";
	private String IdentityExistingforAddNewCustomer = "true";
	private String SuccesfullyAddedCustomer = "true";
	private FileInputStream fis;
	LoginModel login = new LoginModel();
	AdministratorModel admin = new AdministratorModel();
	Customer customer = new Customer();
	ObservableList<CustomerList> datalist = FXCollections.observableArrayList();
	ObservableList<CustomerDebt> datadebt = FXCollections.observableArrayList();
	public void AdministratorButtonClicked() 
	{
		 AdminInformationPanel.setVisible(true);
		 administratorinformationbutton.setDisable(true);
		 AddNewCustomerPanel.setVisible(false);
		 addnewcustomerbutton.setDisable(false);
		 CustomerListPanel.setVisible(false);
		 customerlistbutton.setDisable(false);
		 DebtmanagementPanel.setVisible(false);
		 debtmanagementbutton.setDisable(false);
		 AdminPaymentTransactionPanel.setVisible(false);
		 adminpaymenttransactionbutton.setDisable(false);
		 customerstatisticsbutton.setDisable(false);
		 connection = SqliteConnection.Connector();
    }
    
	public void AddNewCustomerMenuButtonClicked() 
	{
    	AdminInformationPanel.setVisible(false);
		administratorinformationbutton.setDisable(false);
		AddNewCustomerPanel.setVisible(true);
		addnewcustomerbutton.setDisable(true);
		CustomerListPanel.setVisible(false);
		customerlistbutton.setDisable(false);
		DebtmanagementPanel.setVisible(false);
		debtmanagementbutton.setDisable(false);
		AdminPaymentTransactionPanel.setVisible(false);
		adminpaymenttransactionbutton.setDisable(false);
		customerStatisticsPanel.setVisible(false);
		customerstatisticsbutton.setDisable(false);
		connection = SqliteConnection.Connector();
		NumbersInputValidationforAddNewCustomer();
		LettersInputValidationforAddNewCustomer();
    }
    
	public void AddNewCustomerButtonClicked()
	{
		CustomerMissingInputsValidation();
		CustomerIdentityExistingValidation();
		if(MissingInputforAddNewCustomer.equals("true"))
		{
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Missing Input");
			alert.setHeaderText(null);
			alert.setContentText("PLEASE COMPLETE ALL THE INFORMATION");
			alert.initStyle(StageStyle.UTILITY);
			alert.showAndWait();
		}
		else if(IdentityExistingforAddNewCustomer.equals("true"))
		{
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Existing Identity");
			alert.setHeaderText(null);
			alert.setContentText("NAME ALREADY EXIST");
			alert.initStyle(StageStyle.UTILITY);
			alert.showAndWait();
		}
		else
		{
			connection = SqliteConnection.Connector();
			customer.setFirstname(customerfirstnametxtfield.getText().toUpperCase());
			customer.setLastname(customerlastnametxtfield.getText().toUpperCase());
			customer.setReligion(customerreligiontxtfield.getText().toUpperCase());
			customer.setAddress(customeraddresstxtfield.getText().toUpperCase());
			customer.setEmail(customeremailtxtfield.getText());
			customer.setContactnumber(customercontactnumtxtfield.getText());
			
			
			try
				{
					String query = "INSERT INTO CUSTOMER(FIRSTNAME,LASTNAME,RELIGION,ADDRESS,EMAIL,CONTACTNUMBER) VALUES(?,?,?,?,?,?)";				
					PreparedStatement pst2 = connection.prepareStatement(query);
					
				    pst2.setString(1, customer.getFirstname());
				    pst2.setString(2, customer.getLastname());
				    pst2.setString(3, customer.getReligion());
				    pst2.setString(4, customer.getAddress());
				    pst2.setString(5, customer.getEmail());
				    pst2.setString(6, customer.getContactnumber());
				    
				    pst2.executeUpdate();
				    Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("ADD CUSTOMER SUCCESFUL");
					alert.setHeaderText(null);
					alert.setContentText("CUSTOMER SUCCESFULY ADDED");
					alert.initStyle(StageStyle.UTILITY);
					alert.showAndWait();
					SuccesfullyAddedCustomer = "true";
				    pst2.close();
				    connection.close();
				    
				}catch(SQLException e1)
				{
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("CUSTOMER ADD ERROR");
					alert.setHeaderText(null);
					alert.setContentText("THERE WAS AN ERROR ADDING THE CUSTOMER");
					alert.initStyle(StageStyle.UTILITY);
					alert.showAndWait();
					SuccesfullyAddedCustomer = "false";
				}
			if(SuccesfullyAddedCustomer.equals("true"))
			{
				GetCustomerIDofCreatedCustomer();
				ConvertCustomerIdtoQRCode();
				SetCustomerQRCode();
			}
			
		}
	}	
	
	public void CustomerListButtonClicked() 
	{
		AdminInformationPanel.setVisible(false);
		administratorinformationbutton.setDisable(false);
		AddNewCustomerPanel.setVisible(false);
		addnewcustomerbutton.setDisable(false);
		CustomerListPanel.setVisible(true);
		customerlistbutton.setDisable(true);
		DebtmanagementPanel.setVisible(false);
		debtmanagementbutton.setDisable(false);
		AdminPaymentTransactionPanel.setVisible(false);
		adminpaymenttransactionbutton.setDisable(false);
		customerStatisticsPanel.setVisible(false);
		customerstatisticsbutton.setDisable(false);
		connection = SqliteConnection.Connector();
		LettersInputValidationforCustomerList();
		customerlisttable.getItems().clear();
		customerlistfirstnametablecolumn.setCellValueFactory(new PropertyValueFactory<>("firstname"));
		customerlistlastnametablecolumn.setCellValueFactory(new PropertyValueFactory<>("lastname"));
		customerlisttotalbalancetableview.setCellValueFactory(new PropertyValueFactory<>("totalbalance"));
		
		try
		{
			String query1 = "SELECT FIRSTNAME,LASTNAME FROM CUSTOMER";
	
	
			PreparedStatement pst1 = connection.prepareStatement(query1);
			ResultSet rs1 = pst1.executeQuery();
			while(rs1.next()) 
			{
		          datalist.add(new CustomerList
		        		  (
		        			rs1.getString("FIRSTNAME"),
		        			rs1.getString("LASTNAME"), 0
		        		  ));
		        		
		          customerlisttable.setItems(datalist);	
		         
			}
			
			 pst1.close();
			 rs1.close();
			connection.close();
		}catch(SQLException e1)
		{
			e1.printStackTrace();
		}
	}
	
	public void DebtManagementButtonClicked() 
	{
		AdminInformationPanel.setVisible(false);
		administratorinformationbutton.setDisable(false);
		AddNewCustomerPanel.setVisible(false);
		addnewcustomerbutton.setDisable(false);
		CustomerListPanel.setVisible(false);
		customerlistbutton.setDisable(false);
		DebtmanagementPanel.setVisible(true);
		debtmanagementbutton.setDisable(true);
		AdminPaymentTransactionPanel.setVisible(false);
		adminpaymenttransactionbutton.setDisable(false);
		customerStatisticsPanel.setVisible(false);
		customerstatisticsbutton.setDisable(false);
		connection = SqliteConnection.Connector();
		LettersInputValidationforDebtManagement();
		DebtManagementTable.getItems().clear();
		DebtCustomerFirstnameColumn.setCellValueFactory(new PropertyValueFactory<>("firstname"));
		DebtCustomerLastnameColumn.setCellValueFactory(new PropertyValueFactory<>("lastname"));
		DebtCustomerTotalBalanceColumn.setCellValueFactory(new PropertyValueFactory<>("totalbalance"));
		
		try
		{
			String query1 = "SELECT FIRSTNAME,LASTNAME FROM CUSTOMER";
	
	
			PreparedStatement pst1 = connection.prepareStatement(query1);
			ResultSet rs1 = pst1.executeQuery();
			while(rs1.next()) 
			{
		          datadebt.add(new CustomerDebt
		        		  (
		        			rs1.getString("FIRSTNAME"),
		        			rs1.getString("LASTNAME"), 0
		        		  ));
		        		
		          DebtManagementTable.setItems(datadebt);	
		         
			}
			
			 pst1.close();
			 rs1.close();
			connection.close();
		}catch(SQLException e1)
		{
			e1.printStackTrace();
		}
	}
	
	public void AdminPaymentTransactionButtonClicked() 
	{
		AdminInformationPanel.setVisible(false);
		administratorinformationbutton.setDisable(false);
		AddNewCustomerPanel.setVisible(false);
		addnewcustomerbutton.setDisable(false);
		CustomerListPanel.setVisible(false);
		customerlistbutton.setDisable(false);
		DebtmanagementPanel.setVisible(false);
		debtmanagementbutton.setDisable(false);
		AdminPaymentTransactionPanel.setVisible(true);
		adminpaymenttransactionbutton.setDisable(true);
		customerStatisticsPanel.setVisible(false);
		customerstatisticsbutton.setDisable(false);
		connection = SqliteConnection.Connector();
	}
	
	public void CustomersGraphButtonClicked() 
	{
		AdminInformationPanel.setVisible(false);
		administratorinformationbutton.setDisable(false);
		AddNewCustomerPanel.setVisible(false);
		addnewcustomerbutton.setDisable(false);
		CustomerListPanel.setVisible(false);
		customerlistbutton.setDisable(false);
		DebtmanagementPanel.setVisible(false);
		debtmanagementbutton.setDisable(false);
		AdminPaymentTransactionPanel.setVisible(false);
		adminpaymenttransactionbutton.setDisable(false);
		customerStatisticsPanel.setVisible(true);
		customerstatisticsbutton.setDisable(true);
		connection = SqliteConnection.Connector();
	}
	
	public void signOutButtonClicked()
	{
	admin.closeAdmin();
	 login.openLogin();
	}
	
	private void NumbersInputValidationforAddNewCustomer() 
	{
		customercontactnumtxtfield.textProperty().addListener(new ChangeListener<String>() {
	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	        	 if(!newValue.matches("[0-9]*")){
	        		 customercontactnumtxtfield.setText(oldValue);
	             }
	        	 if (customercontactnumtxtfield.getText().length() > 11) {
	                 String s = customercontactnumtxtfield.getText().substring(0, 11);
	                 customercontactnumtxtfield.setText(s);
	             }
	        }
	    });
		
	}
	
	private void LettersInputValidationforAddNewCustomer() 
	{
		customerfirstnametxtfield.textProperty().addListener(new ChangeListener<String>() {
	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	        	 if(!newValue.matches("[A-Za-z\\s]*")){
	        		 customerfirstnametxtfield.setText(oldValue);
	             }
	        	 if (customerfirstnametxtfield.getText().length() > 20) {
	                 String s = customerfirstnametxtfield.getText().substring(0, 20);
	                 customerfirstnametxtfield.setText(s);
	             }
	        }
	    });
		customerlastnametxtfield.textProperty().addListener(new ChangeListener<String>() {
	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	        	 if(!newValue.matches("[A-Za-z\\s]*")){
	        		 customerlastnametxtfield.setText(oldValue);
	             }
	        	 if (customerlastnametxtfield.getText().length() > 20) {
	                 String s = customerlastnametxtfield.getText().substring(0, 20);
	                 customerlastnametxtfield.setText(s);
	             }
	        }
	    });
		customerreligiontxtfield.textProperty().addListener(new ChangeListener<String>() {
	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	        	 if(!newValue.matches("[A-Za-z\\s]*")){
	        		 customerreligiontxtfield.setText(oldValue);
	             }
	        	 if (customerreligiontxtfield.getText().length() > 15) {
	                 String s = customerreligiontxtfield.getText().substring(0, 15);
	                 customerreligiontxtfield.setText(s);
	             }
	        }
	    });
		customeraddresstxtfield.textProperty().addListener(new ChangeListener<String>() {
	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	        	 if(!newValue.matches("[A-Za-z.,\\s0-9]*")){
	        		 customeraddresstxtfield.setText(oldValue);
	             }
	        	 
	        	 if (customeraddresstxtfield.getText().length() > 50) {
	                 String s = customeraddresstxtfield.getText().substring(0, 50);
	                 customeraddresstxtfield.setText(s);
	             }
	        }
	    });
		customeremailtxtfield.textProperty().addListener(new ChangeListener<String>() {
	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	        	 if(!newValue.matches("[A-Za-z_.@\\s0-9]*")){
	        		 customeremailtxtfield.setText(oldValue);
	        	 }
	        }
	    });
	}
	
	private void LettersInputValidationforCustomerList() 
	{
		firstnamesearchtextfield.textProperty().addListener(new ChangeListener<String>() {
	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	        	 if(!newValue.matches("[A-Za-z\\s]*")){
	        		 firstnamesearchtextfield.setText(oldValue);
	             }
	        	 if (firstnamesearchtextfield.getText().length() > 20) {
	                 String s = firstnamesearchtextfield.getText().substring(0, 20);
	                 firstnamesearchtextfield.setText(s);
	             }
	        }
	    });
		lastnamesearchtextfield.textProperty().addListener(new ChangeListener<String>() {
	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	        	 if(!newValue.matches("[A-Za-z\\s]*")){
	        		 lastnamesearchtextfield.setText(oldValue);
	             }
	        	 if (lastnamesearchtextfield.getText().length() > 20) {
	                 String s = lastnamesearchtextfield.getText().substring(0, 20);
	                 lastnamesearchtextfield.setText(s);
	             }
	        }
	    });
		
	}
	
	private void LettersInputValidationforDebtManagement() 
	{
		firstnamesearchtextfieldDebt.textProperty().addListener(new ChangeListener<String>() {
	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	        	 if(!newValue.matches("[A-Za-z\\s]*")){
	        		 firstnamesearchtextfieldDebt.setText(oldValue);
	             }
	        	 if (firstnamesearchtextfieldDebt.getText().length() > 20) {
	                 String s = firstnamesearchtextfieldDebt.getText().substring(0, 20);
	                 firstnamesearchtextfieldDebt.setText(s);
	             }
	        }
	    });
		lastnamesearchtextfieldDebt.textProperty().addListener(new ChangeListener<String>() {
	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	        	 if(!newValue.matches("[A-Za-z\\s]*")){
	        		 lastnamesearchtextfieldDebt.setText(oldValue);
	             }
	        	 if (lastnamesearchtextfieldDebt.getText().length() > 20) {
	                 String s = lastnamesearchtextfieldDebt.getText().substring(0, 20);
	                 lastnamesearchtextfieldDebt.setText(s);
	             }
	        }
	    });
		
	}
	
	private void CustomerMissingInputsValidation() 
	{
		
		if(customerfirstnametxtfield.getText().length() == 0 && customerlastnametxtfield.getText().length() == 0 && customerreligiontxtfield.getText().length() == 0 && customeraddresstxtfield.getText().length() == 0 && customeremailtxtfield.getText().length() == 0 && customercontactnumtxtfield.getText().length() == 0)
		{
			MissingInputforAddNewCustomer = "true";
		}
		else if(customerfirstnametxtfield.getText().length() != 0 && customerlastnametxtfield.getText().length() != 0 && customerreligiontxtfield.getText().length() != 0 && customeraddresstxtfield.getText().length() != 0 && customeremailtxtfield.getText().length() != 0 && customercontactnumtxtfield.getText().length() != 0)
		{
			MissingInputforAddNewCustomer = "false";
		}
	}
	
	private void CustomerIdentityExistingValidation() 
	{
		connection = SqliteConnection.Connector();
		try
		{
			String query = "SELECT FIRSTNAME,LASTNAME FROM CUSTOMER where FIRSTNAME = '" + customerfirstnametxtfield.getText().toUpperCase() + "' AND LASTNAME = '" + customerlastnametxtfield.getText().toUpperCase() + "'" ;
			PreparedStatement pst2 = connection.prepareStatement(query);
			ResultSet rs2 = pst2.executeQuery();
			
			if(!rs2.next())
			{
				
				IdentityExistingforAddNewCustomer = "false";
	        }
			else
			{
				IdentityExistingforAddNewCustomer = "true";
			}
			 pst2.close();
			 connection.close();
		}catch(SQLException e1)
		{
			e1.printStackTrace();
		}
		
	}
	
	private void GetCustomerIDofCreatedCustomer() 
	{
		connection = SqliteConnection.Connector();
		try
		{
			String query = "SELECT CUSTOMERID FROM CUSTOMER WHERE FIRSTNAME = '" + customer.getFirstname() + "' AND LASTNAME = '" + customer.getLastname() + "'" ;
			PreparedStatement pst2 = connection.prepareStatement(query);
			ResultSet rs2 = pst2.executeQuery();
			
			while(rs2.next()) 
			{
		          int customerid = rs2.getInt("CUSTOMERID");
		          customer.setCustomerid(customerid);
			}
			
			 pst2.close();
			 connection.close();
		}catch(SQLException e1)
		{
			e1.printStackTrace();
		}
		
	}
	
	private void ConvertCustomerIdtoQRCode() 
	{
		String customerid = Integer.toString(customer.getCustomerid());
		File file = new File("C:\\CustomerQRCode");
		file.mkdirs();
		QRCodeWriter writer = new QRCodeWriter();
	    int width = 256, height = 256;
	    BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB); // create an empty image
	    int white = 255 << 16 | 255 << 8 | 255;
	    int black = 0;
	    try {
	        BitMatrix bitMatrix = writer.encode(customerid, BarcodeFormat.QR_CODE, width, height);
	        for (int i = 0; i < width; i++) {
	            for (int j = 0; j < height; j++) {
	                image.setRGB(i, j, bitMatrix.get(i, j) ? black : white); // set pixel one by one
	            }
	        }
	 
	        try {
	            ImageIO.write(image, "jpg", new File("C:\\CustomerQRCode\\QRCustomerCode.jpg")); // save QR image to disk
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	 
	    } catch (WriterException e) {
	        e.printStackTrace();
	    }
		
	}

	private void SetCustomerQRCode() 
	{
		connection = SqliteConnection.Connector();
		 try
			{
				String query = "INSERT INTO CUSTOMERQRCODE_INVOICE(CUSTOMERID,CUSTOMERQRCODE) VALUES(?,?)";				
				PreparedStatement pst2 = connection.prepareStatement(query);
			    pst2.setInt(1, customer.getCustomerid());
			    File f1 = new File("C:\\CustomerQRCode\\QRCustomerCode.jpg");
			    fis = new FileInputStream(f1);
			    pst2.setBinaryStream(2, (InputStream)fis, (int)f1.length());
			    pst2.executeUpdate();
			    pst2.close();
			    connection.close();
			}catch(SQLException | FileNotFoundException e1)
			{
				e1.printStackTrace();
			}
		 
	}

}
