package Model.customer;

public class CustomerDebt
{
	private String firstname;
	private String lastname;
	private int totalbalance;
	public CustomerDebt(String firstname, String lastname, int totalbalance) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.totalbalance = totalbalance;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public int getTotalbalance() {
		return totalbalance;
	}
	public void setTotalbalance(int totalbalance) {
		this.totalbalance = totalbalance;
	} 
}
