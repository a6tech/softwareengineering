package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

import Model.AdministratorModel;
import Model.CustomerModel;
import Model.LoginModel;
import Model.customer.Customer;
import Model.customer.CustomerDebtHistory;
import Model.customer.CustomerList;
import database.SqliteConnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class CustomerController 
{
	@FXML private Button accountinformationbutton;
	@FXML private Button paymenttransactionbutton;
	@FXML private Button paymenthistorybutton;
	@FXML private Button  debtinformationbutton;
	@FXML private Button UpdateInformation;
	@FXML private Button signoutbutton;
	@FXML private AnchorPane CustomerMenuTab;
	@FXML private AnchorPane AccountInformationPanel;
	@FXML private AnchorPane PaymentTransaction;
	@FXML private AnchorPane CustomerInformationHeader;
	@FXML private AnchorPane  PaymentHistory;
	@FXML private AnchorPane DebtInformation;
	@FXML private AnchorPane UpdateCustomerInformationPanel;
	@FXML private TableView<CustomerDebtHistory> DebtTableView;
	@FXML private TableColumn debtAmountTable;
	@FXML private TableColumn debtAmountDateAdded;
	@FXML private TableColumn debtAmountTimeAdded;
	@FXML private TableView PaymentTableView;
	@FXML private TableColumn paymentamount;
	@FXML private TableColumn paymentdate;
	@FXML private TableColumn paymenttime;
	@FXML private ImageView imagedatabase;
	@FXML private Label CustomerNameDisplay;
	@FXML private Label CustomerReligionDisplay;
	@FXML private Label CustomerAddressDisplay;
	@FXML private Label CustomerEmailDisplay;
	@FXML private Label CustomerContactNumberDisplay;
	@FXML private Label CustomerCurrentBalanceDisplay;
	@FXML private TextField customerfirstnametxtfieldupdate;
	@FXML private TextField customerlastnametxtfieldupdate;
	@FXML private TextField customerreligiontxtfieldupdate;
	@FXML private TextField customeraddresstxtfieldupdate;
	@FXML private TextField customeremailtxtfieldupdate;
	@FXML private TextField customercontactnumbertxtfieldupdate;
	private FileInputStream fis;
	private Connection connection;
	ObservableList<CustomerDebtHistory> datadebthistory = FXCollections.observableArrayList();
	Customer customer = new Customer();
	
	LoginModel login = new LoginModel();
	CustomerModel customerFrame = new CustomerModel();
	
	@FXML
	public void DebtInformationButtonClicked() 
	{
		 PaymentTransaction.setVisible(false);
		 paymenttransactionbutton.setDisable(false);
		 PaymentHistory.setVisible(false);
		 paymenthistorybutton.setDisable(false);
		 AccountInformationPanel.setVisible(false);
		 accountinformationbutton.setDisable(false);
		 DebtInformation.setVisible(true);
		 debtinformationbutton.setDisable(true);
		 connection = SqliteConnection.Connector();
		 debtAmountTable.setCellValueFactory(new PropertyValueFactory<>("debtamount"));
		 debtAmountDateAdded.setCellValueFactory(new PropertyValueFactory<>("dateadded"));
		 debtAmountTimeAdded.setCellValueFactory(new PropertyValueFactory<>("timeadded"));
		 try
			{
				String query1 = "SELECT DEBT_AMOUNT,DATE_ADDED,TIME_ADDED FROM DEBT WHERE CUSTOMERID = '1'";
		
		
				PreparedStatement pst1 = connection.prepareStatement(query1);
				ResultSet rs1 = pst1.executeQuery();
				while(rs1.next()) 
				{
					datadebthistory.add(new CustomerDebtHistory
			        		  (
			        			rs1.getInt("DEBT_AMOUNT"),
			        			rs1.getString("DATE_ADDED"), 
			        			rs1.getString("TIME_ADDED")
			        		  ));
			        		
					DebtTableView.setItems(datadebthistory);	
			         
				}
				
				 pst1.close();
				 rs1.close();
				connection.close();
			}catch(SQLException e1)
			{
				e1.printStackTrace();
			}
	}
    public void AccountInformationButtonClicked() 
	{
		
    	System.out.println("Accinfo selected");
    	PaymentTransaction.setVisible(false);
		 paymenttransactionbutton.setDisable(false);
		 PaymentHistory.setVisible(false);
		 paymenthistorybutton.setDisable(false);
		 AccountInformationPanel.setVisible(true);
		 accountinformationbutton.setDisable(true);
		 DebtInformation.setVisible(false);
		 debtinformationbutton.setDisable(false);
		 connection = SqliteConnection.Connector();
		 
		  try
			{
				String query1 = "SELECT FIRSTNAME,LASTNAME,RELIGION,ADDRESS,EMAIL,CONTACTNUMBER FROM Customer WHERE CUSTOMERID = " + "2";
				PreparedStatement pst1 = connection.prepareStatement(query1);
				ResultSet rs1 = pst1.executeQuery();
				
				while(rs1.next()) 
				{
					CustomerNameDisplay.setText(rs1.getString("FIRSTNAME") +" "+ rs1.getString("LASTNAME"));
					CustomerReligionDisplay.setText(rs1.getString("RELIGION"));
					CustomerAddressDisplay.setText(rs1.getString("ADDRESS"));
					CustomerEmailDisplay.setText(rs1.getString("EMAIL"));
					CustomerContactNumberDisplay.setText(rs1.getString("CONTACTNUMBER"));
				}
				 pst1.close();
				 rs1.close();
				connection.close();
			}catch(SQLException e1)
			{
				e1.printStackTrace();
			}
    }
	
	public void PaymentTransactionButtonClicked() 
	{
        System.out.println("paymenttransac selected");
        PaymentTransaction.setVisible(true);
		 paymenttransactionbutton.setDisable(true);
		 PaymentHistory.setVisible(false);
		 paymenthistorybutton.setDisable(false);
		 AccountInformationPanel.setVisible(false);
		 accountinformationbutton.setDisable(false);
		 DebtInformation.setVisible(false);
		 debtinformationbutton.setDisable(false);
	     connection = SqliteConnection.Connector();
	     try
			{
				String query1 = "SELECT CustomerQRCode FROM IMAGEQRCODE";
		
		
				PreparedStatement pst = connection.prepareStatement(query1);
				ResultSet resultset = pst.executeQuery();
				while(resultset.next()) 
				{
					InputStream input = resultset.getBinaryStream("CustomerQRCode");
	                InputStreamReader inputReader = new InputStreamReader(input);                        
	                if(inputReader.ready())
	                {
	                    File tempFile = new File("tempFile.jpg");

	                    FileOutputStream fos = new FileOutputStream(tempFile);
	                    byte[] buffer = new byte[1024];
	                    while(input.read(buffer) > 0){
	                        fos.write(buffer);                        
	                    }
	                    Image image = new Image(tempFile.toURI().toURL().toString());
	                    imagedatabase.setImage(image);
	                }
				}
				 pst.close();
				 resultset.close();
				 connection.close();
			}catch(SQLException | MalformedURLException e1)
			{
				e1.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	     
	}
	public void PaymentHistoryButtonClicked() 
	{
        System.out.println("payment history selected");
        PaymentTransaction.setVisible(false);
		 paymenttransactionbutton.setDisable(false);
		 PaymentHistory.setVisible(true);
		 paymenthistorybutton.setDisable(true);
		 AccountInformationPanel.setVisible(false);
		 accountinformationbutton.setDisable(false);
		 DebtInformation.setVisible(false);
		 debtinformationbutton.setDisable(false);
	     connection = SqliteConnection.Connector();
	     
	     
	     
	     try
			{
				String query = "INSERT INTO Will(Name,pic) VALUES(?,?)";				
				PreparedStatement pst2 = connection.prepareStatement(query);
			    pst2.setString(1, "jessa");
			    File f1 = new File("C:\\Users\\ZABALA\\Documents\\info.png");
			    fis = new FileInputStream(f1);
			    pst2.setBinaryStream(2, (InputStream)fis, (int)f1.length());
			    pst2.executeUpdate();
			    pst2.close();
			    connection.close();
			}catch(SQLException | FileNotFoundException e1)
			{
				e1.printStackTrace();
			}
	     
	}
	public void UpdateInformationButtonClicked()
	{
		AccountInformationPanel.setVisible(false);
		UpdateCustomerInformationPanel.setVisible(true);
	}
	
	public void updateCustomerInfoSubmitButtonClicked(){
		
		connection = SqliteConnection.Connector();
		CustomerIdQRCodeRead();
		customer.setFirstname(customerfirstnametxtfieldupdate.getText());
		customer.setLastname(customerlastnametxtfieldupdate.getText());
		customer.setReligion(customerreligiontxtfieldupdate.getText());
		customer.setAddress(customeraddresstxtfieldupdate.getText());
		customer.setEmail(customeremailtxtfieldupdate.getText());
		customer.setContactnumber(customercontactnumbertxtfieldupdate.getText());
		
		
			try
					{
					
						
						String query = "UPDATE CUSTOMER SET FIRSTNAME ='" + customer.getFirstname().toUpperCase() + "',LASTNAME ='"
										+ customer.getLastname().toUpperCase() +"',ADDRESS ='" +"',RELIGION ='"+customer.getReligion().toUpperCase()+  customer.getAddress().toUpperCase()
										+ "',CONTACTNO ='" + customer.getContactnumber().toUpperCase() + "' WHERE "
									    + "CUSTOMERID = '" + "SELECT" ;
						PreparedStatement pst2 = connection.prepareStatement(query);
						pst2.executeUpdate();
						pst2.close();
			   			 connection.close();
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
	}
	
	public void customerSignOutButtonClick(){
		customerFrame.closeCustomer();
		 login.openLogin();
	}

	private void CustomerIdQRCodeRead() {
		// TODO Auto-generated method stub
		
	}
	
}