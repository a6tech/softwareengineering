package view;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.geom.Ellipse2D;
import static java.awt.GraphicsDevice.WindowTranslucency.*;
 
public class framechoose extends JFrame {
    public framechoose() {
        super("ShapedWindow");
        
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                setShape(new Ellipse2D.Double(140, 54, 550.0f,550.0f));
            }
        });
        
        setUndecorated(true);
        setSize(900,700);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(getParent());
        getContentPane().setLayout(null);
        
        JButton btnNewButton = new JButton("New button");
        btnNewButton.setBounds(356, 92, 121, 85);
        getContentPane().add(btnNewButton);
        
        JButton button = new JButton("New button");
        button.setBounds(356, 485, 121, 85);
        getContentPane().add(button);
 
        
    }
}
