package GUI;


import javax.swing.*;

import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import GUI.EditInformation.JTextFieldLimit;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UI extends JFrame{
	

	public JFrame frame = new JFrame();
	public JFrame adddebtframe = new JFrame();
	public JFrame logInFrame = new JFrame();
	public JFrame EditFrame = new JFrame();
	public JFrame PaymentFullTransaction = new JFrame();
	public JFrame PaymentFullAmountFrame = new JFrame();
	public JFrame PaymentPartialAmountFrame = new JFrame();
	public JFrame PaymentPartialTransaction = new JFrame();
	public JPanel userpanel = new JPanel();
	public JPanel adddebitpanel = new JPanel();
	public JPanel viewpanel = new JPanel();
	public JPanel billoutpanel = new JPanel();
	public JPanel aboutuspanel = new JPanel();
	public JLabel userlabel = new JLabel("");
	public JButton ADDDEBT = new JButton("New button");
	public JButton VIEW = new JButton("New button");
	public JButton BILLOUT = new JButton("New button");
	public JButton ABOUTUS = new JButton("New button");
	public JLabel background = new JLabel("New label");
	public JButton user = new JButton("USER");
	public JLabel userposition = new JLabel("OWNER",SwingConstants.CENTER);
	public JLabel username = new JLabel("GLENA S. ZABALA",SwingConstants.CENTER);
	public SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d yyyy");
	public SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
	public Calendar currentCalendar = Calendar.getInstance();
	public Date currentTime = currentCalendar.getTime();
	public JLabel timein = new JLabel("",SwingConstants.CENTER);
	public JLabel lasttimeout = new JLabel("", SwingConstants.CENTER);
	public JButton btnNewCostumer = new JButton("");
	public JButton existingbutton = new JButton("Existing");
	public JButton signout = new JButton("");
	public JLabel personalinformation = new JLabel("PERSONAL INFORMATION",SwingConstants.CENTER);
	public JLabel firstname = new JLabel("FIRSTNAME:",SwingConstants.CENTER);
	public JLabel lastname = new JLabel("LASTNAME:",SwingConstants.CENTER);
	public JLabel address = new JLabel("ADDRESS",SwingConstants.CENTER);
	public JLabel contactnumber = new JLabel("CONTACT#:",SwingConstants.CENTER);
	public JLabel amount = new JLabel("DEBT AMOUNT:",SwingConstants.CENTER);
	public JTextField firstnametext = new JTextField("",SwingConstants.CENTER);
	public JTextField lastnametext = new JTextField("",SwingConstants.CENTER);
	public JTextField contactnumbertext = new JTextField("",SwingConstants.CENTER);
	public JTextField addresstext = new JTextField("",SwingConstants.CENTER);
	public JTextField Amount = new JTextField("",SwingConstants.CENTER);
	public JButton btnSubmit = new JButton("SUBMIT");
	public JButton btnCancel = new JButton("CANCEL");
	public JLabel personalinfopic = new JLabel("personalinfopic");
	public JLabel backgroundinside = new JLabel("New label");
	public JLabel backgroundinside1 = new JLabel("New label");
	public JLabel backgroundinside2 = new JLabel("New label");
	public JLabel backgroundinside3 = new JLabel("New label");
	public JTextField textlog = new JTextField();
	public JLabel lblLog_user = new JLabel("Username:");
	public JLabel lblLog_pass = new JLabel("Password:");
	public JPasswordField passwordField = new JPasswordField();
	public JButton btnLogIn = new JButton("LOGIN");
	public JButton btncancel = new JButton("EXIT");
	public JLabel lblLogin_welcome = new JLabel("WELCOME!!! ");
	public JTable table = new JTable();
	public JScrollPane scrollPane = new JScrollPane();
	public JButton Search = new JButton("SEARCH");
	public JButton DONE = new JButton("DONE");
	public JButton CANCEL = new JButton("CANCEL");
	public JLabel LISTOFCUSTOMERS = new JLabel("LIST OF CUSTOMERS",SwingConstants.CENTER);
	public JButton RESET = new JButton("RESET");
	public JLabel adddebitpersonalinfo = new JLabel("PERSONAL INFORMATION",SwingConstants.CENTER);
	public JLabel adddebitfirstname = new JLabel("FIRSTNAME:",SwingConstants.CENTER);
	public JLabel adddebitlastname = new JLabel("LASTNAME:",SwingConstants.CENTER);
	public JLabel adddebitaddress = new JLabel("ADDRESS:",SwingConstants.CENTER);
	public JLabel adddebitcontact = new JLabel("CONTACT#:",SwingConstants.CENTER);
	public JLabel addebitfirstnametext = new JLabel("");
	public JLabel adddebitlastnametext = new JLabel("");
	public JLabel adddebitaddresstext = new JLabel("");
	public JLabel adddebitcontacttext = new JLabel("");
	public JLabel adddebitdebtinformation = new JLabel("DEBT INFORMATION");
	public JTable table_1 = new JTable();
	public JScrollPane ADDDEBITDEBTINFOTABLE = new JScrollPane();
	public JButton addebitcancel = new JButton("CANCEL");
	public JButton ADDDEBITREFRESH = new JButton("REFRESH");
	public JLabel backgroundinsideuerpanel = new JLabel("New label");
	public JTextField firstnamesearchexistingbutton = new JTextField("", SwingConstants.CENTER);
	public JTextField lastnamesearchexistingbutton = new JTextField("", SwingConstants.CENTER);
	public JLabel firstnameexistingbutton = new JLabel("FIRSTNAME:", SwingConstants.CENTER);
	public JLabel lastnamexistingbutton = new JLabel("LASTNAME:", SwingConstants.CENTER);
	public JLabel saerchcustomerexisting = new JLabel("SEARCH CUSTOMER ", SwingConstants.CENTER);
	public JLabel QUESTIONFORADDINGDEBT = new JLabel("DO YOU WANT TO ADD DEBT TO YOUR EXISTING BALANCE?");
	public JButton YESBUTTONFORADDINGDEBT = new JButton("YES");
	public JTextField adddebttextfield = new JTextField("",SwingConstants.CENTER);
	public JLabel adddebtbackgroundinside = new JLabel("New label");
	public JButton btnSubmitDebt = new JButton("CONFIRM");
	public JButton btnCancelDebt = new JButton("CANCEL");
	public JLabel QUESTIONADDEBT = new JLabel("HOW MUCH DEBT WOULD YOU LIKE \r\nTO \r\nADD ?",SwingConstants.CENTER);
	public JButton ViewSearch = new JButton("SEARCH");
	public JTextField Viewfirstnamefield = new JTextField("", SwingConstants.CENTER);
	public JTextField Viewlastnamefield = new JTextField("", SwingConstants.CENTER);
	public JLabel Viewfistnamelabel = new JLabel("FIRSTNAME:", SwingConstants.CENTER);
	public JLabel Viewlastnamelabel = new JLabel("LASTNAME:", SwingConstants.CENTER);
	public JLabel Viewsearchcustomerlabel = new JLabel("SEARCH CUSTOMER ", SwingConstants.CENTER);
	public JTable Viewtable = new JTable();
	public JScrollPane ViewtableScrollpane = new JScrollPane();
	public JLabel Viewlistofcustomers = new JLabel("LIST OF CUSTOMERS",SwingConstants.CENTER);
	public JButton ViewResetButton = new JButton("RESET");
	public JLabel ViewSelectedpersonalinfo = new JLabel("PERSONAL INFORMATION",SwingConstants.CENTER);
	public JLabel ViewSelectedfirstname = new JLabel("FIRSTNAME:",SwingConstants.CENTER);
	public JLabel ViewSelectedlastname = new JLabel("LASTNAME:",SwingConstants.CENTER);
	public JLabel ViewSelectedaddress = new JLabel("ADDRESS:",SwingConstants.CENTER);
	public JLabel ViewSelectedcontactno = new JLabel("CONTACT#:",SwingConstants.CENTER);
	public JLabel ViewSelectedBalance = new JLabel("BALANCE:",SwingConstants.CENTER);
	public JLabel ViewSelectedfirstnametext = new JLabel("");
	public JLabel ViewSelectedlastnametext = new JLabel("");
	public JLabel ViewSelectedaddresstext = new JLabel("");
	public JLabel ViewSelectedcontacttext = new JLabel("");
	public JTable ViewDebtinformationtable = new JTable();
	public JTable ViewtablePaymentHistory = new JTable();
	public JLabel ViewPaymentHistory = new JLabel("PAYMENT TRANSACTIONS ",SwingConstants.CENTER);
	public JLabel ViewDebtInformation = new JLabel("DEBT INFORMATION", SwingConstants.CENTER);
	public JLabel ViewSelectedbalanceText = new JLabel("");
	public JScrollPane ViewDebtinformationscrollpane = new JScrollPane();
	public JScrollPane ViewPaymentHistoryScrollpane = new JScrollPane();
	public JButton Vieweditinformation = new JButton("UPDATE INFORMATION");
	public JButton ViewReturnButton = new JButton("RETURN");
	public JLabel ViewEditInformation = new JLabel("PERSONAL INFORMATION",SwingConstants.CENTER);
	public JLabel ViewEditFirstname = new JLabel("FIRSTNAME:",SwingConstants.CENTER);
	public JLabel ViewEditlastname = new JLabel("LASTNAME:",SwingConstants.CENTER);
	public JLabel ViewEditaddress = new JLabel("ADDRESS",SwingConstants.CENTER);
	public JLabel ViewEditcontactnumber = new JLabel("CONTACT#:",SwingConstants.CENTER);
	public JTextField ViewEditfirstnametext = new JTextField("",SwingConstants.CENTER);
	public JTextField ViewEditlastnametext = new JTextField("",SwingConstants.CENTER);
	public JTextField ViewEditaddresstext = new JTextField("",SwingConstants.CENTER);
	public JTextField ViewEditcontactnumbertext = new JTextField("",SwingConstants.CENTER);
	public JButton ViewEditbtnSubmit = new JButton("SUBMIT");
	public JButton ViewEditbtnCancel = new JButton("CANCEL");
	public JLabel ViewEditpersonalinfopic = new JLabel("personalinfopic");
	public JLabel ViewEditBackgroundInside = new JLabel("New label");
	public JButton PaymentFullpaymentBtn = new JButton("FULL PAYMENT TRANSACTION");
	public JButton PaymentDownPaymentBtn = new JButton("DOWN PAYMENT TRANSACTION");
	public JTable PaymentFullTable = new JTable();
	public JScrollPane PaymentFullTableScrollpane = new JScrollPane();
	public JButton PaymentFullSearchBtn = new JButton("SEARCH");
	public JButton PaymentFullDoneBtn = new JButton("DONE");
	public JButton PaymentFullCancelBtn = new JButton("CANCEL");
	public JLabel PaymentFullListCofCustomers = new JLabel("LIST OF CUSTOMERS",SwingConstants.CENTER);
	public JButton PaymentFullResetBtn = new JButton("RESET");
	public JTextField PaymentFullfirstnameTextfield = new JTextField("", SwingConstants.CENTER);
	public JTextField PaymentFullLastnameTextfield = new JTextField("", SwingConstants.CENTER);
	public JLabel PaymentFullFirstnamelabel = new JLabel("FIRSTNAME:", SwingConstants.CENTER);
	public JLabel PaymentFulllastnamelabel = new JLabel("LASTNAME:", SwingConstants.CENTER);
	public JLabel PaymentFullSearchCustomertext = new JLabel("SEARCH CUSTOMER ", SwingConstants.CENTER);
	public JTable PaymentPartialTable = new JTable();
	public JScrollPane PaymentPartialTableScrollpane = new JScrollPane();
	public JButton PaymentPartialSearchBtn = new JButton("SEARCH");
	public JButton PaymentPartialDoneBtn = new JButton("DONE");
	public JButton PaymentPartialCancelBtn = new JButton("CANCEL");
	public JLabel PaymentPartialListCofCustomers = new JLabel("LIST OF CUSTOMERS",SwingConstants.CENTER);
	public JButton PaymentPartialResetBtn = new JButton("RESET");
	public JTextField PaymentPartialfirstnameTextfield = new JTextField("", SwingConstants.CENTER);
	public JTextField PaymentPartialLastnameTextfield = new JTextField("", SwingConstants.CENTER);
	public JLabel PaymentPartialFirstnamelabel = new JLabel("FIRSTNAME:", SwingConstants.CENTER);
	public JLabel PaymentPartiallastnamelabel = new JLabel("LASTNAME:", SwingConstants.CENTER);
	public JLabel PaymentPartialSearchCustomertext = new JLabel("SEARCH CUSTOMER ", SwingConstants.CENTER);
	public JLabel paymentfullTransactionbackgroundinside = new JLabel("New label");
	public JLabel PaymentFullTransactionCompanyLabel = new JLabel("ZABALA'S CANTEEN", SwingConstants.CENTER);
	public JButton PaymentFullTransactionConfirmBtn = new JButton("CONFIRM");
	public JLabel PaymentFullTransactionCustomerNameLabel = new JLabel("CUSTOMER NAME:");
	public JLabel PaymentFullTransactionCustomerBalLabel = new JLabel("CUSTOMER BALANCE:");
	public JLabel PaymentFullTransactionPaymentAmountLabel = new JLabel("PAYMENT AMOUNT:");
	public JLabel PaymentFullTransactionCustomerNameText = new JLabel("");
	public JLabel PaymentFullTransactionCustomerBalText = new JLabel("");
	public JLabel PaymentFullTransactionPaymentAmountText = new JLabel("");
	public JLabel PaymentFullTransactionChangedLabel = new JLabel("CHANGE:", SwingConstants.CENTER);
	public JLabel PaymentFullTransactionChangedText = new JLabel("");
	public JLabel PaymentFullDateAndTimeText = new JLabel("");
	public JLabel PaymentFullDateAndTimeLabel = new JLabel("DATE AND TIME:");
	public JTextField paymentfulltamounttextfield = new JTextField("",SwingConstants.CENTER);
	public JLabel paymentfullbackgroundinside = new JLabel("New label");
	public JButton paymentfullconfirmbtn = new JButton("CONFIRM");
	public JButton paymentfullcancelbtn = new JButton("CANCEL");
	public JLabel paymentfullquestion = new JLabel("AMOUNT PAID BY THE CUSTOMER",SwingConstants.CENTER);
	public JTextField paymentPartialtamounttextfield = new JTextField("",SwingConstants.CENTER);
	public JLabel paymentPartialbackgroundinside = new JLabel("New label");
	public JButton paymentPartialconfirmbtn = new JButton("CONFIRM");
	public JButton paymentPartialcancelbtn = new JButton("CANCEL");
	public JLabel paymentPartialquestion = new JLabel("AMOUNT PAID BY THE CUSTOMER",SwingConstants.CENTER);
	public JLabel paymentPartialTransactionbackgroundinside = new JLabel("New label");
	public JLabel PaymentPartialTransactionCompanyLabel = new JLabel("ZABALA'S CANTEEN", SwingConstants.CENTER);
	public JButton PaymentPartialTransactionConfirmBtn = new JButton("CONFIRM");
	public JLabel PaymentPartialTransactionCustomerNameLabel = new JLabel("CUSTOMER NAME:");
	public JLabel PaymentPartialTransactionCustomerBalLabel = new JLabel("CUSTOMER BALANCE:");
	public JLabel PaymentPartialTransactionPaymentAmountLabel = new JLabel("PAYMENT AMOUNT:");
	public JLabel PaymentPartialTransactionCustomerNameText = new JLabel("");
	public JLabel PaymentPartialTransactionCustomerBalText = new JLabel("");
	public JLabel PaymentPartialTransactionPaymentAmountText = new JLabel("");
	public JLabel PaymentPartialTransactionChangedLabel = new JLabel("CHANGE:", SwingConstants.CENTER);
	public JLabel PaymentPartialTransactionChangedText = new JLabel("");
	public JLabel PaymentPartialDateAndTimeText = new JLabel("");
	public JLabel PaymentPartialDateAndTimeLabel = new JLabel("DATE AND TIME:");
	
	public JLabel lblNewLabel = new JLabel("");
	public JLabel lblrenmart = new JLabel("PROJECT MANAGER: Renmart Callejo");
	public JLabel lblDatabaseAnalystArt = new JLabel("DATABASE ANALYST: Art Maco Del Rosario");
	public JLabel lblFrontendDeveloperDiomede = new JLabel("FRONT-END DEVELOPER: Diomede Zabala");
	public JLabel lblBackendDeveloperSear = new JLabel("BACK-END DEVELOPER: Sear John Venturina");
	public JLabel lblTe = new JLabel("TESTER 1: Joshua Soriano");
	public JLabel lblTesterReylin = new JLabel("TESTER 2: Reylin Julwin Cortez");
	public JLabel motoLbl = new JLabel("\"We CREATE in the EASIEST way\"");
	public JLabel lblatech = new JLabel("-A6Tech");
	public JLabel contactlbl = new JLabel("a6tectbrotherhood@gmail.com");
	
	public UI(){
		
		
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.setResizable(false);
		frame.setBounds(100, 100, 1139, 656);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		frame.setUndecorated(true);
		
		getLoginFrame();
		getUserAttributes();
		getAddebitAttributes();
		getVIEWATTRIBUTES();
		getABOUTUSATTRIBUTES();
		getBILLOUTATTRIBUTES();
		getDebtFrame();
		getEditInformationFrame();
		getPaymentFullFrame();
		getPaymentFullAmountframe();
		getPaymentPartialAmountframe();
		getPaymentPartialFrame();
		
		background.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\background.png"));
		background.setBounds(0, 0, 1138, 658);
		frame.getContentPane().add(background);
		
	}
	
	private void getPaymentPartialFrame() 
	{
		PaymentPartialTransaction.setUndecorated(true);
		PaymentPartialTransaction.setBounds(67, 170, 500, 313);
		PaymentPartialTransaction.getContentPane().setLayout(null);
		PaymentPartialTransaction.setResizable(false);
		PaymentPartialTransaction.setLocationRelativeTo(null);
		PaymentPartialTransaction.setVisible(false);
		PaymentPartialTransaction.getRootPane().setDefaultButton(PaymentPartialTransactionConfirmBtn);
		PaymentPartialTransaction.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		PaymentPartialTransactionCompanyLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		PaymentPartialTransactionCompanyLabel.setForeground(Color.WHITE);
		PaymentPartialTransactionCompanyLabel.setBounds(29, 11, 364, 23);
		PaymentPartialTransaction.getContentPane().add(PaymentPartialTransactionCompanyLabel);
		
		
		PaymentPartialTransactionConfirmBtn.setBounds(159, 250, 126, 38);
		PaymentPartialTransaction.getContentPane().add(PaymentPartialTransactionConfirmBtn);
		
		
		PaymentPartialTransactionCustomerNameLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		PaymentPartialTransactionCustomerNameLabel.setForeground(Color.WHITE);
		PaymentPartialTransactionCustomerNameLabel.setBounds(25, 94, 142, 28);
		PaymentPartialTransaction.getContentPane().add(PaymentPartialTransactionCustomerNameLabel);
		
		
		PaymentPartialTransactionCustomerBalLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		PaymentPartialTransactionCustomerBalLabel.setForeground(Color.WHITE);
		PaymentPartialTransactionCustomerBalLabel.setBounds(25, 133, 142, 28);
		PaymentPartialTransaction.getContentPane().add(PaymentPartialTransactionCustomerBalLabel);
		
		
		PaymentPartialTransactionPaymentAmountLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		PaymentPartialTransactionPaymentAmountLabel.setForeground(Color.WHITE);
		PaymentPartialTransactionPaymentAmountLabel.setBounds(29, 172, 142, 28);
		PaymentPartialTransaction.getContentPane().add(PaymentPartialTransactionPaymentAmountLabel);
		
		
		PaymentPartialTransactionCustomerNameText.setFont(new Font("Tahoma", Font.BOLD, 14));
		PaymentPartialTransactionCustomerNameText.setForeground(Color.WHITE);
		PaymentPartialTransactionCustomerNameText.setBounds(177, 92, 313, 30);
		PaymentPartialTransaction.getContentPane().add(PaymentPartialTransactionCustomerNameText);
		
		
		PaymentPartialTransactionCustomerBalText.setFont(new Font("Tahoma", Font.PLAIN, 16));
		PaymentPartialTransactionCustomerBalText.setForeground(Color.WHITE);
		PaymentPartialTransactionCustomerBalText.setBounds(177, 131, 132, 30);
		PaymentPartialTransaction.getContentPane().add(PaymentPartialTransactionCustomerBalText);
		
		
		PaymentPartialTransactionPaymentAmountText.setFont(new Font("Tahoma", Font.PLAIN, 16));
		PaymentPartialTransactionPaymentAmountText.setForeground(Color.WHITE);
		PaymentPartialTransactionPaymentAmountText.setBounds(177, 170, 132, 30);
		PaymentPartialTransaction.getContentPane().add(PaymentPartialTransactionPaymentAmountText);
		
		
		PaymentPartialTransactionChangedLabel.setForeground(Color.WHITE);
		PaymentPartialTransactionChangedLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		PaymentPartialTransactionChangedLabel.setBounds(29, 211, 142, 28);
		PaymentPartialTransaction.getContentPane().add(PaymentPartialTransactionChangedLabel);
		
		
		PaymentPartialTransactionChangedText.setForeground(Color.WHITE);
		PaymentPartialTransactionChangedText.setFont(new Font("Tahoma", Font.PLAIN, 16));
		PaymentPartialTransactionChangedText.setBounds(177, 211, 132, 30);
		PaymentPartialTransaction.getContentPane().add(PaymentPartialTransactionChangedText);
		

		PaymentPartialDateAndTimeLabel.setForeground(Color.WHITE);
		PaymentPartialDateAndTimeLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		PaymentPartialDateAndTimeLabel.setBounds(25, 63, 142, 28);
		PaymentPartialTransaction.getContentPane().add(PaymentPartialDateAndTimeLabel);
		
		
		PaymentPartialDateAndTimeText.setForeground(Color.WHITE);
		PaymentPartialDateAndTimeText.setFont(new Font("Tahoma", Font.PLAIN, 16));
		PaymentPartialDateAndTimeText.setBounds(177, 63, 216, 30);
		PaymentPartialTransaction.getContentPane().add(PaymentPartialDateAndTimeText);
		

		paymentPartialTransactionbackgroundinside.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\background.png"));
		paymentPartialTransactionbackgroundinside.setBounds(0, 0, 500, 313);
		PaymentPartialTransaction.getContentPane().add(paymentPartialTransactionbackgroundinside);	
		
	}

	private void getPaymentPartialAmountframe() 
	{
		
		PaymentPartialAmountFrame.setUndecorated(true);
		PaymentPartialAmountFrame.setBounds(100, 100, 439, 306);
		PaymentPartialAmountFrame.getContentPane().setLayout(null);
		PaymentPartialAmountFrame.setResizable(false);
		PaymentPartialAmountFrame.setLocationRelativeTo(null);
		PaymentPartialAmountFrame.setVisible(false);
		PaymentPartialAmountFrame.getRootPane().setDefaultButton(paymentPartialconfirmbtn);
		PaymentPartialAmountFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		

		paymentPartialtamounttextfield.setFont(new Font("SansSerif", Font.BOLD, 20));
		paymentPartialtamounttextfield.setBounds(46, 106, 325, 51);
		PaymentPartialAmountFrame.getContentPane().add(paymentPartialtamounttextfield);
		paymentPartialtamounttextfield.setColumns(10);
		paymentPartialtamounttextfield.setDocument(new TextFieldNumbersOnlyLimit(6));
		paymentPartialtamounttextfield.setHorizontalAlignment(JTextField.CENTER);
		
		paymentPartialquestion.setFont(new Font("SansSerif", Font.BOLD, 14));
		paymentPartialquestion.setBounds(46, 34, 325, 62);
		paymentPartialquestion.setForeground(Color.WHITE);
		PaymentPartialAmountFrame.getContentPane().add(paymentPartialquestion);
		
		paymentPartialconfirmbtn.setBounds(46, 186, 150, 45);
		PaymentPartialAmountFrame.getContentPane().add(paymentPartialconfirmbtn);
		
		paymentPartialcancelbtn.setBounds(221, 186, 150, 45);
		PaymentPartialAmountFrame.getContentPane().add(paymentPartialcancelbtn);
		
		paymentPartialbackgroundinside.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\background.png"));
		paymentPartialbackgroundinside.setBounds(0, 0, 1011, 477);
		PaymentPartialAmountFrame.getContentPane().add(paymentPartialbackgroundinside);
	}

	public void getPaymentFullAmountframe()
	{

		PaymentFullAmountFrame.setUndecorated(true);
		PaymentFullAmountFrame.setBounds(100, 100, 439, 306);
		PaymentFullAmountFrame.getContentPane().setLayout(null);
		PaymentFullAmountFrame.setResizable(false);
		PaymentFullAmountFrame.setLocationRelativeTo(null);
		PaymentFullAmountFrame.setVisible(false);
		PaymentFullAmountFrame.getRootPane().setDefaultButton(paymentfullconfirmbtn);
		PaymentFullAmountFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		

		paymentfulltamounttextfield.setFont(new Font("SansSerif", Font.BOLD, 20));
		paymentfulltamounttextfield.setBounds(46, 106, 325, 51);
		PaymentFullAmountFrame.getContentPane().add(paymentfulltamounttextfield);
		paymentfulltamounttextfield.setColumns(10);
		paymentfulltamounttextfield.setDocument(new TextFieldNumbersOnlyLimit(6));
		paymentfulltamounttextfield.setHorizontalAlignment(JTextField.CENTER);
		
		paymentfullquestion.setFont(new Font("SansSerif", Font.BOLD, 14));
		paymentfullquestion.setBounds(46, 34, 325, 62);
		paymentfullquestion.setForeground(Color.WHITE);
		PaymentFullAmountFrame.getContentPane().add(paymentfullquestion);
		
		paymentfullconfirmbtn.setBounds(46, 186, 150, 45);
		PaymentFullAmountFrame.getContentPane().add(paymentfullconfirmbtn);
		
		paymentfullcancelbtn.setBounds(221, 186, 150, 45);
		PaymentFullAmountFrame.getContentPane().add(paymentfullcancelbtn);
		
		paymentfullbackgroundinside.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\background.png"));
		paymentfullbackgroundinside.setBounds(0, 0, 1011, 477);
		PaymentFullAmountFrame.getContentPane().add(paymentfullbackgroundinside);
		
	}
	
	public void getDebtFrame()
	{
		adddebtframe.setUndecorated(true);
		adddebtframe.setBounds(100, 100, 439, 306);
		adddebtframe.getContentPane().setLayout(null);
		adddebtframe.setResizable(false);
		adddebtframe.setLocationRelativeTo(null);
		adddebtframe.setVisible(false);
		adddebtframe.getRootPane().setDefaultButton(btnSubmitDebt);
		adddebtframe.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		
		
		adddebttextfield.setFont(new Font("SansSerif", Font.BOLD, 20));
		adddebttextfield.setBounds(46, 106, 325, 51);
		adddebtframe.getContentPane().add(adddebttextfield);
		adddebttextfield.setColumns(10);
		adddebttextfield.setDocument(new TextFieldNumbersOnlyLimit(6));
		adddebttextfield.setHorizontalAlignment(JTextField.CENTER);
		
		QUESTIONADDEBT.setFont(new Font("SansSerif", Font.BOLD, 14));
		QUESTIONADDEBT.setBounds(46, 34, 325, 62);
		QUESTIONADDEBT.setForeground(Color.WHITE);
		adddebtframe.getContentPane().add(QUESTIONADDEBT);
		
		btnSubmitDebt.setBounds(46, 186, 150, 45);
		adddebtframe.getContentPane().add(btnSubmitDebt);
		
		btnCancelDebt.setBounds(221, 186, 150, 45);
		adddebtframe.getContentPane().add(btnCancelDebt);
		
		adddebtbackgroundinside.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\background.png"));
		adddebtbackgroundinside.setBounds(0, 0, 1011, 477);
		adddebtframe.getContentPane().add(adddebtbackgroundinside);
	}
	
	public void getEditInformationFrame()
	{
		EditFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		EditFrame.setBounds(67, 170, 1011, 477);
		EditFrame.getContentPane().setLayout(null);
		EditFrame.setUndecorated(true);
		EditFrame.setResizable(false);
		EditFrame.setLocationRelativeTo(null);
		EditFrame.setVisible(false);
		EditFrame.getRootPane().setDefaultButton(ViewEditbtnSubmit);
		
		
		
		
		ViewEditInformation.setFont(new Font("Times New Roman", Font.PLAIN, 25));
		ViewEditInformation.setBounds(338, 6, 327, 70);
		ViewEditInformation.setForeground(Color.WHITE);
		EditFrame.getContentPane().add(ViewEditInformation);
		


		ViewEditFirstname.setFont(new Font("Times New Roman", Font.BOLD, 17));
		ViewEditFirstname.setBounds(190, 87, 110, 41);
		ViewEditFirstname.setForeground(Color.WHITE);
		EditFrame.getContentPane().add(ViewEditFirstname);
		
		
		ViewEditlastname.setFont(new Font("Times New Roman", Font.BOLD, 17));
		ViewEditlastname.setBounds(190, 140, 110, 41);
		ViewEditlastname.setForeground(Color.WHITE);
		EditFrame.getContentPane().add(ViewEditlastname);
		
		
		ViewEditaddress.setFont(new Font("Times New Roman", Font.BOLD, 17));
		ViewEditaddress.setBounds(190, 193, 110, 41);
		ViewEditaddress.setForeground(Color.WHITE);
		EditFrame.getContentPane().add(ViewEditaddress);
		
		
		ViewEditcontactnumber.setFont(new Font("Times New Roman", Font.BOLD, 17));
		ViewEditcontactnumber.setBounds(190, 246, 110, 41);
		ViewEditcontactnumber.setForeground(Color.WHITE);
		EditFrame.getContentPane().add(ViewEditcontactnumber);
		



		ViewEditfirstnametext = new JTextField();
		ViewEditfirstnametext.setFont(new Font("SansSerif", Font.BOLD, 15));
		ViewEditfirstnametext.setBounds(312, 93, 263, 28);
		EditFrame.getContentPane().add(ViewEditfirstnametext);
		ViewEditfirstnametext.setColumns(10);
		
		ViewEditfirstnametext.setDocument(new JTextFieldLettersOnlyLimit(20));
		
		
		ViewEditlastnametext = new JTextField();
		ViewEditlastnametext.setFont(new Font("SansSerif", Font.BOLD, 15));
		ViewEditlastnametext.setColumns(10);
		ViewEditlastnametext.setBounds(312, 147, 263, 28);
		EditFrame.getContentPane().add(ViewEditlastnametext);
		
		ViewEditlastnametext.setDocument(new JTextFieldLettersOnlyLimit(20));
		
		
	
		ViewEditaddresstext.setFont(new Font("SansSerif", Font.BOLD, 15));
		ViewEditaddresstext.setColumns(10);
		ViewEditaddresstext.setBounds(312, 200, 340, 28);
		EditFrame.getContentPane().add(ViewEditaddresstext);
		
		ViewEditaddresstext.setDocument(new JTextFieldLimit(40));
		
		
	
		ViewEditcontactnumbertext.setFont(new Font("SansSerif", Font.BOLD, 15));
		ViewEditcontactnumbertext.setColumns(10);
		ViewEditcontactnumbertext.setBounds(312, 253, 160, 28);
		EditFrame.getContentPane().add(ViewEditcontactnumbertext);
		
		ViewEditcontactnumbertext.setDocument(new TextFieldContactNumberOnlyLimit(11));



		ViewEditbtnSubmit.setBounds(193, 367, 182, 52);
		EditFrame.getContentPane().add(ViewEditbtnSubmit);
		
		
		
		ViewEditbtnCancel.setBounds(584, 367, 182, 52);
		EditFrame.getContentPane().add(ViewEditbtnCancel);
		
		
		ViewEditpersonalinfopic.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\infoicon.png"));
		ViewEditpersonalinfopic.setBounds(677, 115, 182, 200);
		EditFrame.getContentPane().add(ViewEditpersonalinfopic);
		
		
		ViewEditBackgroundInside.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\background.png"));
		ViewEditBackgroundInside.setBounds(0, 0, 1011, 477);
		EditFrame.getContentPane().add(ViewEditBackgroundInside);
	}
	
	public void getPaymentFullFrame()
	{
		PaymentFullTransaction.setUndecorated(true);
		PaymentFullTransaction.setBounds(67, 170, 500, 313);
		PaymentFullTransaction.getContentPane().setLayout(null);
		PaymentFullTransaction.setResizable(false);
		PaymentFullTransaction.setLocationRelativeTo(null);
		PaymentFullTransaction.setVisible(false);
		PaymentFullTransaction.getRootPane().setDefaultButton(PaymentFullTransactionConfirmBtn);
		PaymentFullTransaction.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		PaymentFullTransactionCompanyLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		PaymentFullTransactionCompanyLabel.setForeground(Color.WHITE);
		PaymentFullTransactionCompanyLabel.setBounds(29, 11, 364, 23);
		PaymentFullTransaction.getContentPane().add(PaymentFullTransactionCompanyLabel);
		
		
		PaymentFullTransactionConfirmBtn.setBounds(159, 250, 126, 38);
		PaymentFullTransaction.getContentPane().add(PaymentFullTransactionConfirmBtn);
		
		
		PaymentFullTransactionCustomerNameLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		PaymentFullTransactionCustomerNameLabel.setForeground(Color.WHITE);
		PaymentFullTransactionCustomerNameLabel.setBounds(25, 94, 142, 28);
		PaymentFullTransaction.getContentPane().add(PaymentFullTransactionCustomerNameLabel);
		
		
		PaymentFullTransactionCustomerBalLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		PaymentFullTransactionCustomerBalLabel.setForeground(Color.WHITE);
		PaymentFullTransactionCustomerBalLabel.setBounds(25, 133, 142, 28);
		PaymentFullTransaction.getContentPane().add(PaymentFullTransactionCustomerBalLabel);
		
		
		PaymentFullTransactionPaymentAmountLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		PaymentFullTransactionPaymentAmountLabel.setForeground(Color.WHITE);
		PaymentFullTransactionPaymentAmountLabel.setBounds(29, 172, 142, 28);
		PaymentFullTransaction.getContentPane().add(PaymentFullTransactionPaymentAmountLabel);
		
		
		PaymentFullTransactionCustomerNameText.setFont(new Font("Tahoma", Font.BOLD, 14));
		PaymentFullTransactionCustomerNameText.setForeground(Color.WHITE);
		PaymentFullTransactionCustomerNameText.setBounds(177, 92, 313, 30);
		PaymentFullTransaction.getContentPane().add(PaymentFullTransactionCustomerNameText);
		
		
		PaymentFullTransactionCustomerBalText.setFont(new Font("Tahoma", Font.PLAIN, 16));
		PaymentFullTransactionCustomerBalText.setForeground(Color.WHITE);
		PaymentFullTransactionCustomerBalText.setBounds(177, 131, 132, 30);
		PaymentFullTransaction.getContentPane().add(PaymentFullTransactionCustomerBalText);
		
		
		PaymentFullTransactionPaymentAmountText.setFont(new Font("Tahoma", Font.PLAIN, 16));
		PaymentFullTransactionPaymentAmountText.setForeground(Color.WHITE);
		PaymentFullTransactionPaymentAmountText.setBounds(177, 170, 132, 30);
		PaymentFullTransaction.getContentPane().add(PaymentFullTransactionPaymentAmountText);
		
		
		PaymentFullTransactionChangedLabel.setForeground(Color.WHITE);
		PaymentFullTransactionChangedLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		PaymentFullTransactionChangedLabel.setBounds(29, 211, 142, 28);
		PaymentFullTransaction.getContentPane().add(PaymentFullTransactionChangedLabel);
		
		
		PaymentFullTransactionChangedText.setForeground(Color.WHITE);
		PaymentFullTransactionChangedText.setFont(new Font("Tahoma", Font.PLAIN, 16));
		PaymentFullTransactionChangedText.setBounds(177, 211, 132, 30);
		PaymentFullTransaction.getContentPane().add(PaymentFullTransactionChangedText);
		

		PaymentFullDateAndTimeLabel.setForeground(Color.WHITE);
		PaymentFullDateAndTimeLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		PaymentFullDateAndTimeLabel.setBounds(25, 63, 142, 28);
		PaymentFullTransaction.getContentPane().add(PaymentFullDateAndTimeLabel);
		
		
		PaymentFullDateAndTimeText.setForeground(Color.WHITE);
		PaymentFullDateAndTimeText.setFont(new Font("Tahoma", Font.PLAIN, 16));
		PaymentFullDateAndTimeText.setBounds(177, 63, 216, 30);
		PaymentFullTransaction.getContentPane().add(PaymentFullDateAndTimeText);
		

		paymentfullTransactionbackgroundinside.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\background.png"));
		paymentfullTransactionbackgroundinside.setBounds(0, 0, 500, 313);
		PaymentFullTransaction.getContentPane().add(paymentfullTransactionbackgroundinside);	}
	
	
	public void getLoginFrame()
	{
		logInFrame.setTitle("LogIn");
		logInFrame.getContentPane().setLayout(null);
		logInFrame.setBounds(100, 100, 368, 254);
		logInFrame.getRootPane().setDefaultButton(btnLogIn);
		logInFrame.setLocationRelativeTo(null);
		logInFrame.setUndecorated(true);
		logInFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		logInFrame.setBackground(SystemColor.desktop);
		logInFrame.setForeground(SystemColor.desktop);
		
		
		textlog.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		textlog.setForeground(SystemColor.desktop);
		textlog.setToolTipText("");
		textlog.setBounds(97, 100, 244, 34);
		textlog.setBackground(Color.WHITE);
		logInFrame.getContentPane().add(textlog);
		textlog.setColumns(10);
		textlog.setDocument(new JTextFieldLimit(10));
		
		
		lblLog_user.setForeground(SystemColor.desktop);
		lblLog_user.setBounds(28, 111, 69, 14);
		lblLog_user.setBackground(SystemColor.desktop);
		lblLog_user.setFont(new Font("Franklin Gothic Book", Font.BOLD, 11));
		logInFrame.getContentPane().add(lblLog_user);
		
		lblLog_pass.setFont(new Font("Franklin Gothic Book", Font.BOLD, 11));
		lblLog_pass.setForeground(SystemColor.desktop);
		lblLog_pass.setBackground(SystemColor.controlLtHighlight);
		lblLog_pass.setBounds(30, 156, 77, 14);
		logInFrame.getContentPane().add(lblLog_pass);
		
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 13));
		passwordField.setForeground(SystemColor.desktop);
		passwordField.setBackground(Color.WHITE);
		passwordField.setColumns(10);
		passwordField.setBounds(97, 145, 244, 34);
		logInFrame.getContentPane().add(passwordField);
		passwordField.setDocument(new JTextFieldLimit(10));
		
	
		btnLogIn.setForeground(SystemColor.desktop);
		btnLogIn.setFont(new Font("Franklin Gothic Medium Cond", Font.BOLD, 12));
		btnLogIn.setBounds(28, 201, 114, 34);
		logInFrame.getContentPane().add(btnLogIn);

		btncancel.setForeground(SystemColor.desktop);
		btncancel.setFont(new Font("Franklin Gothic Medium Cond", Font.BOLD, 12));
		btncancel.setBounds(227, 201, 114, 34);
		logInFrame.getContentPane().add(btncancel);
	
		
		
		lblLogin_welcome.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\FB_IMG_1456030201858.jpg"));
		lblLogin_welcome.setBounds(116, 18, 150, 71);
		logInFrame.getContentPane().add(lblLogin_welcome);
		
	}
	
	public void getUserAttributes()
	{
		userpanel.setBounds(67, 170, 1011, 477);
		userpanel.setLayout(null);
		frame.getContentPane().add(userpanel);
		
		userlabel.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\USERIMAGE.png"));
		userlabel.setBounds(137, 120, 200, 200);
		userpanel.add(userlabel);
		
		username.setFont(new Font("Times New Roman", Font.BOLD, 36));
		username.setBounds(400, 105, 448, 68);
		userpanel.add(username);
		
		
		
		signout.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\signout.jpg"));
		signout.setBounds(157, 331, 172, 55);
		userpanel.add(signout);
		userposition.setFont(new Font("Times New Roman", Font.BOLD, 36));
		userposition.setBounds(400, 184, 448, 68);
		
		userpanel.add(userposition);
		timein.setFont(new Font("Times New Roman", Font.PLAIN, 25));
		timein.setBounds(381, 331, 517, 57);
		userpanel.add(timein);
		
		lasttimeout.setFont(new Font("Times New Roman", Font.PLAIN, 25));
		lasttimeout.setBounds(381, 263, 498, 55);
		userpanel.add(lasttimeout);
		
		backgroundinsideuerpanel.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\backgroundjpanel.png"));
		backgroundinsideuerpanel.setBounds(0, 0, 1011, 477);
		userpanel.add(backgroundinsideuerpanel);
		
		user.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\USERLOGO.png"));
		user.setBackground(SystemColor.menu);
		user.setBounds(67, 11, 199, 139);
		frame.getContentPane().add(user);
		user.setToolTipText("User Information");
		pack();
	}
	
	
	public void getAddebitAttributes()
	{
		adddebitpanel.setBounds(67, 170, 1011, 477);
		adddebitpanel.setLayout(null);
		frame.getContentPane().add(adddebitpanel);
		adddebitpanel.setVisible(false);
		
		btnNewCostumer.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\addnewcustomer.png"));
		btnNewCostumer.setBounds(522, 49, 415, 319);
		adddebitpanel.add(btnNewCostumer);
		btnNewCostumer.setToolTipText("Add New Customer");
		pack();
		
		existingbutton.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\existing.png"));
		existingbutton.setBounds(69, 49, 415, 319);
		adddebitpanel.add(existingbutton);
		existingbutton.setToolTipText("Existing Customer");
		pack();
		
		
		personalinformation.setFont(new Font("Times New Roman", Font.PLAIN, 25));
		personalinformation.setBounds(338, 6, 327, 70);
		adddebitpanel.add(personalinformation);
		personalinformation.setVisible(false);
	
		firstname.setFont(new Font("Times New Roman", Font.BOLD, 17));
		firstname.setBounds(190, 87, 110, 41);
		adddebitpanel.add(firstname);
		firstname.setVisible(false);
		
		lastname.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lastname.setBounds(190, 140, 110, 41);
		adddebitpanel.add(lastname);
		lastname.setVisible(false);
		
		address.setFont(new Font("Times New Roman", Font.BOLD, 17));
		address.setBounds(190, 193, 110, 41);
		adddebitpanel.add(address);
		address.setVisible(false);
		
		contactnumber.setFont(new Font("Times New Roman", Font.BOLD, 17));
		contactnumber.setBounds(190, 246, 110, 41);
		adddebitpanel.add(contactnumber);
		contactnumber.setVisible(false);
	
		
		amount.setFont(new Font("Times New Roman", Font.BOLD, 17));
		amount.setBounds(171, 298, 129, 41);
		adddebitpanel.add(amount);
		amount.setVisible(false);
		
		
		
		Amount.setFont(new Font("SansSerif", Font.BOLD, 15));
		Amount.setBounds(312, 302, 104, 28);
		adddebitpanel.add(Amount);
		Amount.setColumns(10);
		Amount.setVisible(false);
		Amount.setDocument(new TextFieldNumbersOnlyLimit(6));
		
		
		
		firstnametext = new JTextField();
		firstnametext.setFont(new Font("SansSerif", Font.BOLD, 15));
		firstnametext.setBounds(312, 93, 263, 28);
		adddebitpanel.add(firstnametext);
		firstnametext.setColumns(10);
		firstnametext.setVisible(false);
		firstnametext.setDocument(new JTextFieldLettersOnlyLimit(20));
		
		
		lastnametext = new JTextField();
		lastnametext.setFont(new Font("SansSerif", Font.BOLD, 15));
		lastnametext.setColumns(10);
		lastnametext.setBounds(312, 147, 263, 28);
		adddebitpanel.add(lastnametext);
		lastnametext.setVisible(false);
		lastnametext.setDocument(new JTextFieldLettersOnlyLimit(20));
		
		
	
		addresstext.setFont(new Font("SansSerif", Font.BOLD, 15));
		addresstext.setColumns(10);
		addresstext.setBounds(312, 200, 340, 28);
		adddebitpanel.add(addresstext);
		addresstext.setVisible(false);
		addresstext.setDocument(new JTextFieldLimit(40));
		
		
	
		contactnumbertext.setFont(new Font("SansSerif", Font.BOLD, 15));
		contactnumbertext.setColumns(10);
		contactnumbertext.setBounds(312, 253, 160, 28);
		adddebitpanel.add(contactnumbertext);
		contactnumbertext.setVisible(false);
		contactnumbertext.setDocument(new TextFieldContactNumberOnlyLimit(11));
		
	
		
		
		btnSubmit.setBounds(193, 367, 182, 52);
		adddebitpanel.add(btnSubmit);
		btnSubmit.setVisible(false);
		
		
		btnCancel.setBounds(584, 367, 182, 52);
		adddebitpanel.add(btnCancel);
		btnCancel.setVisible(false);
		
		personalinfopic.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\infoicon.png"));
		personalinfopic.setBounds(677, 115, 182, 200);
		adddebitpanel.add(personalinfopic);
		personalinfopic.setVisible(false);
		
		
		
		
		
		
		Search.setBounds(10, 201, 97, 33);
		Search.setVisible(false);
		adddebitpanel.add(Search);
		
		firstnamesearchexistingbutton.setFont(new Font("SansSerif", Font.BOLD, 13));
		firstnamesearchexistingbutton.setBounds(90, 97, 134, 30);
		adddebitpanel.add(firstnamesearchexistingbutton);
		firstnamesearchexistingbutton.setColumns(10);
		firstnamesearchexistingbutton.setDocument(new JTextFieldLettersOnlyLimit(20));
		firstnamesearchexistingbutton.setVisible(false);
		
		lastnamesearchexistingbutton.setFont(new Font("SansSerif", Font.BOLD, 13));
		lastnamesearchexistingbutton.setColumns(10);
		lastnamesearchexistingbutton.setBounds(90, 144, 134, 30);
		lastnamesearchexistingbutton.setDocument(new JTextFieldLettersOnlyLimit(20));	
		adddebitpanel.add(lastnamesearchexistingbutton);
		lastnamesearchexistingbutton.setVisible(false);
		
		firstnameexistingbutton.setFont(new Font("SansSerif", Font.BOLD, 12));
		firstnameexistingbutton.setBounds(0, 97, 85, 30);
		adddebitpanel.add(firstnameexistingbutton);
		firstnameexistingbutton.setVisible(false);
		
		lastnamexistingbutton.setFont(new Font("SansSerif", Font.BOLD, 12));
		lastnamexistingbutton.setBounds(0, 143, 85, 30);
		adddebitpanel.add(lastnamexistingbutton);
		lastnamexistingbutton.setVisible(false);
		
		DONE.setBounds(54, 288, 116, 44);
		DONE.setVisible(false);
		adddebitpanel.add(DONE);
		
		RESET.setBounds(127, 201, 97, 33);
		RESET.setVisible(false);
		adddebitpanel.add(RESET);
		
		CANCEL.setBounds(54, 362, 116, 44);
		CANCEL.setVisible(false);
		adddebitpanel.add(CANCEL);
		
		scrollPane.setBounds(236, 90, 715, 356);
		adddebitpanel.add(scrollPane);
		scrollPane.setVisible(false);
		
		table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		table.setForeground(Color.BLACK);
		table.getTableHeader().setReorderingAllowed(false);	
		scrollPane.setViewportView(table);
		
		
		saerchcustomerexisting.setFont(new Font("SansSerif", Font.BOLD, 19));
		saerchcustomerexisting.setBounds(10, 63, 214, 30);
		adddebitpanel.add(saerchcustomerexisting);
		saerchcustomerexisting.setVisible(false);
		
		LISTOFCUSTOMERS.setForeground(Color.BLACK);
		LISTOFCUSTOMERS.setFont(new Font("SansSerif", Font.BOLD, 34));
		LISTOFCUSTOMERS.setBounds(425, 39, 365, 44);
		adddebitpanel.add(LISTOFCUSTOMERS);
		LISTOFCUSTOMERS.setVisible(false);
		
		
		adddebitpersonalinfo.setFont(new Font("SansSerif", Font.BOLD, 18));
		adddebitpersonalinfo.setBounds(90, 24, 367, 47);
		adddebitpanel.add(adddebitpersonalinfo);
		adddebitpersonalinfo.setVisible(false);
		
		
		adddebitfirstname.setFont(new Font("SansSerif", Font.BOLD, 17));
		adddebitfirstname.setBounds(10, 102, 110, 37);
		adddebitfirstname.setVisible(false);
		adddebitpanel.add(adddebitfirstname);
		
		adddebitlastname.setFont(new Font("SansSerif", Font.BOLD, 17));
		adddebitlastname.setBounds(10, 150, 110, 37);
		adddebitlastname.setVisible(false);
		adddebitpanel.add(adddebitlastname);
		
		adddebitaddress.setFont(new Font("SansSerif", Font.BOLD, 17));
		adddebitaddress.setBounds(10, 198, 110, 37);
		adddebitaddress.setVisible(false);
		adddebitpanel.add(adddebitaddress);	
		
		adddebitcontact.setFont(new Font("SansSerif", Font.BOLD, 17));
		adddebitcontact.setBounds(10, 246, 110, 37);
		adddebitcontact.setVisible(false);
		adddebitpanel.add(adddebitcontact);
		
		addebitfirstnametext.setFont(new Font("SansSerif", Font.BOLD, 18));
		addebitfirstnametext.setBounds(130, 102, 228, 37);
		addebitfirstnametext.setVisible(false);
		adddebitpanel.add(addebitfirstnametext);
		
		adddebitlastnametext.setFont(new Font("SansSerif", Font.BOLD, 18));
		adddebitlastnametext.setBounds(130, 150, 228, 37);
		adddebitlastnametext.setVisible(false);
		adddebitpanel.add(adddebitlastnametext);
		
			adddebitaddresstext.setFont(new Font("SansSerif", Font.BOLD, 14));
			adddebitaddresstext.setBounds(130, 198, 385, 37);
			adddebitaddresstext.setVisible(false);
			adddebitpanel.add(adddebitaddresstext);
		
		adddebitcontacttext.setFont(new Font("SansSerif", Font.BOLD, 18));
		adddebitcontacttext.setBounds(130, 246, 228, 37);
		adddebitcontacttext.setVisible(false);
		adddebitpanel.add(adddebitcontacttext);
		
		adddebitdebtinformation.setFont(new Font("SansSerif", Font.BOLD, 18));
		adddebitdebtinformation.setBounds(592, 24, 367, 47);
		adddebitdebtinformation.setVisible(false);
		adddebitpanel.add(adddebitdebtinformation);
		
		
		
		
		addebitcancel.setBounds(328, 389, 165, 47);
		addebitcancel.setVisible(false);
		adddebitpanel.add(addebitcancel);
		
		
		
		ADDDEBITREFRESH.setBounds(706, 419, 135, 47);
		ADDDEBITREFRESH.setVisible(false);
		adddebitpanel.add(ADDDEBITREFRESH);
		ADDDEBITREFRESH.setVisible(false);
	
		
		
		table_1.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		table_1.setForeground(Color.BLACK);
		table_1.getTableHeader().setReorderingAllowed(false);
		
		
		
		ADDDEBITDEBTINFOTABLE.setBounds(568, 70, 391, 343);
		ADDDEBITDEBTINFOTABLE.setVisible(false);
		adddebitpanel.add(ADDDEBITDEBTINFOTABLE);
		ADDDEBITDEBTINFOTABLE.setViewportView(table_1);
		ADDDEBITDEBTINFOTABLE.setVisible(false);
		
		
		YESBUTTONFORADDINGDEBT.setBounds(72, 389, 165, 47);
		adddebitpanel.add(YESBUTTONFORADDINGDEBT);
		YESBUTTONFORADDINGDEBT.setVisible(false);
		
		QUESTIONFORADDINGDEBT.setFont(new Font("SansSerif", Font.BOLD, 17));
		QUESTIONFORADDINGDEBT.setBounds(41, 294, 505, 84);
		adddebitpanel.add(QUESTIONFORADDINGDEBT);
		QUESTIONFORADDINGDEBT.setVisible(false);
		
		backgroundinside.setBounds(0, 0, 1011, 477);
		adddebitpanel.add(backgroundinside);
		
		backgroundinside.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\backgroundjpanel.png"));
		
		
		ADDDEBT.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\add debit.png"));
		ADDDEBT.setBackground(SystemColor.inactiveCaptionBorder);
		ADDDEBT.setBounds(270, 11, 199, 139);
		frame.getContentPane().add(ADDDEBT);
		ADDDEBT.setToolTipText("Add Debt");
		pack();
	}
	
	
	
	public void getVIEWATTRIBUTES()
	{
		viewpanel.setBounds(67, 170, 1011, 477);
		frame.getContentPane().add(viewpanel);
		viewpanel.setLayout(null);
		viewpanel.setVisible(false);
		
		ViewSearch.setBounds(10, 201, 97, 33);
		viewpanel.add(ViewSearch);
		
		
		ViewResetButton.setBounds(127, 201, 97, 33);
		viewpanel.add(ViewResetButton);

		Viewfirstnamefield.setFont(new Font("SansSerif", Font.BOLD, 13));
		Viewfirstnamefield.setBounds(90, 97, 134, 30);
		Viewfirstnamefield.setColumns(10);
		Viewfirstnamefield.setDocument(new JTextFieldLettersOnlyLimit(20));
		viewpanel.add(Viewfirstnamefield);



		Viewlastnamefield.setFont(new Font("SansSerif", Font.BOLD, 13));
		Viewlastnamefield.setColumns(10);
		Viewlastnamefield.setBounds(90, 144, 134, 30);
		Viewlastnamefield.setDocument(new JTextFieldLettersOnlyLimit(20));	
		viewpanel.add(Viewlastnamefield);


		Viewfistnamelabel.setFont(new Font("SansSerif", Font.BOLD, 12));
		Viewfistnamelabel.setBounds(0, 97, 85, 30);
		viewpanel.add(Viewfistnamelabel);
		
		Viewlastnamelabel.setFont(new Font("SansSerif", Font.BOLD, 12));
		Viewlastnamelabel.setBounds(0, 143, 85, 30);
		viewpanel.add(Viewlastnamelabel);


		ViewtableScrollpane.setBounds(234, 74, 765, 356);
		viewpanel.add(ViewtableScrollpane);

		Viewtable.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		Viewtable.setForeground(Color.BLUE);
		Viewtable.getTableHeader().setReorderingAllowed(false);
		ViewtableScrollpane.setViewportView(Viewtable);
		

		Viewsearchcustomerlabel.setFont(new Font("SansSerif", Font.BOLD, 19));
		Viewsearchcustomerlabel.setBounds(10, 63, 214, 30);
		viewpanel.add(Viewsearchcustomerlabel);
		
		Viewlistofcustomers.setForeground(Color.BLACK);
		Viewlistofcustomers.setFont(new Font("SansSerif", Font.BOLD, 34));
		Viewlistofcustomers.setBounds(422, 24, 365, 44);
		viewpanel.add(Viewlistofcustomers);
		
		ViewSelectedpersonalinfo.setFont(new Font("SansSerif", Font.BOLD, 18));
		ViewSelectedpersonalinfo.setBounds(71, 25, 367, 47);
		viewpanel.add(ViewSelectedpersonalinfo);
		ViewSelectedpersonalinfo.setVisible(false);

		ViewSelectedfirstname.setFont(new Font("SansSerif", Font.BOLD, 17));
		ViewSelectedfirstname.setBounds(10, 102, 110, 37);
		ViewSelectedfirstname.setVisible(false);
		viewpanel.add(ViewSelectedfirstname);

		ViewSelectedlastname.setFont(new Font("SansSerif", Font.BOLD, 17));
		ViewSelectedlastname.setBounds(10, 150, 110, 37);
		ViewSelectedlastname.setVisible(false);
		viewpanel.add(ViewSelectedlastname);


		ViewSelectedaddress.setFont(new Font("SansSerif", Font.BOLD, 17));
		ViewSelectedaddress.setBounds(10, 198, 110, 37);
		ViewSelectedaddress.setVisible(false);
		viewpanel.add(ViewSelectedaddress);	




		ViewSelectedcontactno.setFont(new Font("SansSerif", Font.BOLD, 17));
		ViewSelectedcontactno.setBounds(10, 246, 110, 37);
		ViewSelectedcontactno.setVisible(false);
		viewpanel.add(ViewSelectedcontactno);



		ViewSelectedfirstnametext.setFont(new Font("SansSerif", Font.BOLD, 18));
		ViewSelectedfirstnametext.setBounds(130, 102, 228, 37);
		ViewSelectedfirstnametext.setVisible(false);
		viewpanel.add(ViewSelectedfirstnametext);


		ViewSelectedlastnametext.setFont(new Font("SansSerif", Font.BOLD, 18));
		ViewSelectedlastnametext.setBounds(130, 150, 228, 37);
		ViewSelectedlastnametext.setVisible(false);
		viewpanel.add(ViewSelectedlastnametext);

		ViewSelectedaddresstext.setFont(new Font("SansSerif", Font.BOLD, 14));
		ViewSelectedaddresstext.setBounds(130, 198, 345, 37);
		ViewSelectedaddresstext.setVisible(false);
		viewpanel.add(ViewSelectedaddresstext);

		ViewSelectedcontacttext.setFont(new Font("SansSerif", Font.BOLD, 18));
		ViewSelectedcontacttext.setBounds(130, 246, 228, 37);
		ViewSelectedcontacttext.setVisible(false);
		viewpanel.add(ViewSelectedcontacttext);
		
		
		ViewDebtinformationtable.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		ViewDebtinformationtable.setForeground(Color.BLACK);
		ViewDebtinformationtable.getTableHeader().setReorderingAllowed(false);	
		
		ViewDebtinformationscrollpane.setBounds(485, 44, 494, 188);
		viewpanel.add(ViewDebtinformationscrollpane);
		ViewDebtinformationscrollpane.setViewportView(ViewDebtinformationtable);
		ViewDebtinformationscrollpane.setVisible(false);
		
		ViewtablePaymentHistory.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		ViewtablePaymentHistory.setForeground(Color.BLACK);
		ViewtablePaymentHistory.getTableHeader().setReorderingAllowed(false);	
		
		ViewPaymentHistoryScrollpane.setBounds(485, 265, 494, 209);
		viewpanel.add(ViewPaymentHistoryScrollpane);
		ViewPaymentHistoryScrollpane.setViewportView(ViewtablePaymentHistory);
		ViewPaymentHistoryScrollpane.setVisible(false);
		
		ViewSelectedBalance.setFont(new Font("SansSerif", Font.BOLD, 17));
		ViewSelectedBalance.setBounds(10, 290, 110, 37);
		viewpanel.add(ViewSelectedBalance);
		ViewSelectedBalance.setVisible(false);
		
		ViewPaymentHistory.setFont(new Font("SansSerif", Font.BOLD, 26));
		ViewPaymentHistory.setBounds(556, 238, 367, 24);
		viewpanel.add(ViewPaymentHistory);
		ViewPaymentHistory.setVisible(false);
		
		ViewDebtInformation.setFont(new Font("SansSerif", Font.BOLD, 30));
		ViewDebtInformation.setBounds(556, 9, 367, 24);
		viewpanel.add(ViewDebtInformation);
		ViewDebtInformation.setVisible(false);
		
		ViewSelectedbalanceText.setFont(new Font("SansSerif", Font.BOLD, 18));
		ViewSelectedbalanceText.setBounds(130, 290, 110, 37);
		ViewSelectedbalanceText.setVisible(false);
		viewpanel.add(ViewSelectedbalanceText);
		ViewSelectedbalanceText.setVisible(false);
		
		Vieweditinformation.setBounds(35, 359, 183, 68);
		viewpanel.add(Vieweditinformation);
		Vieweditinformation.setVisible(false);
		
		ViewReturnButton.setBounds(255, 359, 183, 68);
		viewpanel.add(ViewReturnButton);
		ViewReturnButton.setVisible(false);
		
		backgroundinside1.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\backgroundjpanel.png"));
		backgroundinside1.setBounds(0, 0, 1011, 477);
		viewpanel.add(backgroundinside1);
		
		VIEW.setBackground(Color.WHITE);
		VIEW.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\view icon.png"));
		VIEW.setBounds(473, 11, 199, 139);
		frame.getContentPane().add(VIEW);
		VIEW.setToolTipText("View");
		pack();
	}
	
	
	public void getBILLOUTATTRIBUTES()
	{
		billoutpanel.setBounds(67, 170, 1011, 477);
		frame.getContentPane().add(billoutpanel);
		billoutpanel.setLayout(null);
		billoutpanel.setVisible(false);
		PaymentDownPaymentBtn.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\partialpayment.gif"));
		
		
		PaymentDownPaymentBtn.setFont(new Font("SansSerif", Font.BOLD, 21));
		PaymentDownPaymentBtn.setBounds(522, 49, 415, 319);
		billoutpanel.add(PaymentDownPaymentBtn);
		PaymentFullpaymentBtn.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\fullpayment.gif"));
		
		PaymentFullpaymentBtn.setFont(new Font("SansSerif", Font.BOLD, 21));
		PaymentFullpaymentBtn.setBounds(69, 49, 415, 319);
		billoutpanel.add(PaymentFullpaymentBtn);
		
		PaymentFullSearchBtn.setBounds(10, 201, 97, 33);
		PaymentFullSearchBtn.setVisible(false);
		billoutpanel.add(PaymentFullSearchBtn);
		
		PaymentFullfirstnameTextfield.setFont(new Font("SansSerif", Font.BOLD, 13));
		PaymentFullfirstnameTextfield.setBounds(90, 97, 134, 30);
		billoutpanel.add(PaymentFullfirstnameTextfield);
		PaymentFullfirstnameTextfield.setColumns(10);
		PaymentFullfirstnameTextfield.setDocument(new JTextFieldLettersOnlyLimit(20));
		PaymentFullfirstnameTextfield.setVisible(false);
		
		PaymentFullLastnameTextfield.setFont(new Font("SansSerif", Font.BOLD, 13));
		PaymentFullLastnameTextfield.setColumns(10);
		PaymentFullLastnameTextfield.setBounds(90, 144, 134, 30);
		PaymentFullLastnameTextfield.setDocument(new JTextFieldLettersOnlyLimit(20));	
		billoutpanel.add(PaymentFullLastnameTextfield);
		PaymentFullLastnameTextfield.setVisible(false);
		
		PaymentFullFirstnamelabel.setFont(new Font("SansSerif", Font.BOLD, 12));
		PaymentFullFirstnamelabel.setBounds(0, 97, 85, 30);
		billoutpanel.add(PaymentFullFirstnamelabel);
		PaymentFullFirstnamelabel.setVisible(false);
		
		PaymentFulllastnamelabel.setFont(new Font("SansSerif", Font.BOLD, 12));
		PaymentFulllastnamelabel.setBounds(0, 143, 85, 30);
		billoutpanel.add(PaymentFulllastnamelabel);
		PaymentFulllastnamelabel.setVisible(false);
		
		PaymentFullDoneBtn.setBounds(54, 288, 116, 44);
		PaymentFullDoneBtn.setVisible(false);
		billoutpanel.add(PaymentFullDoneBtn);
		
		PaymentFullResetBtn.setBounds(127, 201, 97, 33);
		PaymentFullResetBtn.setVisible(false);
		billoutpanel.add(PaymentFullResetBtn);
		
		PaymentFullCancelBtn.setBounds(54, 362, 116, 44);
		PaymentFullCancelBtn.setVisible(false);
		billoutpanel.add(PaymentFullCancelBtn);
		
		PaymentFullTableScrollpane.setBounds(236, 90, 715, 356);
		billoutpanel.add(PaymentFullTableScrollpane);
		PaymentFullTableScrollpane.setVisible(false);
		
		PaymentFullTable.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		PaymentFullTable.setForeground(Color.BLACK);
		PaymentFullTable.getTableHeader().setReorderingAllowed(false);	
		PaymentFullTableScrollpane.setViewportView(PaymentFullTable);
		
		
		PaymentFullSearchCustomertext.setFont(new Font("SansSerif", Font.BOLD, 19));
		PaymentFullSearchCustomertext.setBounds(10, 63, 214, 30);
		billoutpanel.add(PaymentFullSearchCustomertext);
		PaymentFullSearchCustomertext.setVisible(false);
		
		PaymentFullListCofCustomers.setForeground(Color.BLACK);
		PaymentFullListCofCustomers.setFont(new Font("SansSerif", Font.BOLD, 34));
		PaymentFullListCofCustomers.setBounds(425, 39, 365, 44);
		billoutpanel.add(PaymentFullListCofCustomers);
		PaymentFullListCofCustomers.setVisible(false);
		
		PaymentPartialSearchBtn.setBounds(10, 201, 97, 33);
		PaymentPartialSearchBtn.setVisible(false);
		billoutpanel.add(PaymentPartialSearchBtn);
		
		PaymentPartialfirstnameTextfield.setFont(new Font("SansSerif", Font.BOLD, 13));
		PaymentPartialfirstnameTextfield.setBounds(90, 97, 134, 30);
		billoutpanel.add(PaymentPartialfirstnameTextfield);
		PaymentPartialfirstnameTextfield.setColumns(10);
		PaymentPartialfirstnameTextfield.setDocument(new JTextFieldLettersOnlyLimit(20));
		PaymentPartialfirstnameTextfield.setVisible(false);
		
		PaymentPartialLastnameTextfield.setFont(new Font("SansSerif", Font.BOLD, 13));
		PaymentPartialLastnameTextfield.setColumns(10);
		PaymentPartialLastnameTextfield.setBounds(90, 144, 134, 30);
		PaymentPartialLastnameTextfield.setDocument(new JTextFieldLettersOnlyLimit(20));	
		billoutpanel.add(PaymentPartialLastnameTextfield);
		PaymentPartialLastnameTextfield.setVisible(false);
		
		PaymentPartialFirstnamelabel.setFont(new Font("SansSerif", Font.BOLD, 12));
		PaymentPartialFirstnamelabel.setBounds(0, 97, 85, 30);
		billoutpanel.add(PaymentPartialFirstnamelabel);
		PaymentPartialFirstnamelabel.setVisible(false);
		
		PaymentPartiallastnamelabel.setFont(new Font("SansSerif", Font.BOLD, 12));
		PaymentPartiallastnamelabel.setBounds(0, 143, 85, 30);
		billoutpanel.add(PaymentPartiallastnamelabel);
		PaymentPartiallastnamelabel.setVisible(false);
		
		
		PaymentPartialDoneBtn.setBounds(54, 288, 116, 44);
		PaymentPartialDoneBtn.setVisible(false);
		billoutpanel.add(PaymentPartialDoneBtn);
		
		
		PaymentPartialResetBtn.setBounds(127, 201, 97, 33);
		PaymentPartialResetBtn.setVisible(false);
		billoutpanel.add(PaymentPartialResetBtn);
		
		
		PaymentPartialCancelBtn.setBounds(54, 362, 116, 44);
		PaymentPartialCancelBtn.setVisible(false);
		billoutpanel.add(PaymentPartialCancelBtn);
		
		PaymentPartialTableScrollpane.setBounds(236, 90, 715, 356);
		billoutpanel.add(PaymentPartialTableScrollpane);
		PaymentPartialTableScrollpane.setVisible(false);
		
		PaymentPartialTable.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		PaymentPartialTable.setForeground(Color.BLACK);
		PaymentPartialTable.getTableHeader().setReorderingAllowed(false);	
		PaymentPartialTableScrollpane.setViewportView(PaymentPartialTable);
		
		
		PaymentPartialSearchCustomertext.setFont(new Font("SansSerif", Font.BOLD, 19));
		PaymentPartialSearchCustomertext.setBounds(10, 63, 214, 30);
		billoutpanel.add(PaymentPartialSearchCustomertext);
		PaymentPartialSearchCustomertext.setVisible(false);
		
		PaymentPartialListCofCustomers.setForeground(Color.BLACK);
		PaymentPartialListCofCustomers.setFont(new Font("SansSerif", Font.BOLD, 34));
		PaymentPartialListCofCustomers.setBounds(425, 39, 365, 44);
		billoutpanel.add(PaymentPartialListCofCustomers);
		PaymentPartialListCofCustomers.setVisible(false);
		
		backgroundinside2.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\backgroundjpanel.png"));
		backgroundinside2.setBounds(0, 0, 1011, 477);
		billoutpanel.add(backgroundinside2);
		
		BILLOUT.setBackground(new Color(255, 255, 255));
		BILLOUT.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\bill out.png"));
		BILLOUT.setBounds(676, 11, 199, 139);
		frame.getContentPane().add(BILLOUT);
		BILLOUT.setToolTipText("PAYMENT TRANSACTION");
		pack();
		
		
	}
	
	public void getABOUTUSATTRIBUTES()
	{
		aboutuspanel.setBounds(67, 170, 1011, 477);
		frame.getContentPane().add(aboutuspanel);
		aboutuspanel.setLayout(null);
		aboutuspanel.setVisible(false);
		lblNewLabel.setBounds(0, 0, 1011, 154);
		aboutuspanel.add(lblNewLabel);
		
		
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\doms\\Downloads\\imageedit_1_2073454868.gif"));
		lblNewLabel.setVisible(true);
		
		
		lblrenmart.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblrenmart.setBounds(10, 177, 330, 40);
		aboutuspanel.add(lblrenmart);
		lblrenmart.setVisible(true);
		
		
		lblDatabaseAnalystArt.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblDatabaseAnalystArt.setBounds(10, 226, 330, 40);
		aboutuspanel.add(lblDatabaseAnalystArt);
		lblDatabaseAnalystArt.setVisible(true);
		
		
		lblFrontendDeveloperDiomede.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblFrontendDeveloperDiomede.setBounds(10, 273, 330, 40);
		aboutuspanel.add(lblFrontendDeveloperDiomede);
		lblFrontendDeveloperDiomede.setVisible(true);
		
		
		lblBackendDeveloperSear.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblBackendDeveloperSear.setBounds(10, 324, 330, 40);
		aboutuspanel.add(lblBackendDeveloperSear);
		lblBackendDeveloperSear.setVisible(true);
		
		
		lblTe.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblTe.setBounds(10, 375, 330, 40);
		aboutuspanel.add(lblTe);
		lblTe.setVisible(true);
		
		lblTesterReylin.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblTesterReylin.setBounds(10, 426, 297, 40);
		aboutuspanel.add(lblTesterReylin);
		lblTesterReylin.setVisible(true);
		
		
		motoLbl.setHorizontalAlignment(SwingConstants.CENTER);
		motoLbl.setFont(new Font("Times New Roman", Font.BOLD, 22));
		motoLbl.setBounds(400, 192, 565, 51);
		aboutuspanel.add(motoLbl);
		motoLbl.setVisible(true);
		
		
		lblatech.setBounds(783, 233, 102, 22);
		lblatech.setHorizontalAlignment(SwingConstants.CENTER);
		aboutuspanel.add(lblatech);
		motoLbl.setVisible(true);
		
		
		contactlbl.setHorizontalAlignment(SwingConstants.RIGHT);
		contactlbl.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		contactlbl.setBounds(772, 444, 229, 22);
		aboutuspanel.add(contactlbl);
		motoLbl.setVisible(true);
		
		
		backgroundinside3.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\backgroundjpanel.png"));
		backgroundinside3.setBounds(0, 0, 1011, 477);
		aboutuspanel.add(backgroundinside3);
	
		

		ABOUTUS.setBackground(Color.WHITE);
		ABOUTUS.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\about-us.png"));
		ABOUTUS.setBounds(879, 11, 199, 139);
		frame.getContentPane().add(ABOUTUS);
		ABOUTUS.setToolTipText("About us");
		pack();
		
	}
	
	public class JTextFieldLimit extends PlainDocument {
		  private int limit;
		  JTextFieldLimit(int limit) {
		    super();
		    this.limit = limit;
		  }

		  JTextFieldLimit(int limit, boolean upper) {
		    super();
		    this.limit = limit;
		  }

		  public void insertString(int offset, String str, javax.swing.text.AttributeSet attr) throws BadLocationException {
		    if (str == null)
		      return;
	
		    
		    if ((getLength() + str.length()) <= limit) {
		      super.insertString(offset, str, attr);
		    }
		  }
		}
	
	public class TextFieldNumbersOnlyLimit extends PlainDocument {
		  private int limit;
		  TextFieldNumbersOnlyLimit(int limit) {
		    super();
		    this.limit = limit;
		  }

		  TextFieldNumbersOnlyLimit(int limit, boolean upper) {
		    super();
		    this.limit = limit;
		  }

		  public void insertString(int offset, String str, javax.swing.text.AttributeSet attr) throws BadLocationException {
		    if (str == null)
		      return;
		    
		    char[] chars = str.toCharArray();
            boolean ok = true;
            
            
            for ( int i = 0; i < chars.length; i++ ) 
            {

                try {
                	if(String.valueOf(chars[i]).equals("."))
                	{
                		
                		continue;
                	}
                	else
                	{
                		 Integer.parseInt( String.valueOf( chars[i] ) );
                	}
                   
                } catch ( NumberFormatException exc ) {
                    ok = false;
                    break;
                }


            }
		    
		    if ((getLength() + str.length()) <= limit && ok) 
		    {
		      super.insertString(offset, str, attr);
		    }
		  }
		}
	
	
	public class TextFieldContactNumberOnlyLimit extends PlainDocument {
		  private int limit;
		  TextFieldContactNumberOnlyLimit(int limit) {
		    super();
		    this.limit = limit;
		  }

		  TextFieldContactNumberOnlyLimit(int limit, boolean upper) {
		    super();
		    this.limit = limit;
		  }

		  public void insertString(int offset, String str, javax.swing.text.AttributeSet attr) throws BadLocationException {
		    if (str == null)
		      return;
		    
		    char[] chars = str.toCharArray();
            boolean ok = true;
          
          
          for ( int i = 0; i < chars.length; i++ ) 
          {

              try {
              	
              		 Integer.parseInt( String.valueOf( chars[i] ) );
              
                 
              } catch ( NumberFormatException exc ) {
                  ok = false;
                  break;
              }


          }
		    
		    if ((getLength() + str.length()) <= limit && ok) 
		    {
		      super.insertString(offset, str, attr);
		    }
		  }
		}
	
	public class JTextFieldLettersOnlyLimit extends PlainDocument {
		  private int limit;
		  JTextFieldLettersOnlyLimit(int limit) {
		    super();
		    this.limit = limit;
		  }

		  JTextFieldLettersOnlyLimit(int limit, boolean upper) {
		    super();
		    this.limit = limit;
		  }

		  public void insertString(int offset, String str, javax.swing.text.AttributeSet attr) throws BadLocationException {
		    if (str == null)
		      return;
		    
		    
		    char[] chars = str.toCharArray();
          boolean ok = false;
        
        
        for ( int i = 0; i < chars.length; i++ ) {

            try {
            	
               Integer.parseInt( String.valueOf( chars[i] ) );
            
               
            } catch ( NumberFormatException exc ) {
                int z;
                char l = 'a', n = 'A';
            	
                for(z = 0; z<=26; z++)
                {
                	if(l == chars[i] || n == chars[i] || ' ' == chars[i] )
                	{
                		ok = true;
                		
                	}
                	
                n++;
                l++;
                }

                
                break;
            }


        }
		    
		    if ((getLength() + str.length()) <= limit && ok) {
		      super.insertString(offset, str, attr);
		    }
		  }
		}
	
	
	
}
