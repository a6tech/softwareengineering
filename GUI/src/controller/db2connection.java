package controller;

import java.sql.*;
import javax.swing.JOptionPane;

public class db2connection
{

	
	public static Connection dbConnection() 
	{
		try
		{
	    Class.forName("com.ibm.db2.jcc.DB2Driver");
	    Connection conn = DriverManager.getConnection("jdbc:db2://localhost:50000/DEBT", "db2admin", "db2admin");
		return conn;
		}
		catch(ClassNotFoundException | SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
}
