package Model;
	
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.fxml.FXMLLoader;


public class AdministratorModel extends Application {
	 public static Stage adminStage = new Stage();
	
	public void start(Stage primaryStage) 
	{
		try {
			VBox root = (VBox)FXMLLoader.load(getClass().getResource("/view/AdministratorGUI.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/view/AdministratorDesign.css").toExternalForm());
			scene.setCursor(Cursor.DEFAULT);
			primaryStage.setScene(scene);
			primaryStage.setTitle("AdministratorDebtInformation");
			primaryStage.setResizable(false);
			primaryStage.initStyle(StageStyle.UNDECORATED);
			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			    @Override
			    public void handle(WindowEvent event) {
			        event.consume();
			    }
			});
			AdministratorModel.adminStage = primaryStage;
			adminStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void closeAdmin()
	{
		adminStage.hide();
	}
	public void openAdmin()
	{
		adminStage.show();
	}
	

	
}
