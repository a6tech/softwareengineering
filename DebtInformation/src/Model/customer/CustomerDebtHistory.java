package Model.customer;

import java.sql.Date;
import java.sql.Time;
import java.text.SimpleDateFormat;

public class CustomerDebtHistory 
{

	private int debtamount;
	private String dateadded;
	private String timeadded;
	public CustomerDebtHistory(int debtamount, String dateadded, String timeadded) {
		super();
		this.debtamount = debtamount;
		this.dateadded = dateadded;
		this.timeadded = timeadded;
	}
	public int getDebtamount() {
		return debtamount;
	}
	public void setDebtamount(int debtamount) {
		this.debtamount = debtamount;
	}
	public String getDateadded() {
		return dateadded;
	}
	public void setDateadded(String dateadded) {
		this.dateadded = dateadded;
	}
	public String getTimeadded() {
		return timeadded;
	}
	public void setTimeadded(String timeadded) {
		this.timeadded = timeadded;
	}
}
