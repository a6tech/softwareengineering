package entities;

public class Costumer 
{
	private String costumercode;
	private String firstname;
	private String lastname;
	private String address;
	private String contactnumber;
	private double debtbalance;
	private String paymentid;
	private String balanceid;
	private double paymentamount;
	private double balanceNewCustomer;
	private String Viewbalance;
	private double balance;
	
	
	
	
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getViewbalance() {
		return Viewbalance;
	}
	public void setViewbalance(String viewbalance) {
		Viewbalance = viewbalance;
	}
	public double getBalanceNewCustomer() {
		return balanceNewCustomer;
	}
	public void setBalanceNewCustomer(double balanceNewCustomer) {
		
		
		this.balanceNewCustomer = balanceNewCustomer;
	}
	public String getContactnumber() {
		return contactnumber;
	}
	public void setContactnumber(String contactnumber) {
		this.contactnumber = contactnumber;
	}
	public String getCostumercode() {
		return costumercode;
	}
	public void setCostumercode(String costumercode) {
		this.costumercode = costumercode;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public double getDebtbalance() {
		return debtbalance;
	}
	public void setDebtbalance(double debtbalance) {
		this.debtbalance = debtbalance;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPaymentid() {
		return paymentid;
	}
	public void setPaymentid(String paymentid) {
		this.paymentid = paymentid;
	}
	public String getBalanceid() {
		return balanceid;
	}
	public void setBalanceid(String balanceid) {
		this.balanceid = balanceid;
	}
	public double getPaymentamount() {
		return paymentamount;
	}
	public void setPaymentamount(double paymentamount) {
		this.paymentamount = paymentamount;
	}
	
	
}
