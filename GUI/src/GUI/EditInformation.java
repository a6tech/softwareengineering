package GUI;

import java.awt.*;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import GUI.UI.JTextFieldLimit;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class EditInformation extends JFrame {

	public JFrame EditFrame = new JFrame();
	public JTextField ViewEditfirstnametext = new JTextField("",SwingConstants.CENTER);
	public JTextField ViewEditlastnametext = new JTextField("",SwingConstants.CENTER);
	public JLabel paymentfullTransactionbackgroundinside = new JLabel("New label");
	public JLabel PaymentFullTransactionCompanyLabel = new JLabel("ZABALA'S CANTEEN", SwingConstants.CENTER);
	public JButton PaymentFullTransactionConfirmBtn = new JButton("CONFIRM");
	public JLabel PaymentFullTransactionCustomerNameLabel = new JLabel("CUSTOMER NAME:");
	public JLabel PaymentFullTransactionCustomerBalLabel = new JLabel("CUSTOMER BALANCE:");
	public JLabel PaymentFullTransactionPaymentAmountLabel = new JLabel("PAYMENT AMOUNT:");
	public JLabel PaymentFullTransactionCustomerNameText = new JLabel("");
	public JLabel PaymentFullTransactionCustomerBalText = new JLabel("");
	public JLabel PaymentFullTransactionPaymentAmountText = new JLabel("");
	JLabel PaymentFullTransactionChangedLabel = new JLabel("CHANGE:", SwingConstants.CENTER);
	JLabel PaymentFullTransactionChangedText = new JLabel("");
	public JLabel PaymentFullDateAndTimeText = new JLabel("");
	public JLabel PaymentFullDateAndTimeLabel = new JLabel("DATE AND TIME:");
	public EditInformation() 
	{
		
		EditFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		EditFrame.setBounds(67, 170, 500, 313);
		EditFrame.getContentPane().setLayout(null);
		
		
		PaymentFullTransactionCompanyLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		PaymentFullTransactionCompanyLabel.setForeground(Color.WHITE);
		PaymentFullTransactionCompanyLabel.setBounds(29, 11, 364, 23);
		EditFrame.getContentPane().add(PaymentFullTransactionCompanyLabel);
		
		
		
		PaymentFullTransactionConfirmBtn.setBounds(159, 250, 126, 38);
		EditFrame.getContentPane().add(PaymentFullTransactionConfirmBtn);
		
		
		PaymentFullTransactionCustomerNameLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		PaymentFullTransactionCustomerNameLabel.setForeground(Color.WHITE);
		PaymentFullTransactionCustomerNameLabel.setBounds(25, 94, 142, 28);
		EditFrame.getContentPane().add(PaymentFullTransactionCustomerNameLabel);
		
		
		PaymentFullTransactionCustomerBalLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		PaymentFullTransactionCustomerBalLabel.setForeground(Color.WHITE);
		PaymentFullTransactionCustomerBalLabel.setBounds(25, 133, 142, 28);
		EditFrame.getContentPane().add(PaymentFullTransactionCustomerBalLabel);
		
		
		PaymentFullTransactionPaymentAmountLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		PaymentFullTransactionPaymentAmountLabel.setForeground(Color.WHITE);
		PaymentFullTransactionPaymentAmountLabel.setBounds(29, 172, 142, 28);
		EditFrame.getContentPane().add(PaymentFullTransactionPaymentAmountLabel);
		
		
		PaymentFullTransactionCustomerNameText.setFont(new Font("Tahoma", Font.BOLD, 14));
		PaymentFullTransactionCustomerNameText.setForeground(Color.WHITE);
		PaymentFullTransactionCustomerNameText.setBounds(177, 92, 313, 30);
		EditFrame.getContentPane().add(PaymentFullTransactionCustomerNameText);
		
		
		PaymentFullTransactionCustomerBalText.setFont(new Font("Tahoma", Font.PLAIN, 16));
		PaymentFullTransactionCustomerBalText.setForeground(Color.WHITE);
		PaymentFullTransactionCustomerBalText.setBounds(177, 131, 132, 30);
		EditFrame.getContentPane().add(PaymentFullTransactionCustomerBalText);
		
		
		PaymentFullTransactionPaymentAmountText.setFont(new Font("Tahoma", Font.PLAIN, 16));
		PaymentFullTransactionPaymentAmountText.setForeground(Color.WHITE);
		PaymentFullTransactionPaymentAmountText.setBounds(177, 170, 132, 30);
		EditFrame.getContentPane().add(PaymentFullTransactionPaymentAmountText);
		
		
		PaymentFullTransactionChangedLabel.setForeground(Color.WHITE);
		PaymentFullTransactionChangedLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		PaymentFullTransactionChangedLabel.setBounds(29, 211, 142, 28);
		EditFrame.getContentPane().add(PaymentFullTransactionChangedLabel);
		
		
		PaymentFullTransactionChangedText.setForeground(Color.WHITE);
		PaymentFullTransactionChangedText.setFont(new Font("Tahoma", Font.PLAIN, 16));
		PaymentFullTransactionChangedText.setBounds(177, 211, 132, 30);
		EditFrame.getContentPane().add(PaymentFullTransactionChangedText);
		

		PaymentFullDateAndTimeLabel.setForeground(Color.WHITE);
		PaymentFullDateAndTimeLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		PaymentFullDateAndTimeLabel.setBounds(25, 63, 142, 28);
		EditFrame.getContentPane().add(PaymentFullDateAndTimeLabel);
		
		
		PaymentFullDateAndTimeText.setForeground(Color.WHITE);
		PaymentFullDateAndTimeText.setFont(new Font("Tahoma", Font.PLAIN, 16));
		PaymentFullDateAndTimeText.setBounds(177, 63, 216, 30);
		EditFrame.getContentPane().add(PaymentFullDateAndTimeText);
		

		paymentfullTransactionbackgroundinside.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\background.png"));
		paymentfullTransactionbackgroundinside.setBounds(0, 0, 500, 313);
		EditFrame.getContentPane().add(paymentfullTransactionbackgroundinside);
		
		
		EditFrame.setUndecorated(true);
		EditFrame.setResizable(false);
		EditFrame.setLocationRelativeTo(null);
		EditFrame.setVisible(false);
		
	}
	
	public class JTextFieldLimit extends PlainDocument {
		  private int limit;
		  JTextFieldLimit(int limit) {
		    super();
		    this.limit = limit;
		  }

		  JTextFieldLimit(int limit, boolean upper) {
		    super();
		    this.limit = limit;
		  }

		  public void insertString(int offset, String str, javax.swing.text.AttributeSet attr) throws BadLocationException {
		    if (str == null)
		      return;

		    if ((getLength() + str.length()) <= limit) {
		      super.insertString(offset, str, attr);
		    }
		  }
		}
}
