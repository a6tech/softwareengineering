package view;

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;
import java.awt.Cursor;

public class mainFrame extends JFrame{
	framechoose viewHeadParts = new framechoose();
	JFrame frame = new JFrame();
	public mainFrame() {
		
		setTitle("Motorcyle Problem Diagnosis System");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		setBounds(1000, 1000, 1139, 656);
		setLocationRelativeTo(null);
		final JPanel panel = new JPanel();
		getContentPane().add(panel);
		panel.setBackground(new Color(248, 248, 248));
		panel.setLayout(null);
		panel.setSize(1000, 650);
		

		final JLabel headPart = new JLabel("");
		headPart.setBounds(177, 11, 246, 212);
		panel.add(headPart);
		
		headPart.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) 
			{
				 panel.setCursor(new Cursor(Cursor.HAND_CURSOR));
			}
			@Override
			public void mouseExited(MouseEvent e) 
			{
				panel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				
				viewHeadParts.setVisible(true);
			}
		});
		
		JLabel motorcycleImage = new JLabel(new ImageIcon(mainFrame.class.getResource("/images/motor_image.jpg")));
		motorcycleImage.setBounds(10, 11, 984, 611);
		panel.add(motorcycleImage);
		motorcycleImage.setText("");
		
		final JLabel tirePart = new JLabel("");
		tirePart.setBounds(91, 248, 301, 363);
		panel.add(tirePart);
		
		tirePart.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) 
			{
				 panel.setCursor(new Cursor(Cursor.HAND_CURSOR));
			}
			@Override
			public void mouseExited(MouseEvent e) 
			{
				panel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				
			}
		});
		
		final JLabel enginePart = new JLabel("");
		enginePart.setBounds(372, 237, 220, 225);
		panel.add(enginePart);
		
		enginePart.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) 
			{
				 panel.setCursor(new Cursor(Cursor.HAND_CURSOR));
			}
			@Override
			public void mouseExited(MouseEvent e) 
			{
				panel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				
			}
		});
		
		final JLabel tireBackPart = new JLabel("");
		tireBackPart.setBounds(616, 237, 193, 285);
		panel.add(tireBackPart);
		
		tireBackPart.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) 
			{
				 panel.setCursor(new Cursor(Cursor.HAND_CURSOR));
			}
			@Override
			public void mouseExited(MouseEvent e) 
			{
				panel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
			@Override
			public void mouseClicked(MouseEvent arg0) 
			{
				
			}
		});
		
	
	}
	public static void main(String args[]){
		mainFrame f = new mainFrame();
		f.setVisible(true);
		f.setSize(1000,650);
	}
}
