package controller;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import entities.Costumer;
import entities.SEARCHBUTTON;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import GUI.UI;

public class Controler extends UI
{
	public SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm:s a");
	public SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d yyyy");
	Connection connection = db2connection.dbConnection();
	Costumer c = new Costumer();
	SEARCHBUTTON search = new SEARCHBUTTON();
	public int rowcount = 0;
	Random rndm = new Random();
	
	public Controler(){
		
		user.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				userpanel.setVisible(true);
				adddebitpanel.setVisible(false);
				viewpanel.setVisible(false);
				billoutpanel.setVisible(false);
				aboutuspanel.setVisible(false);
				
				
				Viewfirstnamefield.setText("");
				Viewlastnamefield.setText("");
			}
		});
		
		ADDDEBT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				userpanel.setVisible(false);
				adddebitpanel.setVisible(true);
				viewpanel.setVisible(false);
				billoutpanel.setVisible(false);
				aboutuspanel.setVisible(false);
				viewpanel.hide();
				saerchcustomerexisting.setVisible(false);
				
				
				Viewfirstnamefield.setText("");
				Viewlastnamefield.setText("");
			}
		});
		
		VIEW.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				userpanel.setVisible(false);
				adddebitpanel.setVisible(false);
				viewpanel.setVisible(true);
				billoutpanel.setVisible(false);
				aboutuspanel.setVisible(false);
				
				
				viewpanel.getRootPane().setDefaultButton(ViewSearch);
				
				try
				{
					String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
					PreparedStatement pst2 = connection.prepareStatement(query);
					ResultSet rs2 = pst2.executeQuery();
					
					if(rowcount != 0)
					{
						rowcount = 0;
						while(rs2.next())
						{
						    rowcount ++;
						}
					}
					else
					{
						while(rs2.next())
						{
						    rowcount ++;
						}
					}
					
					
				}catch(SQLException e1)
				{
					e1.printStackTrace();
				}
				
				Viewtable.setModel(new DefaultTableModel(
						new Object[rowcount][rowcount] ,
						new String[] {
							"FIRSTNAME", "LASTNAME", "ADDRESS", "CONTACTNO."
						}
					) {
						Class[] columnTypes = new Class[] {
							Object.class, Object.class, String.class, Object.class
						};
						public Class getColumnClass(int columnIndex) {
							return columnTypes[columnIndex];
						}
						boolean[] columnEditables = new boolean[] {
							false, false, false
						};
						public boolean isCellEditable(int row, int column) {
							return columnEditables[column];
						}
					});
				Viewtable.getColumnModel().getColumn(0).setResizable(false);
				Viewtable.getColumnModel().getColumn(0).setPreferredWidth(200);
				Viewtable.getColumnModel().getColumn(1).setResizable(false);
				Viewtable.getColumnModel().getColumn(2).setResizable(false);
				Viewtable.getColumnModel().getColumn(2).setPreferredWidth(300);
				Viewtable.getColumnModel().getColumn(3).setResizable(false);
				
			
				
					
					try
					{
					
						
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) {
				           
				         
				            Object firstname = rs2.getString("FIRSTNAME");
				            Object lastname = rs2.getString("LASTNAME");
				            Object address = rs2.getString("ADDRESS");
				            Object contactnum = rs2.getString("CONTACTNO"); 
				             
				            Viewtable.getModel().setValueAt(firstname, i, 0);
				            Viewtable.getModel().setValueAt(lastname, i, 1);
				           Viewtable.getModel().setValueAt(address, i, 2);
				          Viewtable.getModel().setValueAt(contactnum, i, 3); 
				          
				         
				        i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
			}
		});
		
		BILLOUT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				userpanel.setVisible(false);
				adddebitpanel.setVisible(false);
				viewpanel.setVisible(false);
				billoutpanel.setVisible(true);
				aboutuspanel.setVisible(false);
				
				
				Viewfirstnamefield.setText("");
				Viewlastnamefield.setText("");
			}
		});
		
		ABOUTUS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				userpanel.setVisible(false);
				adddebitpanel.setVisible(false);
				viewpanel.setVisible(false);
				billoutpanel.setVisible(false);
				aboutuspanel.setVisible(true);
				
				
				Viewlastnamefield.setText("");
			}
		});
		
		
		btnNewCostumer.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				getNewCostumerButtonListener();
			}
		});
		
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				
				getNewCostumerCancelButton();
				
			}
		});
		
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				
				getButtonSubmitforNewCostumer();
			}
		});
		
		RESET.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				
				getResetButtonExistingbutton();
					
					
			}
			
		});
				
		existingbutton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) 
		{

			getexistingbuttonFunction();			
							
			}
		});
				
				
		DONE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
						getDONEbuttonexistingbutton();
						
						
			}
		});
			
				
		CANCEL.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
						getCANCELbuttonexistingbutton();
			}
		});		
				
				
				
		
		
		Search.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				
				getSearchButtonExistingButtonFunction();
				
			}
		});
		
		table.addMouseListener(new MouseAdapter() {
			
			public void mouseClicked(MouseEvent arg0) 
			{
				 if (table.getSelectedRow() > -1) 
				 {
			         
					    scrollPane.setVisible(false);
						Search.setVisible(false);
						DONE.setVisible(false);
						RESET.setVisible(false);
						CANCEL.setVisible(false);
						LISTOFCUSTOMERS.setVisible(false);
						firstnamesearchexistingbutton.setVisible(false);
						lastnamesearchexistingbutton.setVisible(false);
						firstnameexistingbutton.setVisible(false);
						lastnamexistingbutton.setVisible(false);
						saerchcustomerexisting.setVisible(false);
						
						
						
					 adddebitpersonalinfo.setVisible(true);
					 adddebitfirstname.setVisible(true);
					 adddebitlastname.setVisible(true);
					 adddebitaddress.setVisible(true);
					 adddebitcontact.setVisible(true);
					 addebitfirstnametext.setVisible(true);
					 adddebitlastnametext.setVisible(true);
					 adddebitaddresstext.setVisible(true);
					 adddebitcontacttext.setVisible(true);
					 adddebitdebtinformation.setVisible(true);
					 addebitcancel.setVisible(true);
					 ADDDEBITREFRESH.setVisible(true);
					 ADDDEBITDEBTINFOTABLE.setVisible(true);
					 YESBUTTONFORADDINGDEBT.setVisible(true);
					 QUESTIONFORADDINGDEBT.setVisible(true);
					 
			            
						addebitfirstnametext.setText(table.getValueAt(table.getSelectedRow(), 0).toString().toUpperCase());
						adddebitlastnametext.setText(table.getValueAt(table.getSelectedRow(), 1).toString().toUpperCase());
						adddebitaddresstext.setText(table.getValueAt(table.getSelectedRow(), 2).toString().toUpperCase());
						adddebitcontacttext.setText(table.getValueAt(table.getSelectedRow(), 3).toString().toUpperCase());
						
						
						try
						{
							String query = "SELECT DEBT_AMOUNT,DATE_ADDED,TIME_ADDED FROM DEBT WHERE CUSTOMERID = "
											+ "(SELECT CUSTOMERID FROM CUSTOMER WHERE FIRSTNAME ='" + addebitfirstnametext.getText() 
											+"' AND LASTNAME ='" + adddebitlastnametext.getText() +"')";
							PreparedStatement pst2 = connection.prepareStatement(query);
							ResultSet rs2 = pst2.executeQuery();
							if(rowcount != 0)
							{
								rowcount = 0;
								while(rs2.next())
								{
								    rowcount ++;
								}
							}
							else
							{
								while(rs2.next())
								{
								    rowcount ++;
								}
							}
							
							
						}catch(SQLException e1)
						{
							e1.printStackTrace();
						}
						
						table_1.setModel(new DefaultTableModel(
								new Object[rowcount][rowcount] ,
								new String[] {
									"DEBTAMOUNT", "DATE ADDED", "TIME ADDED"
								}
							) {
								Class[] columnTypes = new Class[] {
									Object.class, Object.class, String.class, Object.class
								};
								public Class getColumnClass(int columnIndex) {
									return columnTypes[columnIndex];
								}
								boolean[] columnEditables = new boolean[] {
									false, false, false
								};
								public boolean isCellEditable(int row, int column) {
									return columnEditables[column];
								}
							});
						table_1.getColumnModel().getColumn(0).setResizable(false);
						table_1.getColumnModel().getColumn(0).setPreferredWidth(200);
						table_1.getColumnModel().getColumn(1).setResizable(false);
						table_1.getColumnModel().getColumn(2).setResizable(false);
						
						try
						{
							String query1 = "SELECT DEBT_AMOUNT,DATE_ADDED,TIME_ADDED FROM DEBT WHERE CUSTOMERID = "
											+ "(SELECT CUSTOMERID FROM CUSTOMER WHERE FIRSTNAME ='" + addebitfirstnametext.getText() 
											+"' AND LASTNAME ='" + adddebitlastnametext.getText() +"') ORDER BY DATE_ADDED,TIME_ADDED" ;
					
					
							PreparedStatement pst4 = connection.prepareStatement(query1);
							ResultSet rs4 = pst4.executeQuery();
							int i=0;
							while(rs4.next()) 
							{
						           
						         
					            Object debtamount = rs4.getString("DEBT_AMOUNT");
					            Date date = rs4.getDate("DATE_ADDED");
					            Time time = rs4.getTime("TIME_ADDED");
					            Object datetemp,timetemp;
					            
					            datetemp = dateFormat.format(date);
					            timetemp = timeFormat.format(time);
					            
					            table_1.getModel().setValueAt(debtamount, i, 0);
					            table_1.getModel().setValueAt(datetemp, i, 1);
					           table_1.getModel().setValueAt(timetemp, i, 2);
					         
					        i++;
							}
						}catch(SQLException e1)
						{
							e1.printStackTrace();
						}
						
						
			        	
				 }
			}
		});
		PaymentDownPaymentBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				user.setEnabled(false);
				ADDDEBT.setEnabled(false);
				VIEW.setEnabled(false);	
				BILLOUT.setEnabled(false);	
				ABOUTUS.setEnabled(false);
				billoutpanel.getRootPane().setDefaultButton(PaymentPartialSearchBtn);
				PaymentDownPaymentBtn.setVisible(false);
				PaymentFullpaymentBtn.setVisible(false);
				
				PaymentPartialSearchBtn.setVisible(true);
				PaymentPartialfirstnameTextfield.setVisible(true);
				PaymentPartialLastnameTextfield.setVisible(true);
				PaymentPartialFirstnamelabel.setVisible(true);
				PaymentPartiallastnamelabel.setVisible(true);
				PaymentPartialDoneBtn.setVisible(true);
				PaymentPartialResetBtn.setVisible(true);
				PaymentPartialCancelBtn.setVisible(true);
				PaymentPartialTableScrollpane.setVisible(true);
				PaymentPartialSearchCustomertext.setVisible(true);
				PaymentPartialListCofCustomers.setVisible(true);
				
				double balance1 = 0.00;
				int rowcountforselectingbalance = 0;
				try
				{
					String query = "SELECT balanceid from balance_invoice" ;
					PreparedStatement pst2 = connection.prepareStatement(query);
					ResultSet rs2 = pst2.executeQuery();
					while(rs2.next())
					{
						String tempid = "";
						tempid = rs2.getString("BALANCEID");
						try
						{
						
							
							String query1 = "SELECT BALANCE FROM BALANCE where BALANCEID = '"
											+  tempid +"' AND DATE_UPDATED ="
											+ "(SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE "
											+ "BALANCEID = '"+ tempid + "') AND "
											+ "TIME_UPDATED = (SELECT MAX(TIME_UPDATED) FROM"
											+ " BALANCE WHERE BALANCEID ='" + tempid + "' AND DATE_UPDATED"
											+ "= (SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE BALANCEID"
											+ "='"+ tempid + "'))";
							PreparedStatement pst3 = connection.prepareStatement(query1);
							ResultSet rs3 = pst3.executeQuery();
							int z=0;
							while(rs3.next()) {
					           
					         
					           balance1 = rs3.getDouble("BALANCE");
					        z++;
					        }
							
						}catch(SQLException e1)
						{
							e1.printStackTrace();
						}
						if(balance1 > 0)
						{
							rowcountforselectingbalance ++;
							
						}
						else
						{
							continue;
						}
						
						
					}			
				}catch(SQLException e1)
				{
					e1.printStackTrace();
				}
				if(rowcountforselectingbalance == 0)
				{
					PaymentPartialTable.setModel(new DefaultTableModel(
							new Object[0][0] ,
							new String[] {
								"FIRSTNAME", "LASTNAME", "ADDRESS", "CONTACTNO."
							}
						) {
							Class[] columnTypes = new Class[] {
								Object.class, Object.class, String.class, Object.class
							};
							public Class getColumnClass(int columnIndex) {
								return columnTypes[columnIndex];
							}
							boolean[] columnEditables = new boolean[] {
								false, false, false
							};
							public boolean isCellEditable(int row, int column) {
								return columnEditables[column];
							}
						});
					PaymentPartialTable.getColumnModel().getColumn(0).setResizable(false);
					PaymentPartialTable.getColumnModel().getColumn(0).setPreferredWidth(200);
					PaymentPartialTable.getColumnModel().getColumn(1).setResizable(false);
					PaymentPartialTable.getColumnModel().getColumn(2).setResizable(false);
					PaymentPartialTable.getColumnModel().getColumn(2).setPreferredWidth(300);
					PaymentPartialTable.getColumnModel().getColumn(3).setResizable(false);
					
   
					          JOptionPane.showMessageDialog(null, "NO COSTUMER IS ELIGIBLE FOR PAYMENT!");
					    
				}
				else
				{
					String balanceidtemp[] = new String[rowcountforselectingbalance];
					String outputforbalanceidtemp = "";
					
					try
					{
						String query = "SELECT balanceid from balance_invoice" ;
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i =0;
						while(rs2.next())
						{
							
							String tempid = "";
							tempid = rs2.getString("BALANCEID");
							try
							{
							
								
								String query1 = "SELECT BALANCE FROM BALANCE where BALANCEID = '"
												+  tempid +"' AND DATE_UPDATED ="
												+ "(SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE "
												+ "BALANCEID = '"+ tempid + "') AND "
												+ "TIME_UPDATED = (SELECT MAX(TIME_UPDATED) FROM"
												+ " BALANCE WHERE BALANCEID ='" + tempid + "' AND DATE_UPDATED"
												+ "= (SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE BALANCEID"
												+ "='"+ tempid + "'))";
								PreparedStatement pst3 = connection.prepareStatement(query1);
								ResultSet rs3 = pst3.executeQuery();
								int z=0;
								while(rs3.next()) {
						           
						         
						           balance1 = rs3.getDouble("BALANCE");
						        z++;
						        }
								
							}catch(SQLException e1)
							{
								e1.printStackTrace();
							}
							if(balance1 > 0)
							{
								balanceidtemp[i] = tempid;
								i++;
								
							}
							else
							{
								continue;
							}
							
							
						}			
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					
					int k;
					for(k=0;k<=rowcountforselectingbalance-1;k++)
					{
						if(balanceidtemp[k].equals(balanceidtemp[balanceidtemp.length-1]))
						{
							outputforbalanceidtemp += "'" + balanceidtemp[k] + "'" ;
						}
						else
						{
							outputforbalanceidtemp += "'" + balanceidtemp[k] + "'," ;
						}
					}
					
					String customerids[] = new String[rowcountforselectingbalance];
					String outputforcustomerid = "";
					
					try
					{
					
						
						String query = "SELECT customerid from balance_invoice where balanceid in ("
										+outputforbalanceidtemp + ")";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) 
						{
							
							customerids[i] = rs2.getString("CUSTOMERID");
							
							if(customerids[i].equals(customerids[customerids.length-1]))
							{
								outputforcustomerid += "'" + customerids[i] + "'" ;
							}
							else
							{
								outputforcustomerid += "'" + customerids[i] + "'," ;
							}
							
						i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					
					try
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER where customerid in ("
										+ outputforcustomerid +")";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						
						if(rowcount != 0)
						{
							rowcount = 0;
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						else
						{
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					
					PaymentPartialTable.setModel(new DefaultTableModel(
							new Object[rowcount][rowcount] ,
							new String[] {
								"FIRSTNAME", "LASTNAME", "ADDRESS", "CONTACTNO."
							}
						) {
							Class[] columnTypes = new Class[] {
								Object.class, Object.class, String.class, Object.class
							};
							public Class getColumnClass(int columnIndex) {
								return columnTypes[columnIndex];
							}
							boolean[] columnEditables = new boolean[] {
								false, false, false
							};
							public boolean isCellEditable(int row, int column) {
								return columnEditables[column];
							}
						});
					PaymentPartialTable.getColumnModel().getColumn(0).setResizable(false);
					PaymentPartialTable.getColumnModel().getColumn(0).setPreferredWidth(200);
					PaymentPartialTable.getColumnModel().getColumn(1).setResizable(false);
					PaymentPartialTable.getColumnModel().getColumn(2).setResizable(false);
					PaymentPartialTable.getColumnModel().getColumn(2).setPreferredWidth(300);
					PaymentPartialTable.getColumnModel().getColumn(3).setResizable(false);
					
				
					
						
						try
						{
						
							
							String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER where customerid in ("
											+ outputforcustomerid +")" ;
							PreparedStatement pst2 = connection.prepareStatement(query);
							ResultSet rs2 = pst2.executeQuery();
							int i=0;
							while(rs2.next()) {
					           
					         
					            Object firstname = rs2.getString("FIRSTNAME");
					            Object lastname = rs2.getString("LASTNAME");
					            Object address = rs2.getString("ADDRESS");
					            Object contactnum = rs2.getString("CONTACTNO"); 
					             
					            PaymentPartialTable.getModel().setValueAt(firstname, i, 0);
					            PaymentPartialTable.getModel().setValueAt(lastname, i, 1);
					           PaymentPartialTable.getModel().setValueAt(address, i, 2);
					          PaymentPartialTable.getModel().setValueAt(contactnum, i, 3); 
					          
					         
					        i++;
					        }
							
						}catch(SQLException e1)
						{
							e1.printStackTrace();
						}

				}
			}
		});
		PaymentPartialTable.addMouseListener(new MouseAdapter() {
			
			public void mouseClicked(MouseEvent arg0) 
			{
				 if (PaymentPartialTable.getSelectedRow() > -1) 
				 {
					 frame.setEnabled(false);
					 PaymentPartialAmountFrame.setVisible(true);
				     c.setFirstname(PaymentPartialTable.getValueAt(PaymentPartialTable.getSelectedRow(), 0).toString().toUpperCase());
					 c.setLastname(PaymentPartialTable.getValueAt(PaymentPartialTable.getSelectedRow(), 1).toString().toUpperCase());
					
				 }
			}
		});
		paymentPartialconfirmbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				
				double balance1 = 0.00,updatebalance = 0.00;
				String tempzerodebt = "",Amounterror = "";
				
				
				 try
					{
					
						
						String query = "SELECT customerid FROM CUSTOMER where firstname = '" 
										+ c.getFirstname() 
										+"' AND LASTNAME ='" + c.getLastname()
										+"'";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) {
				           
				         
				            c.setCostumercode(rs2.getString("CUSTOMERID"));
				           
				        i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					try
					{
					
						
						String query = "SELECT BALANCEID FROM BALANCE_INVOICE where CUSTOMERID ="
										+ "'" + c.getCostumercode() +"'";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) {
				           
				         
				           c.setBalanceid(rs2.getString("BALANCEID"));
				           
				        i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					
					try
					{
					
						
						String query = "SELECT BALANCE FROM BALANCE where BALANCEID = '"
										+  c.getBalanceid() +"' AND DATE_UPDATED ="
										+ "(SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE "
										+ "BALANCEID = '"+ c.getBalanceid() + "') AND "
										+ "TIME_UPDATED = (SELECT MAX(TIME_UPDATED) FROM"
										+ " BALANCE WHERE BALANCEID ='" + c.getBalanceid() + "' AND DATE_UPDATED"
										+ "= (SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE BALANCEID"
										+ "='"+ c.getBalanceid() + "'))";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) {
				           
				         
				           balance1 = rs2.getDouble("BALANCE");
				        i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
				
				

				
				try {
					 	Double.parseDouble(paymentPartialtamounttextfield.getText());  
				      }catch ( NumberFormatException exc ) 
				 				  {
						              Amounterror = "error";
						          }
				
				
				if(paymentPartialtamounttextfield.getText() == "0")
				{
					tempzerodebt = "error";
				}
				else
				{
					tempzerodebt = "";
				}
				
				
				
				if(tempzerodebt == "error")
				{
					JOptionPane.showMessageDialog(null,"YOU NEED TO INPUT PAYMENT AMOUNT!, 0 PAYMENT AMOUNT IS NOT ACCEPTED!");
				}
				else if(Amounterror.equals("error"))
				{
					JOptionPane.showMessageDialog(null,"UNDEFINED AMOUNT!");
				}
				else if(Double.parseDouble(paymentPartialtamounttextfield.getText()) >= balance1)
				{
					JOptionPane.showMessageDialog(null,"YOUR PAYMENT AMOUNT MUST BE LOWER THAN YOUR TOTAL BALANCE! OR ELSE YOUR TRANSACTION IS FULL PAYMENT!");
				}
				else
				{
					String existingpaymentid = "";
					c.setPaymentamount(Double.parseDouble(paymentPartialtamounttextfield.getText()));
					c.setBalance(balance1);
					double change = 0.00;
					Time timepartialpaymenttransaction = null;
					Date datepartialpaymenttransaction = null;
					

								int paymentidtemp;
								String paymentidgetter = "";
								String paymentidget = "";
								do
								{
								paymentidtemp = rndm.nextInt(9999);
								if(Integer.toString(paymentidtemp).length() == 1)
								{
									paymentidgetter = "000" + Integer.toString(paymentidtemp);
								}
								else if(Integer.toString(paymentidtemp).length() == 2)
								{
									paymentidgetter = "00" + Integer.toString(paymentidtemp);
								}
								else if(Integer.toString(paymentidtemp).length() == 3)
								{
									paymentidgetter = "0" + Integer.toString(paymentidtemp);
								}
								else
								{
									paymentidgetter = Integer.toString(paymentidtemp);
								}
								
								try
								{
									String query = "SELECT PAYMENTID FROM PAYMENT_transaction where PAYMENTID = '" + paymentidgetter +  "'";
									PreparedStatement pst1 = connection.prepareStatement(query);
									ResultSet rs1 = pst1.executeQuery();
									
									if(!rs1.next())
									{
										
							            paymentidget = "";
							        }
									else
									{
										paymentidget = "error";
									}
								}catch(SQLException e1)
								{
									e1.printStackTrace();
								}
								}while(paymentidget == "error");
								c.setPaymentid(paymentidgetter);
								
								
								
								try {
									String query = "INSERT INTO PAYMENT_TRANSACTION VALUES('"+ c.getPaymentid() + "'," 
											 + c.getPaymentamount() + "," 
											 + "CURRENT TIMESTAMP" + "," + "CURRENT TIMESTAMP"
											 + ",'" + "PP" + "')"  ;
																				  
									PreparedStatement pst3 = connection.prepareStatement(query);
									pst3.executeUpdate();
								} catch (SQLException e1) {
									
									e1.printStackTrace();
								}
								
								try {
									String query = "INSERT INTO PAYMENT_INVOICE VALUES('"+ c.getCostumercode() + "','" 
																						 + c.getPaymentid() + "','" 
																						 + c.getBalanceid() +"')"  ;
																				  
									PreparedStatement pst3 = connection.prepareStatement(query);
									pst3.executeUpdate();
								} catch (SQLException e1) {
									
									e1.printStackTrace();
								}
								
								change = c.getBalance() -c.getPaymentamount();
								try
								{
								
									
									String query = "SELECT PAYMENT_DATE,PAYMENT_TIME  FROM PAYMENT_TRANSACTION where PAYMENTID ="
													+ "'" + c.getPaymentid() +"'";
									PreparedStatement pst2 = connection.prepareStatement(query);
									ResultSet rs2 = pst2.executeQuery();
									int i=0;
									while(rs2.next()) {
							           
							         
										timepartialpaymenttransaction = rs2.getTime("PAYMENT_TIME");
										datepartialpaymenttransaction= rs2.getDate("PAYMENT_DATE");
							           
							        i++;
							        }
									
								}catch(SQLException e1)
								{
									e1.printStackTrace();
								}
								updatebalance = c.getBalance() - c.getPaymentamount()  ;
								
								try
								{
								
									
									String query = "INSERT INTO BALANCE VALUES('" + c.getBalanceid() 
													+"'," + updatebalance + ",CURRENT TIMESTAMP"
															+ ",CURRENT TIMESTAMP)";
													
									PreparedStatement pst2 = connection.prepareStatement(query);
									pst2.executeUpdate();
									
									
								}catch(SQLException e1)
								{
									e1.printStackTrace();
								}
								
								paymentPartialtamounttextfield.setText("");
								PaymentPartialTransactionCustomerNameText.setText(c.getFirstname() + "      " + c.getLastname());
								PaymentPartialTransactionCustomerBalText.setText(Double.toString(c.getBalance()));
								PaymentPartialTransactionPaymentAmountText.setText(Double.toString(c.getPaymentamount()));
								PaymentPartialTransactionChangedText.setText(Double.toString(change));
								PaymentPartialDateAndTimeText.setText(dateFormat.format(datepartialpaymenttransaction) + "        " + timeFormat.format(timepartialpaymenttransaction));
								PaymentPartialAmountFrame.setVisible(false);
								PaymentPartialTransaction.setVisible(true);
					
					

				}
				
			}
		});
		PaymentPartialTransactionConfirmBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				frame.setEnabled(true);
				PaymentPartialTransaction.setVisible(false);
				
				double balance1 = 0.00;
				int rowcountforselectingbalance = 0;
				try
				{
					String query = "SELECT balanceid from balance_invoice" ;
					PreparedStatement pst2 = connection.prepareStatement(query);
					ResultSet rs2 = pst2.executeQuery();
					while(rs2.next())
					{
						String tempid = "";
						tempid = rs2.getString("BALANCEID");
						try
						{
						
							
							String query1 = "SELECT BALANCE FROM BALANCE where BALANCEID = '"
											+  tempid +"' AND DATE_UPDATED ="
											+ "(SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE "
											+ "BALANCEID = '"+ tempid + "') AND "
											+ "TIME_UPDATED = (SELECT MAX(TIME_UPDATED) FROM"
											+ " BALANCE WHERE BALANCEID ='" + tempid + "' AND DATE_UPDATED"
											+ "= (SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE BALANCEID"
											+ "='"+ tempid + "'))";
							PreparedStatement pst3 = connection.prepareStatement(query1);
							ResultSet rs3 = pst3.executeQuery();
							int z=0;
							while(rs3.next()) {
					           
					         
					           balance1 = rs3.getDouble("BALANCE");
					        z++;
					        }
							
						}catch(SQLException e1)
						{
							e1.printStackTrace();
						}
						if(balance1 > 0)
						{
							rowcountforselectingbalance ++;
							
						}
						else
						{
							continue;
						}
						
						
					}			
				}catch(SQLException e1)
				{
					e1.printStackTrace();
				}
				if(rowcountforselectingbalance == 0)
				{
					PaymentPartialTable.setModel(new DefaultTableModel(
							new Object[0][0] ,
							new String[] {
								"FIRSTNAME", "LASTNAME", "ADDRESS", "CONTACTNO."
							}
						) {
							Class[] columnTypes = new Class[] {
								Object.class, Object.class, String.class, Object.class
							};
							public Class getColumnClass(int columnIndex) {
								return columnTypes[columnIndex];
							}
							boolean[] columnEditables = new boolean[] {
								false, false, false
							};
							public boolean isCellEditable(int row, int column) {
								return columnEditables[column];
							}
						});
					PaymentPartialTable.getColumnModel().getColumn(0).setResizable(false);
					PaymentPartialTable.getColumnModel().getColumn(0).setPreferredWidth(200);
					PaymentPartialTable.getColumnModel().getColumn(1).setResizable(false);
					PaymentPartialTable.getColumnModel().getColumn(2).setResizable(false);
					PaymentPartialTable.getColumnModel().getColumn(2).setPreferredWidth(300);
					PaymentPartialTable.getColumnModel().getColumn(3).setResizable(false);
					
					          
					        
					    
				}
				else
				{
					String balanceidtemp[] = new String[rowcountforselectingbalance];
					String outputforbalanceidtemp = "";
					
					try
					{
						String query = "SELECT balanceid from balance_invoice" ;
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i =0;
						while(rs2.next())
						{
							
							String tempid = "";
							tempid = rs2.getString("BALANCEID");
							try
							{
							
								
								String query1 = "SELECT BALANCE FROM BALANCE where BALANCEID = '"
												+  tempid +"' AND DATE_UPDATED ="
												+ "(SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE "
												+ "BALANCEID = '"+ tempid + "') AND "
												+ "TIME_UPDATED = (SELECT MAX(TIME_UPDATED) FROM"
												+ " BALANCE WHERE BALANCEID ='" + tempid + "' AND DATE_UPDATED"
												+ "= (SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE BALANCEID"
												+ "='"+ tempid + "'))";
								PreparedStatement pst3 = connection.prepareStatement(query1);
								ResultSet rs3 = pst3.executeQuery();
								int z=0;
								while(rs3.next()) {
						           
						         
						           balance1 = rs3.getDouble("BALANCE");
						        z++;
						        }
								
							}catch(SQLException e1)
							{
								e1.printStackTrace();
							}
							if(balance1 > 0)
							{
								balanceidtemp[i] = tempid;
								i++;
								
							}
							else
							{
								continue;
							}
							
							
						}			
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					
					int k;
					for(k=0;k<=rowcountforselectingbalance-1;k++)
					{
						if(balanceidtemp[k].equals(balanceidtemp[balanceidtemp.length-1]))
						{
							outputforbalanceidtemp += "'" + balanceidtemp[k] + "'" ;
						}
						else
						{
							outputforbalanceidtemp += "'" + balanceidtemp[k] + "'," ;
						}
					}
					
					String customerids[] = new String[rowcountforselectingbalance];
					String outputforcustomerid = "";
					
					try
					{
					
						
						String query = "SELECT customerid from balance_invoice where balanceid in ("
										+outputforbalanceidtemp + ")";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) 
						{
							
							customerids[i] = rs2.getString("CUSTOMERID");
							
							if(customerids[i].equals(customerids[customerids.length-1]))
							{
								outputforcustomerid += "'" + customerids[i] + "'" ;
							}
							else
							{
								outputforcustomerid += "'" + customerids[i] + "'," ;
							}
							
						i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					
					try
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER where customerid in ("
										+ outputforcustomerid +")";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						
						if(rowcount != 0)
						{
							rowcount = 0;
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						else
						{
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					
					PaymentPartialTable.setModel(new DefaultTableModel(
							new Object[rowcount][rowcount] ,
							new String[] {
								"FIRSTNAME", "LASTNAME", "ADDRESS", "CONTACTNO."
							}
						) {
							Class[] columnTypes = new Class[] {
								Object.class, Object.class, String.class, Object.class
							};
							public Class getColumnClass(int columnIndex) {
								return columnTypes[columnIndex];
							}
							boolean[] columnEditables = new boolean[] {
								false, false, false
							};
							public boolean isCellEditable(int row, int column) {
								return columnEditables[column];
							}
						});
					PaymentPartialTable.getColumnModel().getColumn(0).setResizable(false);
					PaymentPartialTable.getColumnModel().getColumn(0).setPreferredWidth(200);
					PaymentPartialTable.getColumnModel().getColumn(1).setResizable(false);
					PaymentPartialTable.getColumnModel().getColumn(2).setResizable(false);
					PaymentPartialTable.getColumnModel().getColumn(2).setPreferredWidth(300);
					PaymentPartialTable.getColumnModel().getColumn(3).setResizable(false);
					
				
					
						
						try
						{
						
							
							String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER where customerid in ("
											+ outputforcustomerid +")" ;
							PreparedStatement pst2 = connection.prepareStatement(query);
							ResultSet rs2 = pst2.executeQuery();
							int i=0;
							while(rs2.next()) {
					           
					         
					            Object firstname = rs2.getString("FIRSTNAME");
					            Object lastname = rs2.getString("LASTNAME");
					            Object address = rs2.getString("ADDRESS");
					            Object contactnum = rs2.getString("CONTACTNO"); 
					             
					            PaymentPartialTable.getModel().setValueAt(firstname, i, 0);
					            PaymentPartialTable.getModel().setValueAt(lastname, i, 1);
					           PaymentPartialTable.getModel().setValueAt(address, i, 2);
					          PaymentPartialTable.getModel().setValueAt(contactnum, i, 3); 
					          
					         
					        i++;
					        }
							
						}catch(SQLException e1)
						{
							e1.printStackTrace();
						}
				}
				
				JOptionPane.showMessageDialog(null,"TRANSACTION SUCCESFUL!");
			}
		});
		
		paymentPartialcancelbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				frame.setEnabled(true);
				paymentPartialtamounttextfield.setText("");
				PaymentPartialAmountFrame.setVisible(false);
				frame.show();
			}
		});
		PaymentPartialSearchBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				search.setFirstname(PaymentPartialfirstnameTextfield.getText().toUpperCase());
				search.setLastname(PaymentPartialLastnameTextfield.getText().toUpperCase());
				String con1 = "false", con2 = "false", con3 = "false";
				
				if((search.getFirstname().length() != 0) && (search.getLastname().length() != 0))
				{
					con1 = "true";
				}
				else if((search.getFirstname().length() != 0) && (search.getLastname().length() == 0))
				{
					con2 = "true";
				}
				else if((search.getFirstname().length() == 0) && (search.getLastname().length() != 0))
				{
					con3 = "true";
				}
				
				
				
				if(con1 == "true" )
				{
					try
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
								+ search.getFirstname() + "' AND lastname = '" 
								+ search.getLastname()  + "'" ;
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						if(rowcount != 0)
						{
							rowcount = 0;
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						else
						{
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
				}
				else if( con2 == "true")
				{
					try
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
								+ search.getFirstname() + "'";
								 
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						if(rowcount != 0)
						{
							rowcount = 0;
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						else
						{
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
				}
				
				else if(con3 == "true")
				{
					try
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE lastname = '" 
								+ search.getLastname() + "'";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						if(rowcount != 0)
						{
							rowcount = 0;
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						else
						{
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
				}
				else
				{
					try
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						
						if(rowcount != 0)
						{
							rowcount = 0;
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						else
						{
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
				}
				
				
				PaymentPartialTable.setModel(new DefaultTableModel(
						new Object[rowcount][rowcount] ,
						new String[] {
							"FIRSTNAME", "LASTNAME", "ADDRESS", "CONTACTNO."
						}
					) {
						Class[] columnTypes = new Class[] {
							Object.class, Object.class, String.class, Object.class
						};
						public Class getColumnClass(int columnIndex) {
							return columnTypes[columnIndex];
						}
						boolean[] columnEditables = new boolean[] {
							false, false, false
						};
						public boolean isCellEditable(int row, int column) {
							return columnEditables[column];
						}
					});
				PaymentPartialTable.getColumnModel().getColumn(0).setResizable(false);
				PaymentPartialTable.getColumnModel().getColumn(0).setPreferredWidth(200);
				PaymentPartialTable.getColumnModel().getColumn(1).setResizable(false);
				PaymentPartialTable.getColumnModel().getColumn(2).setResizable(false);
				PaymentPartialTable.getColumnModel().getColumn(2).setPreferredWidth(300);
				PaymentPartialTable.getColumnModel().getColumn(3).setResizable(false);
				
				try
				{
				
					if(con1 == "true")
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
								+ search.getFirstname() + "' AND lastname = '" 
								+ search.getLastname()  + "'" ;
				
				
						PreparedStatement pst3 = connection.prepareStatement(query);
						ResultSet rs3 = pst3.executeQuery();
				
						if(!rs3.next())
						{
							JOptionPane.showMessageDialog(null, "NO COSTUMER FOUND!");
						}
						else
						{
							String query1 = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
											+ search.getFirstname() + "' AND lastname = '" 
											+ search.getLastname()  + "'"  ;
			
			
							PreparedStatement pst4 = connection.prepareStatement(query1);
							ResultSet rs4 = pst4.executeQuery();
							int i=0;
							while(rs4.next()) 
							{
						           
						         
					            Object firstname = rs4.getString("FIRSTNAME");
					            Object lastname = rs4.getString("LASTNAME");
					            Object address = rs4.getString("ADDRESS");
					            Object cantctnum = rs4.getString("CONTACTNO"); 
					             
					            PaymentPartialTable.getModel().setValueAt(firstname, i, 0);
					            PaymentPartialTable.getModel().setValueAt(lastname, i, 1);
					           PaymentPartialTable.getModel().setValueAt(address, i, 2);
					          PaymentPartialTable.getModel().setValueAt(cantctnum, i, 3); 
					          
					         
					        i++;
							}

						}
					}	
					else if(con2 == "true")
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
								+ search.getFirstname() + "'";
								 
						PreparedStatement pst3 = connection.prepareStatement(query);
						ResultSet rs3 = pst3.executeQuery();
						
						if(!rs3.next())
						{
							JOptionPane.showMessageDialog(null, "NO COSTUMER FOUND!");
						}
						else
						{
							String query1 = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
											+ search.getFirstname() + "'"  ;
			
			
							PreparedStatement pst4 = connection.prepareStatement(query1);
							ResultSet rs4 = pst4.executeQuery();
							int i=0;
							while(rs4.next()) 
							{
						           
						         
					            Object firstname = rs4.getString("FIRSTNAME");
					            Object lastname = rs4.getString("LASTNAME");
					            Object address = rs4.getString("ADDRESS");
					            Object cantctnum = rs4.getString("CONTACTNO"); 
					             
					            PaymentPartialTable.getModel().setValueAt(firstname, i, 0);
					            PaymentPartialTable.getModel().setValueAt(lastname, i, 1);
					           PaymentPartialTable.getModel().setValueAt(address, i, 2);
					          PaymentPartialTable.getModel().setValueAt(cantctnum, i, 3); 
					          
					         
					        i++;
							}
						}
					}	
					else if(con3 == "true")
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE lastname = '" 
								+ search.getLastname() + "'";
						PreparedStatement pst3 = connection.prepareStatement(query);
						ResultSet rs3 = pst3.executeQuery();
						
						if(!rs3.next())
						{
							JOptionPane.showMessageDialog(null, "NO COSTUMER FOUND!");
						}
						else
						{
							String query1 = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE lastname = '" 
											+ search.getLastname() + "'"  ;
			
			
							PreparedStatement pst4 = connection.prepareStatement(query1);
							ResultSet rs4 = pst4.executeQuery();
							int i=0;
							while(rs4.next()) 
							{
						           
						         
					            Object firstname = rs4.getString("FIRSTNAME");
					            Object lastname = rs4.getString("LASTNAME");
					            Object address = rs4.getString("ADDRESS");
					            Object cantctnum = rs4.getString("CONTACTNO"); 
					             
					            PaymentPartialTable.getModel().setValueAt(firstname, i, 0);
					            PaymentPartialTable.getModel().setValueAt(lastname, i, 1);
					           PaymentPartialTable.getModel().setValueAt(address, i, 2);
					          PaymentPartialTable.getModel().setValueAt(cantctnum, i, 3); 
					          
					         
					        i++;
							}
						}
					}
					else
					{
						try
						{
						
							
							String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
							PreparedStatement pst2 = connection.prepareStatement(query);
							ResultSet rs2 = pst2.executeQuery();
							int i=0;
							while(rs2.next()) {
					           
					         
					            Object firstname = rs2.getString("FIRSTNAME");
					            Object lastname = rs2.getString("LASTNAME");
					            Object address = rs2.getString("ADDRESS");
					            Object contactnum = rs2.getString("CONTACTNO"); 
					             
					            PaymentPartialTable.getModel().setValueAt(firstname, i, 0);
					            PaymentPartialTable.getModel().setValueAt(lastname, i, 1);
					           PaymentPartialTable.getModel().setValueAt(address, i, 2);
					          PaymentPartialTable.getModel().setValueAt(contactnum, i, 3); 
					          
					         
					        i++;
					        }
							
						}catch(SQLException e1)
						{
							e1.printStackTrace();
						}
					}
									
						
					
				}catch(SQLException e1)
				{
					e1.printStackTrace();
				}
			}
		});
		PaymentPartialResetBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				PaymentPartialfirstnameTextfield.setText("");
				PaymentPartialLastnameTextfield.setText("");
				try
				{
					String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
					PreparedStatement pst2 = connection.prepareStatement(query);
					ResultSet rs2 = pst2.executeQuery();
					
					if(rowcount != 0)
					{
						rowcount = 0;
						while(rs2.next())
						{
						    rowcount ++;
						}
					}
					else
					{
						while(rs2.next())
						{
						    rowcount ++;
						}
					}
					
					
				}catch(SQLException e1)
				{
					e1.printStackTrace();
				}
				
				PaymentPartialTable.setModel(new DefaultTableModel(
						new Object[rowcount][rowcount] ,
						new String[] {
							"FIRSTNAME", "LASTNAME", "ADDRESS", "CONTACTNO."
						}
					) {
						Class[] columnTypes = new Class[] {
							Object.class, Object.class, String.class, Object.class
						};
						public Class getColumnClass(int columnIndex) {
							return columnTypes[columnIndex];
						}
						boolean[] columnEditables = new boolean[] {
							false, false, false
						};
						public boolean isCellEditable(int row, int column) {
							return columnEditables[column];
						}
					});
				PaymentPartialTable.getColumnModel().getColumn(0).setResizable(false);
				PaymentPartialTable.getColumnModel().getColumn(0).setPreferredWidth(200);
				PaymentPartialTable.getColumnModel().getColumn(1).setResizable(false);
				PaymentPartialTable.getColumnModel().getColumn(2).setResizable(false);
				PaymentPartialTable.getColumnModel().getColumn(2).setPreferredWidth(300);
				PaymentPartialTable.getColumnModel().getColumn(3).setResizable(false);
				
			
				
					
					try
					{
					
						
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) {
				           
				         
				            Object firstname = rs2.getString("FIRSTNAME");
				            Object lastname = rs2.getString("LASTNAME");
				            Object address = rs2.getString("ADDRESS");
				            Object contactnum = rs2.getString("CONTACTNO"); 
				             
				            PaymentPartialTable.getModel().setValueAt(firstname, i, 0);
				            PaymentPartialTable.getModel().setValueAt(lastname, i, 1);
				           PaymentPartialTable.getModel().setValueAt(address, i, 2);
				          PaymentPartialTable.getModel().setValueAt(contactnum, i, 3); 
				          
				         
				        i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
			}
		});
		PaymentPartialDoneBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				PaymentPartialfirstnameTextfield.setText("");
				PaymentPartialLastnameTextfield.setText("");
				user.setEnabled(true);
				ADDDEBT.setEnabled(true);
				VIEW.setEnabled(true);	
				BILLOUT.setEnabled(true);	
				ABOUTUS.setEnabled(true);
				PaymentPartialSearchBtn.getRootPane().setDefaultButton(null);
				PaymentDownPaymentBtn.setVisible(true);
				PaymentFullpaymentBtn.setVisible(true);
				
				
				PaymentPartialSearchBtn.setVisible(false);
				PaymentPartialfirstnameTextfield.setVisible(false);
				PaymentPartialLastnameTextfield.setVisible(false);
				PaymentPartialFirstnamelabel.setVisible(false);
				PaymentPartiallastnamelabel.setVisible(false);
				PaymentPartialDoneBtn.setVisible(false);
				PaymentPartialResetBtn.setVisible(false);
				PaymentPartialCancelBtn.setVisible(false);
				PaymentPartialTableScrollpane.setVisible(false);
				PaymentPartialSearchCustomertext.setVisible(false);
				PaymentPartialListCofCustomers.setVisible(false);
			}
		});
		
		PaymentPartialCancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				PaymentPartialfirstnameTextfield.setText("");
				PaymentPartialLastnameTextfield.setText("");
				user.setEnabled(true);
				ADDDEBT.setEnabled(true);
				VIEW.setEnabled(true);	
				BILLOUT.setEnabled(true);	
				ABOUTUS.setEnabled(true);
				billoutpanel.getRootPane().setDefaultButton(null);
				PaymentDownPaymentBtn.setVisible(true);
				PaymentFullpaymentBtn.setVisible(true);
				
				
				
				PaymentPartialSearchBtn.setVisible(false);
				PaymentPartialfirstnameTextfield.setVisible(false);
				PaymentPartialLastnameTextfield.setVisible(false);
				PaymentPartialFirstnamelabel.setVisible(false);
				PaymentPartiallastnamelabel.setVisible(false);
				PaymentPartialDoneBtn.setVisible(false);
				PaymentPartialResetBtn.setVisible(false);
				PaymentPartialCancelBtn.setVisible(false);
				PaymentPartialTableScrollpane.setVisible(false);
				PaymentPartialSearchCustomertext.setVisible(false);
				PaymentPartialListCofCustomers.setVisible(false);
			}
		});
		
		PaymentFullpaymentBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				user.setEnabled(false);
				ADDDEBT.setEnabled(false);
				VIEW.setEnabled(false);	
				BILLOUT.setEnabled(false);	
				ABOUTUS.setEnabled(false);
				billoutpanel.getRootPane().setDefaultButton(PaymentFullSearchBtn);
				PaymentDownPaymentBtn.setVisible(false);
				PaymentFullpaymentBtn.setVisible(false);
				
				PaymentFullSearchBtn.setVisible(true);
				PaymentFullfirstnameTextfield.setVisible(true);
				PaymentFullLastnameTextfield.setVisible(true);
				PaymentFullFirstnamelabel.setVisible(true);
				PaymentFulllastnamelabel.setVisible(true);
				PaymentFullDoneBtn.setVisible(true);
				PaymentFullResetBtn.setVisible(true);
				PaymentFullCancelBtn.setVisible(true);
				PaymentFullTableScrollpane.setVisible(true);
				PaymentFullSearchCustomertext.setVisible(true);
				PaymentFullListCofCustomers.setVisible(true);
				
				
				double balance1 = 0.00;
				int rowcountforselectingbalance = 0;
				try
				{
					String query = "SELECT balanceid from balance_invoice" ;
					PreparedStatement pst2 = connection.prepareStatement(query);
					ResultSet rs2 = pst2.executeQuery();
					while(rs2.next())
					{
						String tempid = "";
						tempid = rs2.getString("BALANCEID");
						try
						{
						
							
							String query1 = "SELECT BALANCE FROM BALANCE where BALANCEID = '"
											+  tempid +"' AND DATE_UPDATED ="
											+ "(SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE "
											+ "BALANCEID = '"+ tempid + "') AND "
											+ "TIME_UPDATED = (SELECT MAX(TIME_UPDATED) FROM"
											+ " BALANCE WHERE BALANCEID ='" + tempid + "' AND DATE_UPDATED"
											+ "= (SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE BALANCEID"
											+ "='"+ tempid + "'))";
							PreparedStatement pst3 = connection.prepareStatement(query1);
							ResultSet rs3 = pst3.executeQuery();
							int z=0;
							while(rs3.next()) {
					           
					         
					           balance1 = rs3.getDouble("BALANCE");
					        z++;
					        }
							
						}catch(SQLException e1)
						{
							e1.printStackTrace();
						}
						if(balance1 > 0)
						{
							rowcountforselectingbalance ++;
							
						}
						else
						{
							continue;
						}
						
						
					}			
				}catch(SQLException e1)
				{
					e1.printStackTrace();
				}
				if(rowcountforselectingbalance == 0)
				{
					PaymentFullTable.setModel(new DefaultTableModel(
							new Object[0][0] ,
							new String[] {
								"FIRSTNAME", "LASTNAME", "ADDRESS", "CONTACTNO."
							}
						) {
							Class[] columnTypes = new Class[] {
								Object.class, Object.class, String.class, Object.class
							};
							public Class getColumnClass(int columnIndex) {
								return columnTypes[columnIndex];
							}
							boolean[] columnEditables = new boolean[] {
								false, false, false
							};
							public boolean isCellEditable(int row, int column) {
								return columnEditables[column];
							}
						});
					PaymentFullTable.getColumnModel().getColumn(0).setResizable(false);
					PaymentFullTable.getColumnModel().getColumn(0).setPreferredWidth(200);
					PaymentFullTable.getColumnModel().getColumn(1).setResizable(false);
					PaymentFullTable.getColumnModel().getColumn(2).setResizable(false);
					PaymentFullTable.getColumnModel().getColumn(2).setPreferredWidth(300);
					PaymentFullTable.getColumnModel().getColumn(3).setResizable(false);
					

					          JOptionPane.showMessageDialog(null, "NO COSTUMER IS ELIGIBLE FOR PAYMENT!");
				}
				else
				{
					String balanceidtemp[] = new String[rowcountforselectingbalance];
					String outputforbalanceidtemp = "";
					
					try
					{
						String query = "SELECT balanceid from balance_invoice" ;
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i =0;
						while(rs2.next())
						{
							
							String tempid = "";
							tempid = rs2.getString("BALANCEID");
							try
							{
							
								
								String query1 = "SELECT BALANCE FROM BALANCE where BALANCEID = '"
												+  tempid +"' AND DATE_UPDATED ="
												+ "(SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE "
												+ "BALANCEID = '"+ tempid + "') AND "
												+ "TIME_UPDATED = (SELECT MAX(TIME_UPDATED) FROM"
												+ " BALANCE WHERE BALANCEID ='" + tempid + "' AND DATE_UPDATED"
												+ "= (SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE BALANCEID"
												+ "='"+ tempid + "'))";
								PreparedStatement pst3 = connection.prepareStatement(query1);
								ResultSet rs3 = pst3.executeQuery();
								int z=0;
								while(rs3.next()) {
						           
						         
						           balance1 = rs3.getDouble("BALANCE");
						        z++;
						        }
								
							}catch(SQLException e1)
							{
								e1.printStackTrace();
							}
							if(balance1 > 0)
							{
								balanceidtemp[i] = tempid;
								i++;
								
							}
							else
							{
								continue;
							}
							
							
						}			
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					
					int k;
					for(k=0;k<=rowcountforselectingbalance-1;k++)
					{
						if(balanceidtemp[k].equals(balanceidtemp[balanceidtemp.length-1]))
						{
							outputforbalanceidtemp += "'" + balanceidtemp[k] + "'" ;
						}
						else
						{
							outputforbalanceidtemp += "'" + balanceidtemp[k] + "'," ;
						}
					}
					
					String customerids[] = new String[rowcountforselectingbalance];
					String outputforcustomerid = "";
					
					try
					{
					
						
						String query = "SELECT customerid from balance_invoice where balanceid in ("
										+outputforbalanceidtemp + ")";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) 
						{
							
							customerids[i] = rs2.getString("CUSTOMERID");
							
							if(customerids[i].equals(customerids[customerids.length-1]))
							{
								outputforcustomerid += "'" + customerids[i] + "'" ;
							}
							else
							{
								outputforcustomerid += "'" + customerids[i] + "'," ;
							}
							
						i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					
					try
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER where customerid in ("
										+ outputforcustomerid +")";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						
						if(rowcount != 0)
						{
							rowcount = 0;
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						else
						{
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					
					PaymentFullTable.setModel(new DefaultTableModel(
							new Object[rowcount][rowcount] ,
							new String[] {
								"FIRSTNAME", "LASTNAME", "ADDRESS", "CONTACTNO."
							}
						) {
							Class[] columnTypes = new Class[] {
								Object.class, Object.class, String.class, Object.class
							};
							public Class getColumnClass(int columnIndex) {
								return columnTypes[columnIndex];
							}
							boolean[] columnEditables = new boolean[] {
								false, false, false
							};
							public boolean isCellEditable(int row, int column) {
								return columnEditables[column];
							}
						});
					PaymentFullTable.getColumnModel().getColumn(0).setResizable(false);
					PaymentFullTable.getColumnModel().getColumn(0).setPreferredWidth(200);
					PaymentFullTable.getColumnModel().getColumn(1).setResizable(false);
					PaymentFullTable.getColumnModel().getColumn(2).setResizable(false);
					PaymentFullTable.getColumnModel().getColumn(2).setPreferredWidth(300);
					PaymentFullTable.getColumnModel().getColumn(3).setResizable(false);
					
				
					
						
						try
						{
						
							
							String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER where customerid in ("
											+ outputforcustomerid +")" ;
							PreparedStatement pst2 = connection.prepareStatement(query);
							ResultSet rs2 = pst2.executeQuery();
							int i=0;
							while(rs2.next()) {
					           
					         
					            Object firstname = rs2.getString("FIRSTNAME");
					            Object lastname = rs2.getString("LASTNAME");
					            Object address = rs2.getString("ADDRESS");
					            Object contactnum = rs2.getString("CONTACTNO"); 
					             
					            PaymentFullTable.getModel().setValueAt(firstname, i, 0);
					            PaymentFullTable.getModel().setValueAt(lastname, i, 1);
					           PaymentFullTable.getModel().setValueAt(address, i, 2);
					          PaymentFullTable.getModel().setValueAt(contactnum, i, 3); 
					          
					         
					        i++;
					        }
							
						}catch(SQLException e1)
						{
							e1.printStackTrace();
						}
				}
				
				
				
			}
		});
		
		PaymentFullSearchBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				search.setFirstname(PaymentFullfirstnameTextfield.getText().toUpperCase());
				search.setLastname(PaymentFullLastnameTextfield.getText().toUpperCase());
				String con1 = "false", con2 = "false", con3 = "false";
				
				if((search.getFirstname().length() != 0) && (search.getLastname().length() != 0))
				{
					con1 = "true";
				}
				else if((search.getFirstname().length() != 0) && (search.getLastname().length() == 0))
				{
					con2 = "true";
				}
				else if((search.getFirstname().length() == 0) && (search.getLastname().length() != 0))
				{
					con3 = "true";
				}
				
				
				
				if(con1 == "true" )
				{
					try
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
								+ search.getFirstname() + "' AND lastname = '" 
								+ search.getLastname()  + "'" ;
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						if(rowcount != 0)
						{
							rowcount = 0;
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						else
						{
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
				}
				else if( con2 == "true")
				{
					try
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
								+ search.getFirstname() + "'";
								 
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						if(rowcount != 0)
						{
							rowcount = 0;
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						else
						{
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
				}
				
				else if(con3 == "true")
				{
					try
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE lastname = '" 
								+ search.getLastname() + "'";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						if(rowcount != 0)
						{
							rowcount = 0;
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						else
						{
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
				}
				else
				{
					try
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						
						if(rowcount != 0)
						{
							rowcount = 0;
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						else
						{
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
				}
				
				
				PaymentFullTable.setModel(new DefaultTableModel(
						new Object[rowcount][rowcount] ,
						new String[] {
							"FIRSTNAME", "LASTNAME", "ADDRESS", "CONTACTNO."
						}
					) {
						Class[] columnTypes = new Class[] {
							Object.class, Object.class, String.class, Object.class
						};
						public Class getColumnClass(int columnIndex) {
							return columnTypes[columnIndex];
						}
						boolean[] columnEditables = new boolean[] {
							false, false, false
						};
						public boolean isCellEditable(int row, int column) {
							return columnEditables[column];
						}
					});
				PaymentFullTable.getColumnModel().getColumn(0).setResizable(false);
				PaymentFullTable.getColumnModel().getColumn(0).setPreferredWidth(200);
				PaymentFullTable.getColumnModel().getColumn(1).setResizable(false);
				PaymentFullTable.getColumnModel().getColumn(2).setResizable(false);
				PaymentFullTable.getColumnModel().getColumn(2).setPreferredWidth(300);
				PaymentFullTable.getColumnModel().getColumn(3).setResizable(false);
				
				try
				{
				
					if(con1 == "true")
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
								+ search.getFirstname() + "' AND lastname = '" 
								+ search.getLastname()  + "'" ;
				
				
						PreparedStatement pst3 = connection.prepareStatement(query);
						ResultSet rs3 = pst3.executeQuery();
				
						if(!rs3.next())
						{
							JOptionPane.showMessageDialog(null, "NO COSTUMER FOUND!");
						}
						else
						{
							String query1 = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
											+ search.getFirstname() + "' AND lastname = '" 
											+ search.getLastname()  + "'"  ;
			
			
							PreparedStatement pst4 = connection.prepareStatement(query1);
							ResultSet rs4 = pst4.executeQuery();
							int i=0;
							while(rs4.next()) 
							{
						           
						         
					            Object firstname = rs4.getString("FIRSTNAME");
					            Object lastname = rs4.getString("LASTNAME");
					            Object address = rs4.getString("ADDRESS");
					            Object cantctnum = rs4.getString("CONTACTNO"); 
					             
					            PaymentFullTable.getModel().setValueAt(firstname, i, 0);
					            PaymentFullTable.getModel().setValueAt(lastname, i, 1);
					           PaymentFullTable.getModel().setValueAt(address, i, 2);
					          PaymentFullTable.getModel().setValueAt(cantctnum, i, 3); 
					          
					         
					        i++;
							}

						}
					}	
					else if(con2 == "true")
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
								+ search.getFirstname() + "'";
								 
						PreparedStatement pst3 = connection.prepareStatement(query);
						ResultSet rs3 = pst3.executeQuery();
						
						if(!rs3.next())
						{
							JOptionPane.showMessageDialog(null, "NO COSTUMER FOUND!");
						}
						else
						{
							String query1 = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
											+ search.getFirstname() + "'"  ;
			
			
							PreparedStatement pst4 = connection.prepareStatement(query1);
							ResultSet rs4 = pst4.executeQuery();
							int i=0;
							while(rs4.next()) 
							{
						           
						         
					            Object firstname = rs4.getString("FIRSTNAME");
					            Object lastname = rs4.getString("LASTNAME");
					            Object address = rs4.getString("ADDRESS");
					            Object cantctnum = rs4.getString("CONTACTNO"); 
					             
					            PaymentFullTable.getModel().setValueAt(firstname, i, 0);
					            PaymentFullTable.getModel().setValueAt(lastname, i, 1);
					           PaymentFullTable.getModel().setValueAt(address, i, 2);
					          PaymentFullTable.getModel().setValueAt(cantctnum, i, 3); 
					          
					         
					        i++;
							}
						}
					}	
					else if(con3 == "true")
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE lastname = '" 
								+ search.getLastname() + "'";
						PreparedStatement pst3 = connection.prepareStatement(query);
						ResultSet rs3 = pst3.executeQuery();
						
						if(!rs3.next())
						{
							JOptionPane.showMessageDialog(null, "NO COSTUMER FOUND!");
						}
						else
						{
							String query1 = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE lastname = '" 
											+ search.getLastname() + "'"  ;
			
			
							PreparedStatement pst4 = connection.prepareStatement(query1);
							ResultSet rs4 = pst4.executeQuery();
							int i=0;
							while(rs4.next()) 
							{
						           
						         
					            Object firstname = rs4.getString("FIRSTNAME");
					            Object lastname = rs4.getString("LASTNAME");
					            Object address = rs4.getString("ADDRESS");
					            Object cantctnum = rs4.getString("CONTACTNO"); 
					             
					            PaymentFullTable.getModel().setValueAt(firstname, i, 0);
					            PaymentFullTable.getModel().setValueAt(lastname, i, 1);
					           PaymentFullTable.getModel().setValueAt(address, i, 2);
					          PaymentFullTable.getModel().setValueAt(cantctnum, i, 3); 
					          
					         
					        i++;
							}
						}
					}
					else
					{
						try
						{
						
							
							String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
							PreparedStatement pst2 = connection.prepareStatement(query);
							ResultSet rs2 = pst2.executeQuery();
							int i=0;
							while(rs2.next()) {
					           
					         
					            Object firstname = rs2.getString("FIRSTNAME");
					            Object lastname = rs2.getString("LASTNAME");
					            Object address = rs2.getString("ADDRESS");
					            Object contactnum = rs2.getString("CONTACTNO"); 
					             
					            PaymentFullTable.getModel().setValueAt(firstname, i, 0);
					            PaymentFullTable.getModel().setValueAt(lastname, i, 1);
					           PaymentFullTable.getModel().setValueAt(address, i, 2);
					          PaymentFullTable.getModel().setValueAt(contactnum, i, 3); 
					          
					         
					        i++;
					        }
							
						}catch(SQLException e1)
						{
							e1.printStackTrace();
						}
					}
									
						
					
				}catch(SQLException e1)
				{
					e1.printStackTrace();
				}
			}
		});
		PaymentFullDoneBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				PaymentFullfirstnameTextfield.setText("");
				PaymentFullLastnameTextfield.setText("");
				user.setEnabled(true);
				ADDDEBT.setEnabled(true);
				VIEW.setEnabled(true);	
				BILLOUT.setEnabled(true);	
				ABOUTUS.setEnabled(true);
				PaymentFullSearchBtn.getRootPane().setDefaultButton(null);
				PaymentDownPaymentBtn.setVisible(true);
				PaymentFullpaymentBtn.setVisible(true);
				
				
				PaymentFullSearchBtn.setVisible(false);
				PaymentFullfirstnameTextfield.setVisible(false);
				PaymentFullLastnameTextfield.setVisible(false);
				PaymentFullFirstnamelabel.setVisible(false);
				PaymentFulllastnamelabel.setVisible(false);
				PaymentFullDoneBtn.setVisible(false);
				PaymentFullResetBtn.setVisible(false);
				PaymentFullCancelBtn.setVisible(false);
				PaymentFullTableScrollpane.setVisible(false);
				PaymentFullSearchCustomertext.setVisible(false);
				PaymentFullListCofCustomers.setVisible(false);
			}
		});
		
		PaymentFullCancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				PaymentFullfirstnameTextfield.setText("");
				PaymentFullLastnameTextfield.setText("");
				user.setEnabled(true);
				ADDDEBT.setEnabled(true);
				VIEW.setEnabled(true);	
				BILLOUT.setEnabled(true);	
				ABOUTUS.setEnabled(true);
				billoutpanel.getRootPane().setDefaultButton(null);
				PaymentDownPaymentBtn.setVisible(true);
				PaymentFullpaymentBtn.setVisible(true);
				
				
				
				PaymentFullSearchBtn.setVisible(false);
				PaymentFullfirstnameTextfield.setVisible(false);
				PaymentFullLastnameTextfield.setVisible(false);
				PaymentFullFirstnamelabel.setVisible(false);
				PaymentFulllastnamelabel.setVisible(false);
				PaymentFullDoneBtn.setVisible(false);
				PaymentFullResetBtn.setVisible(false);
				PaymentFullCancelBtn.setVisible(false);
				PaymentFullTableScrollpane.setVisible(false);
				PaymentFullSearchCustomertext.setVisible(false);
				PaymentFullListCofCustomers.setVisible(false);
			}
		});

		
		PaymentFullResetBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				PaymentFullfirstnameTextfield.setText("");
				PaymentFullLastnameTextfield.setText("");
				try
				{
					String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
					PreparedStatement pst2 = connection.prepareStatement(query);
					ResultSet rs2 = pst2.executeQuery();
					
					if(rowcount != 0)
					{
						rowcount = 0;
						while(rs2.next())
						{
						    rowcount ++;
						}
					}
					else
					{
						while(rs2.next())
						{
						    rowcount ++;
						}
					}
					
					
				}catch(SQLException e1)
				{
					e1.printStackTrace();
				}
				
				PaymentFullTable.setModel(new DefaultTableModel(
						new Object[rowcount][rowcount] ,
						new String[] {
							"FIRSTNAME", "LASTNAME", "ADDRESS", "CONTACTNO."
						}
					) {
						Class[] columnTypes = new Class[] {
							Object.class, Object.class, String.class, Object.class
						};
						public Class getColumnClass(int columnIndex) {
							return columnTypes[columnIndex];
						}
						boolean[] columnEditables = new boolean[] {
							false, false, false
						};
						public boolean isCellEditable(int row, int column) {
							return columnEditables[column];
						}
					});
				PaymentFullTable.getColumnModel().getColumn(0).setResizable(false);
				PaymentFullTable.getColumnModel().getColumn(0).setPreferredWidth(200);
				PaymentFullTable.getColumnModel().getColumn(1).setResizable(false);
				PaymentFullTable.getColumnModel().getColumn(2).setResizable(false);
				PaymentFullTable.getColumnModel().getColumn(2).setPreferredWidth(300);
				PaymentFullTable.getColumnModel().getColumn(3).setResizable(false);
				
			
				
					
					try
					{
					
						
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) {
				           
				         
				            Object firstname = rs2.getString("FIRSTNAME");
				            Object lastname = rs2.getString("LASTNAME");
				            Object address = rs2.getString("ADDRESS");
				            Object contactnum = rs2.getString("CONTACTNO"); 
				             
				            PaymentFullTable.getModel().setValueAt(firstname, i, 0);
				            PaymentFullTable.getModel().setValueAt(lastname, i, 1);
				           PaymentFullTable.getModel().setValueAt(address, i, 2);
				          PaymentFullTable.getModel().setValueAt(contactnum, i, 3); 
				          
				         
				        i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
			}
		});
		PaymentFullTable.addMouseListener(new MouseAdapter() {
			
			public void mouseClicked(MouseEvent arg0) 
			{
				 if (PaymentFullTable.getSelectedRow() > -1) 
				 {
					 frame.setEnabled(false);
					 PaymentFullAmountFrame.setVisible(true);
					c.setFirstname(PaymentFullTable.getValueAt(PaymentFullTable.getSelectedRow(), 0).toString().toUpperCase());
					c.setLastname(PaymentFullTable.getValueAt(PaymentFullTable.getSelectedRow(), 1).toString().toUpperCase());
					
				 }
			}
		});
		
		paymentfullconfirmbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				
				double balance1 = 0.00,updatebalance = 0.00;
				String tempzerodebt = "",Amounterror = "";
				
				
				 try
					{
					
						
						String query = "SELECT customerid FROM CUSTOMER where firstname = '" 
										+ c.getFirstname() 
										+"' AND LASTNAME ='" + c.getLastname()
										+"'";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) {
				           
				         
				            c.setCostumercode(rs2.getString("CUSTOMERID"));
				           
				        i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					try
					{
					
						
						String query = "SELECT BALANCEID FROM BALANCE_INVOICE where CUSTOMERID ="
										+ "'" + c.getCostumercode() +"'";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) {
				           
				         
				           c.setBalanceid(rs2.getString("BALANCEID"));
				           
				        i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					
					try
					{
					
						
						String query = "SELECT BALANCE FROM BALANCE where BALANCEID = '"
										+  c.getBalanceid() +"' AND DATE_UPDATED ="
										+ "(SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE "
										+ "BALANCEID = '"+ c.getBalanceid() + "') AND "
										+ "TIME_UPDATED = (SELECT MAX(TIME_UPDATED) FROM"
										+ " BALANCE WHERE BALANCEID ='" + c.getBalanceid() + "' AND DATE_UPDATED"
										+ "= (SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE BALANCEID"
										+ "='"+ c.getBalanceid() + "'))";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) {
				           
				         
				           balance1 = rs2.getDouble("BALANCE");
				        i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
				
				

				
				try {
					 	Double.parseDouble(paymentfulltamounttextfield.getText());  
				      }catch ( NumberFormatException exc ) 
				 				  {
						              Amounterror = "error";
						          }
				
				
				if(paymentfulltamounttextfield.getText() == "0")
				{
					tempzerodebt = "error";
				}
				else
				{
					tempzerodebt = "";
				}
				
				
				
				if(tempzerodebt == "error")
				{
					JOptionPane.showMessageDialog(null,"YOU NEED TO INPUT PAYMENT AMOUNT!, 0 PAYMENT AMOUNT IS NOT ACCEPTED!");
				}
				else if(Amounterror.equals("error"))
				{
					JOptionPane.showMessageDialog(null,"UNDEFINED AMOUNT!");
				}
				else if(Double.parseDouble(paymentfulltamounttextfield.getText()) < balance1)
				{
					JOptionPane.showMessageDialog(null,"YOUR PAYMENT AMOUNT MUST BE HIGHER OR EQUAL THAN YOUR TOTAL BALANCE!");
				}
				else
				{
					String existingpaymentid = "";
					c.setPaymentamount(Double.parseDouble(paymentfulltamounttextfield.getText()));
					c.setBalance(balance1);
					double change = 0.00;
					Time timefullpaymenttransaction = null;
					Date datefulpaymenttransaction = null;
					
					
								int paymentidtemp;
								String paymentidgetter = "";
								String paymentidget = "";
								do
								{
								paymentidtemp = rndm.nextInt(999);
								if(Integer.toString(paymentidtemp).length() == 1)
								{
									paymentidgetter = "000" + Integer.toString(paymentidtemp);
								}
								else if(Integer.toString(paymentidtemp).length() == 2)
								{
									paymentidgetter = "00" + Integer.toString(paymentidtemp);
								}
								else if(Integer.toString(paymentidtemp).length() == 3)
								{
									paymentidgetter = "0" + Integer.toString(paymentidtemp);
								}
								else
								{
									paymentidgetter = Integer.toString(paymentidtemp);
								}
								
								try
								{
									String query = "SELECT PAYMENTID FROM PAYMENT_INVOICE where PAYMENTID = '" + paymentidgetter +  "'";
									PreparedStatement pst1 = connection.prepareStatement(query);
									ResultSet rs1 = pst1.executeQuery();
									
									if(!rs1.next())
									{
										
							            paymentidget = "";
							        }
									else
									{
										paymentidget = "error";
									}
								}catch(SQLException e1)
								{
									e1.printStackTrace();
								}
								}while(paymentidget == "error");
								c.setPaymentid(paymentidgetter);
								try {
									String query = "INSERT INTO PAYMENT_TRANSACTION VALUES('"+ c.getPaymentid() + "'," 
											 + c.getPaymentamount() + "," 
											 + "CURRENT TIMESTAMP" + "," + "CURRENT TIMESTAMP"
											 + ",'" + "FP" + "')"  ;
																				  
									PreparedStatement pst3 = connection.prepareStatement(query);
									pst3.executeUpdate();
								} catch (SQLException e1) {
									
									e1.printStackTrace();
								}
								
								try {
									String query = "INSERT INTO PAYMENT_INVOICE VALUES('"+ c.getCostumercode() + "','" 
																						 + c.getPaymentid() + "','" 
																						 + c.getBalanceid() +"')"  ;
																				  
									PreparedStatement pst3 = connection.prepareStatement(query);
									pst3.executeUpdate();
								} catch (SQLException e1) {
									
									e1.printStackTrace();
								}
								
								
								change = c.getPaymentamount() - c.getBalance();
								try
								{
								
									
									String query = "SELECT PAYMENT_DATE,PAYMENT_TIME  FROM PAYMENT_TRANSACTION where PAYMENTID ="
													+ "'" + c.getPaymentid() +"'";
									PreparedStatement pst2 = connection.prepareStatement(query);
									ResultSet rs2 = pst2.executeQuery();
									int i=0;
									while(rs2.next()) {
							           
							         
										timefullpaymenttransaction = rs2.getTime("PAYMENT_TIME");
										datefulpaymenttransaction= rs2.getDate("PAYMENT_DATE");
							           
							        i++;
							        }
									
								}catch(SQLException e1)
								{
									e1.printStackTrace();
								}
								updatebalance = 0;
								
								try
								{
								
									
									String query = "INSERT INTO BALANCE VALUES('" + c.getBalanceid() 
													+"'," + updatebalance + ",CURRENT TIMESTAMP"
															+ ",CURRENT TIMESTAMP)";
													
									PreparedStatement pst2 = connection.prepareStatement(query);
									pst2.executeUpdate();
									
									
								}catch(SQLException e1)
								{
									e1.printStackTrace();
								}
								
								paymentfulltamounttextfield.setText("");
								PaymentFullTransactionCustomerNameText.setText(c.getFirstname() + "      " + c.getLastname());
								PaymentFullTransactionCustomerBalText.setText(Double.toString(c.getBalance()));
								PaymentFullTransactionPaymentAmountText.setText(Double.toString(c.getPaymentamount()));
								PaymentFullTransactionChangedText.setText(Double.toString(change));
								PaymentFullDateAndTimeText.setText(dateFormat.format(datefulpaymenttransaction) + "        " + timeFormat.format(timefullpaymenttransaction));
								PaymentFullAmountFrame.setVisible(false);
								PaymentFullTransaction.setVisible(true);
					

				}
				
			}
		});
		
		PaymentFullTransactionConfirmBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				frame.setEnabled(true);

				double balance1 = 0.00;
				int rowcountforselectingbalance = 0;
				try
				{
					String query = "SELECT balanceid from balance_invoice" ;
					PreparedStatement pst2 = connection.prepareStatement(query);
					ResultSet rs2 = pst2.executeQuery();
					while(rs2.next())
					{
						String tempid = "";
						tempid = rs2.getString("BALANCEID");
						try
						{
						
							
							String query1 = "SELECT BALANCE FROM BALANCE where BALANCEID = '"
											+  tempid +"' AND DATE_UPDATED ="
											+ "(SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE "
											+ "BALANCEID = '"+ tempid + "') AND "
											+ "TIME_UPDATED = (SELECT MAX(TIME_UPDATED) FROM"
											+ " BALANCE WHERE BALANCEID ='" + tempid + "' AND DATE_UPDATED"
											+ "= (SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE BALANCEID"
											+ "='"+ tempid + "'))";
							PreparedStatement pst3 = connection.prepareStatement(query1);
							ResultSet rs3 = pst3.executeQuery();
							int z=0;
							while(rs3.next()) {
					           
					         
					           balance1 = rs3.getDouble("BALANCE");
					        z++;
					        }
							
						}catch(SQLException e1)
						{
							e1.printStackTrace();
						}
						if(balance1 > 0)
						{
							rowcountforselectingbalance ++;
							
						}
						else
						{
							continue;
						}
						
						
					}			
				}catch(SQLException e1)
				{
					e1.printStackTrace();
				}
				if(rowcountforselectingbalance == 0)
				{
					PaymentFullTable.setModel(new DefaultTableModel(
							new Object[0][0] ,
							new String[] {
								"FIRSTNAME", "LASTNAME", "ADDRESS", "CONTACTNO."
							}
						) {
							Class[] columnTypes = new Class[] {
								Object.class, Object.class, String.class, Object.class
							};
							public Class getColumnClass(int columnIndex) {
								return columnTypes[columnIndex];
							}
							boolean[] columnEditables = new boolean[] {
								false, false, false
							};
							public boolean isCellEditable(int row, int column) {
								return columnEditables[column];
							}
						});
					PaymentFullTable.getColumnModel().getColumn(0).setResizable(false);
					PaymentFullTable.getColumnModel().getColumn(0).setPreferredWidth(200);
					PaymentFullTable.getColumnModel().getColumn(1).setResizable(false);
					PaymentFullTable.getColumnModel().getColumn(2).setResizable(false);
					PaymentFullTable.getColumnModel().getColumn(2).setPreferredWidth(300);
					PaymentFullTable.getColumnModel().getColumn(3).setResizable(false);
						          
					        
				}
				else
				{
					String balanceidtemp[] = new String[rowcountforselectingbalance];
					String outputforbalanceidtemp = "";
					
					try
					{
						String query = "SELECT balanceid from balance_invoice" ;
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i =0;
						while(rs2.next())
						{
							
							String tempid = "";
							tempid = rs2.getString("BALANCEID");
							try
							{
							
								
								String query1 = "SELECT BALANCE FROM BALANCE where BALANCEID = '"
												+  tempid +"' AND DATE_UPDATED ="
												+ "(SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE "
												+ "BALANCEID = '"+ tempid + "') AND "
												+ "TIME_UPDATED = (SELECT MAX(TIME_UPDATED) FROM"
												+ " BALANCE WHERE BALANCEID ='" + tempid + "' AND DATE_UPDATED"
												+ "= (SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE BALANCEID"
												+ "='"+ tempid + "'))";
								PreparedStatement pst3 = connection.prepareStatement(query1);
								ResultSet rs3 = pst3.executeQuery();
								int z=0;
								while(rs3.next()) {
						           
						         
						           balance1 = rs3.getDouble("BALANCE");
						        z++;
						        }
								
							}catch(SQLException e1)
							{
								e1.printStackTrace();
							}
							if(balance1 > 0)
							{
								balanceidtemp[i] = tempid;
								i++;
								
							}
							else
							{
								continue;
							}
							
							
						}			
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					
					int k;
					for(k=0;k<=rowcountforselectingbalance-1;k++)
					{
						if(balanceidtemp[k].equals(balanceidtemp[balanceidtemp.length-1]))
						{
							outputforbalanceidtemp += "'" + balanceidtemp[k] + "'" ;
						}
						else
						{
							outputforbalanceidtemp += "'" + balanceidtemp[k] + "'," ;
						}
					}
					
					String customerids[] = new String[rowcountforselectingbalance];
					String outputforcustomerid = "";
					
					try
					{
					
						
						String query = "SELECT customerid from balance_invoice where balanceid in ("
										+outputforbalanceidtemp + ")";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) 
						{
							
							customerids[i] = rs2.getString("CUSTOMERID");
							
							if(customerids[i].equals(customerids[customerids.length-1]))
							{
								outputforcustomerid += "'" + customerids[i] + "'" ;
							}
							else
							{
								outputforcustomerid += "'" + customerids[i] + "'," ;
							}
							
						i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					
					try
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER where customerid in ("
										+ outputforcustomerid +")";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						
						if(rowcount != 0)
						{
							rowcount = 0;
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						else
						{
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					
					PaymentFullTable.setModel(new DefaultTableModel(
							new Object[rowcount][rowcount] ,
							new String[] {
								"FIRSTNAME", "LASTNAME", "ADDRESS", "CONTACTNO."
							}
						) {
							Class[] columnTypes = new Class[] {
								Object.class, Object.class, String.class, Object.class
							};
							public Class getColumnClass(int columnIndex) {
								return columnTypes[columnIndex];
							}
							boolean[] columnEditables = new boolean[] {
								false, false, false
							};
							public boolean isCellEditable(int row, int column) {
								return columnEditables[column];
							}
						});
					PaymentFullTable.getColumnModel().getColumn(0).setResizable(false);
					PaymentFullTable.getColumnModel().getColumn(0).setPreferredWidth(200);
					PaymentFullTable.getColumnModel().getColumn(1).setResizable(false);
					PaymentFullTable.getColumnModel().getColumn(2).setResizable(false);
					PaymentFullTable.getColumnModel().getColumn(2).setPreferredWidth(300);
					PaymentFullTable.getColumnModel().getColumn(3).setResizable(false);
					
				
					
						
						try
						{
						
							
							String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER where customerid in ("
											+ outputforcustomerid +")" ;
							PreparedStatement pst2 = connection.prepareStatement(query);
							ResultSet rs2 = pst2.executeQuery();
							int i=0;
							while(rs2.next()) {
					           
					         
					            Object firstname = rs2.getString("FIRSTNAME");
					            Object lastname = rs2.getString("LASTNAME");
					            Object address = rs2.getString("ADDRESS");
					            Object contactnum = rs2.getString("CONTACTNO"); 
					             
					            PaymentFullTable.getModel().setValueAt(firstname, i, 0);
					            PaymentFullTable.getModel().setValueAt(lastname, i, 1);
					           PaymentFullTable.getModel().setValueAt(address, i, 2);
					          PaymentFullTable.getModel().setValueAt(contactnum, i, 3); 
					          
					         
					        i++;
					        }
							
						}catch(SQLException e1)
						{
							e1.printStackTrace();
						}
				}
				
				

				
					PaymentFullTransaction.setVisible(false);
					JOptionPane.showMessageDialog(null,"TRANSACTION SUCCESFUL!");
				
			}
		});
		
		PaymentFullSearchBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				search.setFirstname(PaymentFullfirstnameTextfield.getText().toUpperCase());
				search.setLastname(PaymentFullLastnameTextfield.getText().toUpperCase());
				String con1 = "false", con2 = "false", con3 = "false";
				
				if((search.getFirstname().length() != 0) && (search.getLastname().length() != 0))
				{
					con1 = "true";
				}
				else if((search.getFirstname().length() != 0) && (search.getLastname().length() == 0))
				{
					con2 = "true";
				}
				else if((search.getFirstname().length() == 0) && (search.getLastname().length() != 0))
				{
					con3 = "true";
				}
				
				
				
				if(con1 == "true" )
				{
					try
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
								+ search.getFirstname() + "' AND lastname = '" 
								+ search.getLastname()  + "'" ;
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						if(rowcount != 0)
						{
							rowcount = 0;
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						else
						{
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
				}
				else if( con2 == "true")
				{
					try
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
								+ search.getFirstname() + "'";
								 
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						if(rowcount != 0)
						{
							rowcount = 0;
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						else
						{
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
				}
				
				else if(con3 == "true")
				{
					try
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE lastname = '" 
								+ search.getLastname() + "'";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						if(rowcount != 0)
						{
							rowcount = 0;
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						else
						{
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
				}
				else
				{
					try
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						
						if(rowcount != 0)
						{
							rowcount = 0;
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						else
						{
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
				}
				
				
				PaymentFullTable.setModel(new DefaultTableModel(
						new Object[rowcount][rowcount] ,
						new String[] {
							"FIRSTNAME", "LASTNAME", "ADDRESS", "CONTACTNO."
						}
					) {
						Class[] columnTypes = new Class[] {
							Object.class, Object.class, String.class, Object.class
						};
						public Class getColumnClass(int columnIndex) {
							return columnTypes[columnIndex];
						}
						boolean[] columnEditables = new boolean[] {
							false, false, false
						};
						public boolean isCellEditable(int row, int column) {
							return columnEditables[column];
						}
					});
				PaymentFullTable.getColumnModel().getColumn(0).setResizable(false);
				PaymentFullTable.getColumnModel().getColumn(0).setPreferredWidth(200);
				PaymentFullTable.getColumnModel().getColumn(1).setResizable(false);
				PaymentFullTable.getColumnModel().getColumn(2).setResizable(false);
				PaymentFullTable.getColumnModel().getColumn(2).setPreferredWidth(300);
				PaymentFullTable.getColumnModel().getColumn(3).setResizable(false);
				
				try
				{
				
					if(con1 == "true")
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
								+ search.getFirstname() + "' AND lastname = '" 
								+ search.getLastname()  + "'" ;
				
				
						PreparedStatement pst3 = connection.prepareStatement(query);
						ResultSet rs3 = pst3.executeQuery();
				
						if(!rs3.next())
						{
							JOptionPane.showMessageDialog(null, "NO COSTUMER FOUND!");
						}
						else
						{
							String query1 = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
											+ search.getFirstname() + "' AND lastname = '" 
											+ search.getLastname()  + "'"  ;
			
			
							PreparedStatement pst4 = connection.prepareStatement(query1);
							ResultSet rs4 = pst4.executeQuery();
							int i=0;
							while(rs4.next()) 
							{
						           
						         
					            Object firstname = rs4.getString("FIRSTNAME");
					            Object lastname = rs4.getString("LASTNAME");
					            Object address = rs4.getString("ADDRESS");
					            Object cantctnum = rs4.getString("CONTACTNO"); 
					             
					            PaymentFullTable.getModel().setValueAt(firstname, i, 0);
					            PaymentFullTable.getModel().setValueAt(lastname, i, 1);
					           PaymentFullTable.getModel().setValueAt(address, i, 2);
					          PaymentFullTable.getModel().setValueAt(cantctnum, i, 3); 
					          
					         
					        i++;
							}

						}
					}	
					else if(con2 == "true")
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
								+ search.getFirstname() + "'";
								 
						PreparedStatement pst3 = connection.prepareStatement(query);
						ResultSet rs3 = pst3.executeQuery();
						
						if(!rs3.next())
						{
							JOptionPane.showMessageDialog(null, "NO COSTUMER FOUND!");
						}
						else
						{
							String query1 = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
											+ search.getFirstname() + "'"  ;
			
			
							PreparedStatement pst4 = connection.prepareStatement(query1);
							ResultSet rs4 = pst4.executeQuery();
							int i=0;
							while(rs4.next()) 
							{
						           
						         
					            Object firstname = rs4.getString("FIRSTNAME");
					            Object lastname = rs4.getString("LASTNAME");
					            Object address = rs4.getString("ADDRESS");
					            Object cantctnum = rs4.getString("CONTACTNO"); 
					             
					            PaymentFullTable.getModel().setValueAt(firstname, i, 0);
					            PaymentFullTable.getModel().setValueAt(lastname, i, 1);
					           PaymentFullTable.getModel().setValueAt(address, i, 2);
					          PaymentFullTable.getModel().setValueAt(cantctnum, i, 3); 
					          
					         
					        i++;
							}
						}
					}	
					else if(con3 == "true")
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE lastname = '" 
								+ search.getLastname() + "'";
						PreparedStatement pst3 = connection.prepareStatement(query);
						ResultSet rs3 = pst3.executeQuery();
						
						if(!rs3.next())
						{
							JOptionPane.showMessageDialog(null, "NO COSTUMER FOUND!");
						}
						else
						{
							String query1 = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE lastname = '" 
											+ search.getLastname() + "'"  ;
			
			
							PreparedStatement pst4 = connection.prepareStatement(query1);
							ResultSet rs4 = pst4.executeQuery();
							int i=0;
							while(rs4.next()) 
							{
						           
						         
					            Object firstname = rs4.getString("FIRSTNAME");
					            Object lastname = rs4.getString("LASTNAME");
					            Object address = rs4.getString("ADDRESS");
					            Object cantctnum = rs4.getString("CONTACTNO"); 
					             
					            PaymentFullTable.getModel().setValueAt(firstname, i, 0);
					            PaymentFullTable.getModel().setValueAt(lastname, i, 1);
					           PaymentFullTable.getModel().setValueAt(address, i, 2);
					          PaymentFullTable.getModel().setValueAt(cantctnum, i, 3); 
					          
					         
					        i++;
							}
						}
					}
					else
					{
						try
						{
						
							
							String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
							PreparedStatement pst2 = connection.prepareStatement(query);
							ResultSet rs2 = pst2.executeQuery();
							int i=0;
							while(rs2.next()) {
					           
					         
					            Object firstname = rs2.getString("FIRSTNAME");
					            Object lastname = rs2.getString("LASTNAME");
					            Object address = rs2.getString("ADDRESS");
					            Object contactnum = rs2.getString("CONTACTNO"); 
					             
					            PaymentFullTable.getModel().setValueAt(firstname, i, 0);
					            PaymentFullTable.getModel().setValueAt(lastname, i, 1);
					           PaymentFullTable.getModel().setValueAt(address, i, 2);
					          PaymentFullTable.getModel().setValueAt(contactnum, i, 3); 
					          
					         
					        i++;
					        }
							
						}catch(SQLException e1)
						{
							e1.printStackTrace();
						}
					}
									
						
					
				}catch(SQLException e1)
				{
					e1.printStackTrace();
				}

				
			}
		});
		
		paymentfullcancelbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				frame.setEnabled(true);
				paymentfulltamounttextfield.setText("");
				PaymentFullAmountFrame.setVisible(false);
				frame.show();
			}
		});
		
		Viewtable.addMouseListener(new MouseAdapter() {
			
			public void mouseClicked(MouseEvent arg0) 
			{
				 if (Viewtable.getSelectedRow() > -1) 
				 {
					 ViewSearch.setVisible(false);
					 ViewResetButton.setVisible(false);
					 Viewfirstnamefield.setVisible(false);
					 Viewlastnamefield.setVisible(false);
					 Viewfistnamelabel.setVisible(false);
					 Viewlastnamelabel.setVisible(false);
					 ViewtableScrollpane.setVisible(false);
					 Viewsearchcustomerlabel.setVisible(false);
					 Viewlistofcustomers.setVisible(false);
					 user.setEnabled(false);
					 ADDDEBT.setEnabled(false);
					 VIEW.setEnabled(false);	
					 BILLOUT.setEnabled(false);	
					 ABOUTUS.setEnabled(false);
					 
					 
					 ViewSelectedpersonalinfo.setVisible(true);
					 ViewSelectedfirstname.setVisible(true);
					 ViewSelectedlastname.setVisible(true);
					 ViewSelectedaddress.setVisible(true);
					 ViewSelectedcontactno.setVisible(true);
					 ViewSelectedfirstnametext.setVisible(true);
					 ViewSelectedlastnametext.setVisible(true);
					 ViewSelectedaddresstext.setVisible(true);
					 ViewSelectedcontacttext.setVisible(true);
					 ViewDebtinformationscrollpane.setVisible(true);
					 ViewPaymentHistoryScrollpane.setVisible(true);
					 ViewSelectedBalance.setVisible(true);
					 ViewSelectedbalanceText.setVisible(true);
					 Vieweditinformation.setVisible(true);
					 ViewDebtInformation.setVisible(true);
					 ViewPaymentHistory.setVisible(true);
					 ViewReturnButton.setVisible(true);
					 

					 ViewSelectedfirstnametext.setText(Viewtable.getValueAt(Viewtable.getSelectedRow(), 0).toString().toUpperCase());
					 ViewSelectedlastnametext.setText(Viewtable.getValueAt(Viewtable.getSelectedRow(), 1).toString().toUpperCase());
					 ViewSelectedaddresstext.setText(Viewtable.getValueAt(Viewtable.getSelectedRow(), 2).toString().toUpperCase());
					 ViewSelectedcontacttext.setText(Viewtable.getValueAt(Viewtable.getSelectedRow(), 3).toString().toUpperCase());
					 c.setFirstname(ViewSelectedfirstnametext.getText());
					 c.setLastname(ViewSelectedlastnametext.getText());
					 String balance = "";
					 String paymentidcondition = "";
					 
					 try
						{
						
							
							String query = "SELECT customerid FROM CUSTOMER where firstname = '" 
											+ ViewSelectedfirstnametext.getText() 
											+"' AND LASTNAME ='" + ViewSelectedlastnametext.getText() 
											+"'";
							PreparedStatement pst2 = connection.prepareStatement(query);
							ResultSet rs2 = pst2.executeQuery();
							int i=0;
							while(rs2.next()) {
					           
					         
					            c.setCostumercode(rs2.getString("CUSTOMERID"));
					           
					        i++;
					        }
							
						}catch(SQLException e1)
						{
							e1.printStackTrace();
						}
					 try
						{
						
							
							String query = "SELECT PAYMENTID FROM PAYMENT_INVOICE where CUSTOMERID ="
											+ "'" + c.getCostumercode() +"'";
							PreparedStatement pst2 = connection.prepareStatement(query);
							ResultSet rs2 = pst2.executeQuery();
							if(!rs2.next()) 
							{
					           
								paymentidcondition = "false";
					        }
							else
							{
								c.setPaymentid(rs2.getString("PAYMENTID"));
								paymentidcondition = "true";
							}
							
						}catch(SQLException e1)
						{
							e1.printStackTrace();
						}
					 try
						{
						
							
							String query = "SELECT BALANCEID FROM BALANCE_INVOICE where CUSTOMERID ="
											+ "'" + c.getCostumercode() +"'";
							PreparedStatement pst2 = connection.prepareStatement(query);
							ResultSet rs2 = pst2.executeQuery();
							int i=0;
							while(rs2.next()) {
					           
					         
					           c.setBalanceid(rs2.getString("BALANCEID"));
					           
					        i++;
					        }
							
						}catch(SQLException e1)
						{
							e1.printStackTrace();
						}
					 try
						{
						
							
							String query = "SELECT BALANCE FROM BALANCE where BALANCEID = '"
											+  c.getBalanceid() +"' AND DATE_UPDATED ="
											+ "(SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE "
											+ "BALANCEID = '"+ c.getBalanceid() + "') AND "
											+ "TIME_UPDATED = (SELECT MAX(TIME_UPDATED) FROM"
											+ " BALANCE WHERE BALANCEID ='" + c.getBalanceid() + "' AND DATE_UPDATED"
											+ "= (SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE BALANCEID"
											+ "='"+ c.getBalanceid() + "'))";
							PreparedStatement pst2 = connection.prepareStatement(query);
							ResultSet rs2 = pst2.executeQuery();
							int i=0;
							while(rs2.next()) {
					           
					         
					            balance = rs2.getString("BALANCE");
					        i++;
					        }
							
						}catch(SQLException e1)
						{
						
						}
					 	ViewSelectedbalanceText.setText(balance);
					 	try
						{
							String query = "SELECT DEBT_AMOUNT,DATE_ADDED,TIME_ADDED FROM DEBT WHERE CUSTOMERID = "
											+ "(SELECT CUSTOMERID FROM CUSTOMER WHERE FIRSTNAME ='" + ViewSelectedfirstnametext.getText() 
											+"' AND LASTNAME ='" + ViewSelectedlastnametext.getText() +"')";
							PreparedStatement pst2 = connection.prepareStatement(query);
							ResultSet rs2 = pst2.executeQuery();
							if(rowcount != 0)
							{
								rowcount = 0;
								while(rs2.next())
								{
								    rowcount ++;
								}
							}
							else
							{
								while(rs2.next())
								{
								    rowcount ++;
								}
							}
							
							
						}catch(SQLException e1)
						{
							e1.printStackTrace();
						}
						
						ViewDebtinformationtable.setModel(new DefaultTableModel(
								new Object[rowcount][rowcount] ,
								new String[] {
									"DEBTAMOUNT", "DATE ADDED", "TIME ADDED"
								}
							) {
								Class[] columnTypes = new Class[] {
									Object.class, Object.class, String.class, Object.class
								};
								public Class getColumnClass(int columnIndex) {
									return columnTypes[columnIndex];
								}
								boolean[] columnEditables = new boolean[] {
									false, false, false
								};
								public boolean isCellEditable(int row, int column) {
									return columnEditables[column];
								}
							});
						ViewDebtinformationtable.getColumnModel().getColumn(0).setResizable(false);
						ViewDebtinformationtable.getColumnModel().getColumn(0).setPreferredWidth(200);
						ViewDebtinformationtable.getColumnModel().getColumn(1).setResizable(false);
						ViewDebtinformationtable.getColumnModel().getColumn(2).setResizable(false);
						
						try
						{
							String query1 = "SELECT DEBT_AMOUNT,DATE_ADDED,TIME_ADDED FROM DEBT WHERE CUSTOMERID = "
											+ "(SELECT CUSTOMERID FROM CUSTOMER WHERE FIRSTNAME ='" + ViewSelectedfirstnametext.getText() 
											+"' AND LASTNAME ='" + ViewSelectedlastnametext.getText() +"') ORDER BY DATE_ADDED,TIME_ADDED" ;
					
					
							PreparedStatement pst4 = connection.prepareStatement(query1);
							ResultSet rs4 = pst4.executeQuery();
							int i=0;
							while(rs4.next()) 
							{
						           
						         
					            Object debtamount = rs4.getString("DEBT_AMOUNT");
					            Date date = rs4.getDate("DATE_ADDED");
					            Time time = rs4.getTime("TIME_ADDED");
					            Object datetemp,timetemp;
					            
					            datetemp = dateFormat.format(date);
					            timetemp = timeFormat.format(time);
					            
					            ViewDebtinformationtable.getModel().setValueAt(debtamount, i, 0);
					            ViewDebtinformationtable.getModel().setValueAt(datetemp, i, 1);
					           ViewDebtinformationtable.getModel().setValueAt(timetemp, i, 2);
					         
					        i++;
							}
						}catch(SQLException e1)
						{
							e1.printStackTrace();
						}
			         	
						if(paymentidcondition.equals("false"))
						{
							ViewtablePaymentHistory.setModel(new DefaultTableModel(
									new Object[0][0] ,
									new String[] {
										"PAYMENT AMOUNT", "PAYMENT DATE", "PAYMENT TIME","PAYMENT METHOD"
									}
								) {
									Class[] columnTypes = new Class[] {
										Object.class, Object.class, String.class, Object.class
									};
									public Class getColumnClass(int columnIndex) {
										return columnTypes[columnIndex];
									}
									boolean[] columnEditables = new boolean[] {
										false, false, false
									};
									public boolean isCellEditable(int row, int column) {
										return columnEditables[column];
									}
								});
							ViewtablePaymentHistory.getColumnModel().getColumn(0).setResizable(false);
							ViewtablePaymentHistory.getColumnModel().getColumn(1).setResizable(false);
							ViewtablePaymentHistory.getColumnModel().getColumn(2).setResizable(false);
							ViewtablePaymentHistory.getColumnModel().getColumn(3).setResizable(false);
						}
						else
						{
							int rowcountforselectingbalance = 0;
							try
							{
								String query = "select paymentid from payment_invoice where customerid = '"
												+ c.getCostumercode() + "'";
								
								PreparedStatement pst2 = connection.prepareStatement(query);
								ResultSet rs2 = pst2.executeQuery();
								if(rowcountforselectingbalance != 0)
								{
									rowcountforselectingbalance = 0;
									while(rs2.next())
									{
										rowcountforselectingbalance ++;
									}
								}
								else
								{
									while(rs2.next())
									{
										rowcountforselectingbalance ++;
									}
								}
								
								
							}catch(SQLException e1)
							{
								e1.printStackTrace();
							}
							
							
							String balanceidtemp[] = new String[rowcountforselectingbalance];
							String outputforbalanceidtemp = "";
							
							try
							{
								String query = "select paymentid from payment_invoice where customerid = '"
												+ c.getCostumercode() + "'";
								PreparedStatement pst2 = connection.prepareStatement(query);
								ResultSet rs2 = pst2.executeQuery();
								int i = 0;
								while(rs2.next()) 
								{
									
									balanceidtemp[i] = rs2.getString("PAYMENTID");
									
									if(balanceidtemp[i].equals(balanceidtemp[balanceidtemp.length-1]))
									{
										outputforbalanceidtemp += "'" + balanceidtemp[i] + "'" ;
									}
									else
									{
										outputforbalanceidtemp += "'" + balanceidtemp[i] + "'," ;
									}
									
								i++;
						        }
								
								
							}catch(SQLException e1)
							{
								e1.printStackTrace();
							}
							
							
							
							
							try
							{
								String query = "SELECT PAYMENT_AMOUNT,PAYMENT_DATE,PAYMENT_TIME,PAYMENT_METHOD FROM PAYMENT_TRANSACTION  WHERE PAYMENTID IN("
												+ outputforbalanceidtemp + ")";
								PreparedStatement pst2 = connection.prepareStatement(query);
								ResultSet rs2 = pst2.executeQuery();
								if(rowcount != 0)
								{
									rowcount = 0;
									while(rs2.next())
									{
									    rowcount ++;
									}
								}
								else
								{
									while(rs2.next())
									{
									    rowcount ++;
									}
								}
								
								
							}catch(SQLException e1)
							{
								e1.printStackTrace();
							}
							
							ViewtablePaymentHistory.setModel(new DefaultTableModel(
									new Object[rowcount][rowcount] ,
									new String[] {
										"PAYMENT AMOUNT", "PAYMENT DATE", "PAYMENT TIME","PAYMENT METHOD"
									}
								) {
									Class[] columnTypes = new Class[] {
										Object.class, Object.class, String.class, Object.class
									};
									public Class getColumnClass(int columnIndex) {
										return columnTypes[columnIndex];
									}
									boolean[] columnEditables = new boolean[] {
										false, false, false
									};
									public boolean isCellEditable(int row, int column) {
										return columnEditables[column];
									}
								});
							ViewtablePaymentHistory.getColumnModel().getColumn(0).setResizable(false);
							ViewtablePaymentHistory.getColumnModel().getColumn(1).setResizable(false);
							ViewtablePaymentHistory.getColumnModel().getColumn(2).setResizable(false);
							ViewtablePaymentHistory.getColumnModel().getColumn(3).setResizable(false);
							
							try
							{
								String query1 = "SELECT PAYMENT_AMOUNT,PAYMENT_DATE,PAYMENT_TIME,PAYMENT_METHOD FROM PAYMENT_TRANSACTION  WHERE PAYMENTID IN("
										+ outputforbalanceidtemp + ")";
						
						
								PreparedStatement pst4 = connection.prepareStatement(query1);
								ResultSet rs4 = pst4.executeQuery();
								int i=0;
								while(rs4.next()) 
								{
							           
							         
						            Object paymentamount = rs4.getString("PAYMENT_AMOUNT");
						            Date date = rs4.getDate("PAYMENT_DATE");
						            Time time = rs4.getTime("PAYMENT_TIME");
						            String paymentmethod = rs4.getString("PAYMENT_METHOD");
						            String paymentmethodview = "";
						            
						            if(paymentmethod.equals("FP"))
						            {
						            	paymentmethodview = "FULL PAYMENT";
						            }
						            else
						            {
						            	paymentmethodview = "PARTIAL PAYMENT";
						            }
						            
						            Object datetemp,timetemp;
						            
						            datetemp = dateFormat.format(date);
						            timetemp = timeFormat.format(time);
						            
						            ViewtablePaymentHistory.getModel().setValueAt(paymentamount, i, 0);
						            ViewtablePaymentHistory.getModel().setValueAt(datetemp, i, 1);
						           ViewtablePaymentHistory.getModel().setValueAt(timetemp, i, 2);
						           ViewtablePaymentHistory.getModel().setValueAt(paymentmethodview, i, 3);
						         
						        i++;
								}
							}catch(SQLException e1)
							{
								e1.printStackTrace();
							}
						}
			         	
				 }
			}
		});
		
		ViewResetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				Viewfirstnamefield.setText("");
				Viewlastnamefield.setText("");
				try
				{
					String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
					PreparedStatement pst2 = connection.prepareStatement(query);
					ResultSet rs2 = pst2.executeQuery();
					
					if(rowcount != 0)
					{
						rowcount = 0;
						while(rs2.next())
						{
						    rowcount ++;
						}
					}
					else
					{
						while(rs2.next())
						{
						    rowcount ++;
						}
					}
					
					
				}catch(SQLException e1)
				{
					e1.printStackTrace();
				}
				
				Viewtable.setModel(new DefaultTableModel(
						new Object[rowcount][rowcount] ,
						new String[] {
							"FIRSTNAME", "LASTNAME", "ADDRESS", "CONTACTNO."
						}
					) {
						Class[] columnTypes = new Class[] {
							Object.class, Object.class, String.class, Object.class
						};
						public Class getColumnClass(int columnIndex) {
							return columnTypes[columnIndex];
						}
						boolean[] columnEditables = new boolean[] {
							false, false, false
						};
						public boolean isCellEditable(int row, int column) {
							return columnEditables[column];
						}
					});
				Viewtable.getColumnModel().getColumn(0).setResizable(false);
				Viewtable.getColumnModel().getColumn(0).setPreferredWidth(200);
				Viewtable.getColumnModel().getColumn(1).setResizable(false);
				Viewtable.getColumnModel().getColumn(2).setResizable(false);
				Viewtable.getColumnModel().getColumn(2).setPreferredWidth(300);
				Viewtable.getColumnModel().getColumn(3).setResizable(false);
				
			
				
					
					try
					{
					
						
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) {
				           
				         
				            Object firstname = rs2.getString("FIRSTNAME");
				            Object lastname = rs2.getString("LASTNAME");
				            Object address = rs2.getString("ADDRESS");
				            Object contactnum = rs2.getString("CONTACTNO"); 
				             
				            Viewtable.getModel().setValueAt(firstname, i, 0);
				            Viewtable.getModel().setValueAt(lastname, i, 1);
				           Viewtable.getModel().setValueAt(address, i, 2);
				          Viewtable.getModel().setValueAt(contactnum, i, 3); 
				          
				         
				        i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
			}
		});
		
		ViewReturnButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				 ViewSearch.setVisible(true);
				 ViewResetButton.setVisible(true);
				 Viewfirstnamefield.setVisible(true);
				 Viewlastnamefield.setVisible(true);
				 Viewfistnamelabel.setVisible(true);
				 Viewlastnamelabel.setVisible(true);
				 ViewtableScrollpane.setVisible(true);
				 Viewsearchcustomerlabel.setVisible(true);
				 Viewlistofcustomers.setVisible(true);
				 user.setEnabled(true);
				 ADDDEBT.setEnabled(true);
				 VIEW.setEnabled(true);	
				 BILLOUT.setEnabled(true);	
				 ABOUTUS.setEnabled(true);
				 
				 
				 ViewSelectedpersonalinfo.setVisible(false);
				 ViewSelectedfirstname.setVisible(false);
				 ViewSelectedlastname.setVisible(false);
				 ViewSelectedaddress.setVisible(false);
				 ViewSelectedcontactno.setVisible(false);
				 ViewSelectedfirstnametext.setVisible(false);
				 ViewSelectedlastnametext.setVisible(false);
				 ViewSelectedaddresstext.setVisible(false);
				 ViewSelectedcontacttext.setVisible(false);
				 ViewDebtinformationscrollpane.setVisible(false);
				 ViewPaymentHistoryScrollpane.setVisible(false);
				 ViewSelectedBalance.setVisible(false);
				 ViewSelectedbalanceText.setVisible(false);
				 Vieweditinformation.setVisible(false);
				 ViewReturnButton.setVisible(false);
				 ViewDebtInformation.setVisible(false);
				 ViewPaymentHistory.setVisible(false);
			}
		});
		
		Vieweditinformation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				 ViewEditfirstnametext.setText(ViewSelectedfirstnametext.getText());
		         ViewEditlastnametext.setText(ViewSelectedlastnametext.getText());
		         ViewEditaddresstext.setText(ViewSelectedaddresstext.getText());
		         ViewEditcontactnumbertext.setText(ViewSelectedcontacttext.getText());
				 EditFrame.show();
				 frame.setEnabled(false);
			}
		});
		
		ViewEditbtnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				String tempcontactnumbe = "",temperrornum ="",tempcustomer = "",tempcustomererror = "";
				String selectname = "",editname = "";
				int error = 0;

				if( ViewEditfirstnametext.getText().length() == 0 &&  ViewEditlastnametext.getText().length() == 0 && ViewEditaddresstext.getText().length() == 0 && ViewEditcontactnumbertext.getText().length() == 0)
				{
					error = 0;
				}
				else if( ViewEditfirstnametext.getText().length() != 0 &&  ViewEditlastnametext.getText().length() != 0 && ViewEditaddresstext.getText().length() != 0 && ViewEditcontactnumbertext.getText().length() != 0)
				{
					error = 1;
				}
				
				editname = ViewEditfirstnametext.getText() + ViewEditlastnametext.getText();
				selectname = ViewSelectedfirstnametext.getText() + ViewSelectedlastnametext.getText();
				
				if(editname.equals(selectname) )
				{
					tempcustomererror = "";
				}
				else
				{
					try
					{
						String query = "SELECT firstname,lastname FROM CUSTOMER where firstname = '" + ViewEditfirstnametext.getText().toUpperCase() + "' AND lastname = '" + ViewEditlastnametext.getText().toUpperCase() + "'" ;
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						
						if(!rs2.next())
						{
							
				            tempcustomer = "";
				        }
						else
						{
							tempcustomererror = "error";
						}
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
				}
				

				if(error == 0)
				{
					JOptionPane.showMessageDialog(null, "PLEASE COMPLETE THE INFORMATION BELOW!");
				}
				else if(tempcustomererror == "error")
				{
					JOptionPane.showMessageDialog(null, "NAME ALREADY EXIST");
				}
				else
				{
					try
					{
					
						
						String query = "SELECT customerid FROM CUSTOMER where firstname = '" 
										+ c.getFirstname() 
										+"' AND LASTNAME ='" + c.getLastname() 
										+"'";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) {
				           
				         
				            c.setCostumercode(rs2.getString("CUSTOMERID"));
				            
				           
				        i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					
					try
					{
					
						
						String query = "UPDATE CUSTOMER SET FIRSTNAME ='" + ViewEditfirstnametext.getText().toUpperCase() + "',LASTNAME ='"
										+ ViewEditlastnametext.getText().toUpperCase() +"',ADDRESS ='" +  ViewEditaddresstext.getText().toUpperCase()
										+ "',CONTACTNO ='" + ViewEditcontactnumbertext.getText().toUpperCase() + "' WHERE "
									    + "CUSTOMERID = '" + c.getCostumercode() +"'" ;
						PreparedStatement pst2 = connection.prepareStatement(query);
						pst2.executeUpdate();
						
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					try
					{
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						
						if(rowcount != 0)
						{
							rowcount = 0;
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						else
						{
							while(rs2.next())
							{
							    rowcount ++;
							}
						}
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					
					Viewtable.setModel(new DefaultTableModel(
							new Object[rowcount][rowcount] ,
							new String[] {
								"FIRSTNAME", "LASTNAME", "ADDRESS", "CONTACTNO."
							}
						) {
							Class[] columnTypes = new Class[] {
								Object.class, Object.class, String.class, Object.class
							};
							public Class getColumnClass(int columnIndex) {
								return columnTypes[columnIndex];
							}
							boolean[] columnEditables = new boolean[] {
								false, false, false
							};
							public boolean isCellEditable(int row, int column) {
								return columnEditables[column];
							}
						});
					Viewtable.getColumnModel().getColumn(0).setResizable(false);
					Viewtable.getColumnModel().getColumn(0).setPreferredWidth(200);
					Viewtable.getColumnModel().getColumn(1).setResizable(false);
					Viewtable.getColumnModel().getColumn(2).setResizable(false);
					Viewtable.getColumnModel().getColumn(2).setPreferredWidth(300);
					Viewtable.getColumnModel().getColumn(3).setResizable(false);
					
				
					try
					{
					
						
						String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) {
				           
				         
				            Object firstname = rs2.getString("FIRSTNAME");
				            Object lastname = rs2.getString("LASTNAME");
				            Object address = rs2.getString("ADDRESS");
				            Object contactnum = rs2.getString("CONTACTNO"); 
				             
				            Viewtable.getModel().setValueAt(firstname, i, 0);
				            Viewtable.getModel().setValueAt(lastname, i, 1);
				           Viewtable.getModel().setValueAt(address, i, 2);
				          Viewtable.getModel().setValueAt(contactnum, i, 3); 
				          
  			         
				        i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
						
						try
						{
						
							
							String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER where customerid = '"
									   +  c.getCostumercode() + "'" ;
							PreparedStatement pst2 = connection.prepareStatement(query);
							ResultSet rs2 = pst2.executeQuery();
							int i=0;
							while(rs2.next()) {
					           
					         
					            Object firstname = rs2.getString("FIRSTNAME");
					            Object lastname = rs2.getString("LASTNAME");
					            Object address = rs2.getString("ADDRESS");
					            Object contactnum = rs2.getString("CONTACTNO");    
					          
					          ViewSelectedfirstnametext.setText(firstname.toString());
					          ViewSelectedlastnametext.setText(lastname.toString());
					          ViewSelectedaddresstext.setText(address.toString());
					          ViewSelectedcontacttext.setText(contactnum.toString());
					          
					          
					         
					        i++;
					        }
							
						}catch(SQLException e1)
						{
							e1.printStackTrace();
						}
					JOptionPane.showMessageDialog(null, "INFORMATION SUCCESFULLY UPDATED!");
					
					EditFrame.hide();
					frame.setEnabled(true);
					frame.setLocationRelativeTo(null);
					frame.show();
				}
				
			}
		});
		
		ViewEditbtnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				EditFrame.hide();
				frame.setEnabled(true);
				frame.setLocationRelativeTo(null);
				frame.show();
			}
		});
		
		
		addebitcancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				getaddebitcancelAction();
			}
		});
		
		
		ADDDEBITREFRESH.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				try
				{
					String query = "SELECT DEBT_AMOUNT,DATE_ADDED,TIME_ADDED FROM DEBT WHERE CUSTOMERID = "
									+ "(SELECT CUSTOMERID FROM CUSTOMER WHERE FIRSTNAME ='" + addebitfirstnametext.getText() 
									+"' AND LASTNAME ='" + adddebitlastnametext.getText() +"')";
					PreparedStatement pst2 = connection.prepareStatement(query);
					ResultSet rs2 = pst2.executeQuery();
					if(rowcount != 0)
					{
						rowcount = 0;
						while(rs2.next())
						{
						    rowcount ++;
						}
					}
					else
					{
						while(rs2.next())
						{
						    rowcount ++;
						}
					}
					
					
				}catch(SQLException e1)
				{
					e1.printStackTrace();
				}
				
				table_1.setModel(new DefaultTableModel(
						new Object[rowcount][rowcount] ,
						new String[] {
							"DEBTAMOUNT", "DATE ADDED", "TIME ADDED"
						}
					) {
						Class[] columnTypes = new Class[] {
							Object.class, Object.class, String.class, Object.class
						};
						public Class getColumnClass(int columnIndex) {
							return columnTypes[columnIndex];
						}
						boolean[] columnEditables = new boolean[] {
							false, false, false
						};
						public boolean isCellEditable(int row, int column) {
							return columnEditables[column];
						}
					});
				table_1.getColumnModel().getColumn(0).setResizable(false);
				table_1.getColumnModel().getColumn(0).setPreferredWidth(200);
				table_1.getColumnModel().getColumn(1).setResizable(false);
				table_1.getColumnModel().getColumn(2).setResizable(false);
				
				try
				{
					String query1 = "SELECT DEBT_AMOUNT,DATE_ADDED,TIME_ADDED FROM DEBT WHERE CUSTOMERID = "
									+ "(SELECT CUSTOMERID FROM CUSTOMER WHERE FIRSTNAME ='" + addebitfirstnametext.getText() 
									+"' AND LASTNAME ='" + adddebitlastnametext.getText() +"') ORDER BY DATE_ADDED,TIME_ADDED" ;
			
			
					PreparedStatement pst4 = connection.prepareStatement(query1);
					ResultSet rs4 = pst4.executeQuery();
					int i=0;
					while(rs4.next()) 
					{
				           
				         
						Object debtamount = rs4.getString("DEBT_AMOUNT");
			            Date date = rs4.getDate("DATE_ADDED");
			            Time time = rs4.getTime("TIME_ADDED");
			            Object datetemp,timetemp;
			            
			            datetemp = dateFormat.format(date);
			            timetemp = timeFormat.format(time);
			            
			            table_1.getModel().setValueAt(debtamount, i, 0);
			            table_1.getModel().setValueAt(datetemp, i, 1);
			           table_1.getModel().setValueAt(timetemp, i, 2);
			           
			        i++;
					}
				}catch(SQLException e1)
				{
					e1.printStackTrace();
				}
			}
		});
		
		YESBUTTONFORADDINGDEBT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				adddebtframe.show();
				frame.setEnabled(false);
			}
		});
		

		btnSubmitDebt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				String tempamount,tempamounterror="",tempzerodebt="";
				String Amounterror = "";
				Double balance1 = 0.00;
				
				 try {
			         	
		             Double.parseDouble(adddebttextfield.getText());
		          
		             
		          } catch ( NumberFormatException exc ) {
		              
		              Amounterror = "error";
		          }
				 
				 
				 
				 try
					{
					
						
						String query = "SELECT customerid FROM CUSTOMER where firstname = '" 
										+ addebitfirstnametext.getText() 
										+"' AND LASTNAME ='" + adddebitlastnametext.getText() 
										+"'";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) {
				           
				         
				            c.setCostumercode(rs2.getString("CUSTOMERID"));
				           
				        i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					try
					{
					
						
						String query = "SELECT BALANCEID FROM BALANCE_INVOICE where CUSTOMERID ="
										+ "'" + c.getCostumercode() +"'";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) {
				           
				         
				           c.setBalanceid(rs2.getString("BALANCEID"));
				           
				        i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					
					try
					{
					
						
						String query = "SELECT BALANCE FROM BALANCE where BALANCEID = '"
										+  c.getBalanceid() +"' AND DATE_UPDATED ="
										+ "(SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE "
										+ "BALANCEID = '"+ c.getBalanceid() + "') AND "
										+ "TIME_UPDATED = (SELECT MAX(TIME_UPDATED) FROM"
										+ " BALANCE WHERE BALANCEID ='" + c.getBalanceid() + "' AND DATE_UPDATED"
										+ "= (SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE BALANCEID"
										+ "='"+ c.getBalanceid() + "'))";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) {
				           
				         
				           balance1 = rs2.getDouble("BALANCE");
				        i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}




				if(adddebttextfield.getText() == "0")
				{
					tempzerodebt = "error";
				}
				else
				{
					tempzerodebt = "";
				}
				
				
				if(tempzerodebt == "error")
				{
					JOptionPane.showMessageDialog(null,"YOU NEED TO INPUT PAYMENT AMOUNT!, 0 PAYMENT AMOUNT IS NOT ACCEPTED!");
				}
				else if(Amounterror.equals("error"))
				{
					JOptionPane.showMessageDialog(null,"UNDEFINED AMOUNT!");
				}
				else if(Double.parseDouble(adddebttextfield.getText()) >= 5000)
				{
					JOptionPane.showMessageDialog(null,"YOU HAVE REACHED THE MAXIMUM DEBT! YOU HAVE TO BORROW LOWER THAN THAT!");
				}
				else if(balance1 >= 5000)
				{
					JOptionPane.showMessageDialog(null,"YOU HAVE REACHED THE MAXIMUM BALANCE! YOU HAVE TO PAY FIRST!");
				}
				else
				{
					
					c.setDebtbalance(Double.parseDouble(adddebttextfield.getText()));
					double updateblance = 0,balance = 0;
					try
					{
					
						
						String query = "SELECT customerid FROM CUSTOMER where firstname = '" 
										+ addebitfirstnametext.getText() 
										+"' AND LASTNAME ='" + adddebitlastnametext.getText() 
										+"'";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) {
				           
				         
				            c.setCostumercode(rs2.getString("CUSTOMERID"));
				           
				        i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					try
					{
					
						
						String query = "SELECT BALANCEID FROM BALANCE_INVOICE where CUSTOMERID ="
										+ "'" + c.getCostumercode() +"'";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) {
				           
				         
				           c.setBalanceid(rs2.getString("BALANCEID"));
				           
				        i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					
					try
					{
					
						
						String query = "INSERT INTO DEBT VALUES('" + c.getCostumercode() 
										+"'," + c.getDebtbalance() + ",CURRENT TIMESTAMP"
												+ ",CURRENT TIMESTAMP)";
										
						PreparedStatement pst2 = connection.prepareStatement(query);
						pst2.executeUpdate();
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					try
					{
					
						
						String query = "SELECT BALANCE FROM BALANCE where BALANCEID = '"
										+  c.getBalanceid() +"' AND DATE_UPDATED ="
										+ "(SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE "
										+ "BALANCEID = '"+ c.getBalanceid() + "') AND "
										+ "TIME_UPDATED = (SELECT MAX(TIME_UPDATED) FROM"
										+ " BALANCE WHERE BALANCEID ='" + c.getBalanceid() + "' AND DATE_UPDATED"
										+ "= (SELECT MAX(DATE_UPDATED) FROM BALANCE WHERE BALANCEID"
										+ "='"+ c.getBalanceid() + "'))";
						PreparedStatement pst2 = connection.prepareStatement(query);
						ResultSet rs2 = pst2.executeQuery();
						int i=0;
						while(rs2.next()) {
				           
				         
				           balance = rs2.getDouble("BALANCE");
				        i++;
				        }
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					
					updateblance = c.getDebtbalance() + balance;
					
					try
					{
					
						
						String query = "INSERT INTO BALANCE VALUES('" + c.getBalanceid() 
										+"'," + updateblance + ",CURRENT TIMESTAMP"
												+ ",CURRENT TIMESTAMP)";
										
						PreparedStatement pst2 = connection.prepareStatement(query);
						pst2.executeUpdate();
						
						
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					JOptionPane.showMessageDialog(null, "DEBT AMOUNT HAVE BEEN SUCCESFULLY ADDED TO THE CUSTOMER'S BALANCE!");
					adddebttextfield.setText("");
					adddebtframe.hide();
					frame.setEnabled(true);
					frame.setLocationRelativeTo(null);
					frame.show();
				}
			}
		});
		
		ViewSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				getViewSearchActionButton();
			}
		});
		

		btnCancelDebt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				adddebtframe.hide();
				adddebttextfield.setText("");
				frame.setEnabled(true);
				frame.setLocationRelativeTo(null);
				frame.show();
			}
		});
		
		btnLogIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				if(textlog.getText().equals("admin") && passwordField.getText().equals("admin123")){
					
					
					
					try
					{
						String query1 = "SELECT * FROM LOGOUT WHERE TIME_OUT = (SELECT MAX(TIME_OUT) FROM LOGOUT WHERE DATE_OUT = (SELECT MAX(DATE_OUT) FROM LOGOUT))" ;
				
				
						PreparedStatement pst4 = connection.prepareStatement(query1);
						ResultSet rs4 = pst4.executeQuery();
						int i=0;
						while(rs4.next()) 
						{
					           
					        
				            Date date = rs4.getDate("DATE_OUT");
				            Time time = rs4.getTime("TIME_OUT");
				            Object datetemp,timetemp;
				            
				            datetemp = dateFormat.format(date);
				            timetemp = timeFormat.format(time);
				            lasttimeout.setText("LAST TIME OUT:" + datetemp + " " + timetemp);
				            
				            
				         
				        i++;
						}
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					try
					{
						String query1 = "SELECT * FROM LOGIN WHERE TIME_IN = (SELECT MAX(TIME_IN) FROM LOGIN WHERE DATE_IN = (SELECT MAX(DATE_IN) FROM LOGIN))" ;
				
				
						PreparedStatement pst4 = connection.prepareStatement(query1);
						ResultSet rs4 = pst4.executeQuery();
						int i=0;
						while(rs4.next()) 
						{
					           
					        
				            Date date = rs4.getDate("DATE_IN");
				            Time time = rs4.getTime("TIME_IN");
				            Object datetemp,timetemp;
				            
				            datetemp = dateFormat.format(date);
				            timetemp = timeFormat.format(time);
				            timein.setText("LAST TIME IN:" + datetemp  + " " + timetemp);
				   
				            
				         
				        i++;
						}
					}catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					try {
						String query = "INSERT INTO LOGIN VALUES(CURRENT TIMESTAMP,CURRENT TIMESTAMP)";
																	  
						PreparedStatement pst3 = connection.prepareStatement(query);
						pst3.executeUpdate();
					} catch (SQLException e1) {
						
						e1.printStackTrace();
					}
					frame.setVisible(true);
					logInFrame.setVisible(false);
					
				}
					
					else{
						Component controllingFrame = null;
						JOptionPane.showMessageDialog(controllingFrame, 
								"Invalid Password or Username",
								"Error Message",
								JOptionPane.ERROR_MESSAGE);
						textlog.setText(null);
						passwordField.setText(null);
					}
			}
		});
		
		btncancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				System.exit(0);

			}
		});
		
		signout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				
				 try {
						String query = "INSERT INTO LOGOUT VALUES(CURRENT TIMESTAMP,CURRENT TIMESTAMP)";
																	  
						PreparedStatement pst3 = connection.prepareStatement(query);
						pst3.executeUpdate();
					} catch (SQLException e1) {
						
						e1.printStackTrace();
					}
				logInFrame.setVisible(true);
				frame.setVisible(false);
				textlog.setText("");
				passwordField.setText("");
				
				
			}
		});
	}

	public void runGUI()
	{
		logInFrame.setVisible(true);
	}
	
	public void getNewCostumerButtonListener()
	{
		btnNewCostumer.setVisible(false);
		existingbutton.setVisible(false);
		adddebitpanel.getRootPane().setDefaultButton(btnSubmit);
		user.setEnabled(false);
		ADDDEBT.setEnabled(false);
		VIEW.setEnabled(false);
		BILLOUT.setEnabled(false);
		ABOUTUS.setEnabled(false);
		
		
		personalinformation.setVisible(true);
		firstname.setVisible(true);
		lastname.setVisible(true);
		contactnumber.setVisible(true);
		address.setVisible(true);
		amount.setVisible(true);
		
		
		firstnametext.setVisible(true);
		lastnametext.setVisible(true);
		contactnumbertext.setVisible(true);
		addresstext.setVisible(true);
		Amount.setVisible(true);
		
		btnSubmit.setVisible(true);
		btnCancel.setVisible(true);
		personalinfopic.setVisible(true);
	}
	
	public void getNewCostumerCancelButton()
	{
		
		firstnametext.setText("");
		lastnametext.setText("");
		addresstext.setText("");
		contactnumbertext.setText("");
		Amount.setText("");
		adddebitpanel.getRootPane().setDefaultButton(null);
		btnNewCostumer.setVisible(true);
		existingbutton.setVisible(true);
		
		user.setEnabled(true);
		ADDDEBT.setEnabled(true);
		VIEW.setEnabled(true);
		BILLOUT.setEnabled(true);
		ABOUTUS.setEnabled(true);
	
		
		personalinformation.setVisible(false);
		firstname.setVisible(false);
		lastname.setVisible(false);
		contactnumber.setVisible(false);
		address.setVisible(false);
		amount.setVisible(false);
		
		
		firstnametext.setVisible(false);
		lastnametext.setVisible(false);
		contactnumbertext.setVisible(false);
		addresstext.setVisible(false);
		Amount.setVisible(false);
		
		btnSubmit.setVisible(false);
		btnCancel.setVisible(false);
		personalinfopic.setVisible(false);
	}
	
	public void getButtonSubmitforNewCostumer()
	{
		String tempamount = "",tempamounterror = "";
		String tempcontactnumbe = "",temperrornum = "";
		String tempcustomer="",tempcustomererror="";
		String customercodeget = "", Amounterror = "";
		String customerdebtcodeget = "";
		
		int error = 0;
		
		if(firstnametext.getText().length() == 0 && lastnametext.getText().length() == 0 && contactnumbertext.getText().length() == 0 && addresstext.getText().length() == 0 && Amount.getText().length() == 0 )
		{
			 error = 0;
		}
		else if(firstnametext.getText().length() != 0 && lastnametext.getText().length() != 0 && contactnumbertext.getText().length() != 0 && addresstext.getText().length() != 0 && Amount.getText().length() != 0)
		{
			error = 1;
		}
		
		int customeridtemp;
		String customeridgetter;
		do
		{
		customeridtemp = rndm.nextInt(999);
		if(Integer.toString(customeridtemp).length() == 1)
		{
			customeridgetter = "00" + Integer.toString(customeridtemp);
		}
		else if(Integer.toString(customeridtemp).length() == 2)
		{
			customeridgetter = "0" + Integer.toString(customeridtemp);
		}
		else
		{
			customeridgetter = Integer.toString(customeridtemp);
		}
		
		try
		{
			String query = "SELECT customerid FROM CUSTOMER where customerid = '" + customeridgetter +  "'";
			PreparedStatement pst1 = connection.prepareStatement(query);
			ResultSet rs1 = pst1.executeQuery();
			
			if(!rs1.next())
			{
				
	            customercodeget = "";
	        }
			else
			{
				customercodeget = "error";
			}
		}catch(SQLException e1)
		{
			e1.printStackTrace();
		}
		}while(customercodeget == "error");
		
		int balanceid;
		String balanceidgetter;
		do
		{
		balanceid = rndm.nextInt(999);
		if(Integer.toString(balanceid).length() == 1)
		{
			balanceidgetter = "00" + Integer.toString(balanceid);
		}
		else if(Integer.toString(balanceid).length() == 2)
		{
			balanceidgetter = "0" + Integer.toString(balanceid);
		}
		else
		{
			balanceidgetter = Integer.toString(balanceid);
		}
		
		try
		{
			String query = "SELECT balanceid FROM balance_invoice where balanceid = '" + balanceidgetter +  "'";
			PreparedStatement pst1 = connection.prepareStatement(query);
			ResultSet rs1 = pst1.executeQuery();
			
			if(!rs1.next())
			{
				
	            customerdebtcodeget = "";
	        }
			else
			{
				customerdebtcodeget = "error";
			}
		}catch(SQLException e1)
		{
			e1.printStackTrace();
		}
		}while(customerdebtcodeget == "error");
		
		try
		{
		
			
			String query = "SELECT firstname,lastname FROM CUSTOMER where firstname = '" + firstnametext.getText().toUpperCase() + "' AND lastname = '" + lastnametext.getText().toUpperCase() + "'" ;
			PreparedStatement pst2 = connection.prepareStatement(query);
			ResultSet rs2 = pst2.executeQuery();
			
			if(!rs2.next())
			{
				
	            tempcustomer = "";
	        }
			else
			{
				tempcustomererror = "error";
			}
		}catch(SQLException e1)
		{
			e1.printStackTrace();
		}
		
		 try {
         	
             Double.parseDouble(Amount.getText());
          
             
          } catch ( NumberFormatException exc ) {
              
              Amounterror = "error";
          }
		
		
		

		if(error == 0)
		{
			JOptionPane.showMessageDialog(null, "PLEASE COMPLETE THE INFORMATION BELOW!");
		}
		else if(tempcustomererror == "error")
		{
			JOptionPane.showMessageDialog(null, "NAME ALREADY EXIST");
		}
		else if(Amounterror.equals("error"))
		{
			JOptionPane.showMessageDialog(null, "UNDEFINED AMOUNT!");
		}
		else if( Double.parseDouble(Amount.getText()) > 2000)
		{
			JOptionPane.showMessageDialog(null, "FIRST GIVE DEBT MUST BE LESSER OR EQUAL THAN 2000!");
		}
		else
		{
			c.setCostumercode(customeridgetter.toUpperCase());
			c.setFirstname(firstnametext.getText().toUpperCase());
			c.setLastname(lastnametext.getText().toUpperCase());
			c.setAddress(addresstext.getText().toUpperCase());
			c.setContactnumber(contactnumbertext.getText().toUpperCase());
			c.setDebtbalance(Double.parseDouble(Amount.getText()));
			c.setBalanceid(balanceidgetter.toUpperCase());
			c.setBalanceNewCustomer(Double.parseDouble(Amount.getText()));
			
			try {
				String query = "INSERT INTO CUSTOMER VALUES('"+ c.getCostumercode() + "','" + c.getFirstname() + "','" 
															  + c.getLastname() + "','" + c.getAddress() + "','" 
															  + c.getContactnumber() +"')"  ;
															  
				PreparedStatement pst3 = connection.prepareStatement(query);
				pst3.executeUpdate();
			} catch (SQLException e1) {
				
				e1.printStackTrace();
			}
			try {
				String query = "INSERT INTO DEBT VALUES('"+ c.getCostumercode() + "'," + c.getDebtbalance() + ",CURRENT TIMESTAMP,CURRENT TIMESTAMP)";
															  
				PreparedStatement pst3 = connection.prepareStatement(query);
				pst3.executeUpdate();
			} catch (SQLException e1) {
				
				e1.printStackTrace();
			}
			try {
				String query = "INSERT INTO BALANCE_INVOICE VALUES('"+ c.getCostumercode() + "','" + c.getBalanceid() + "')";
															  
				PreparedStatement pst3 = connection.prepareStatement(query);
				pst3.executeUpdate();
			} catch (SQLException e1) {
				
				e1.printStackTrace();
			}
			try {
				String query = "INSERT INTO BALANCE VALUES('"+ c.getBalanceid() + "'," + c.getBalanceNewCustomer() + ",CURRENT TIMESTAMP,CURRENT TIMESTAMP)"  ;
															  
				PreparedStatement pst3 = connection.prepareStatement(query);
				pst3.executeUpdate();
			} catch (SQLException e1) {
				
				e1.printStackTrace();
			}
			
			
			JOptionPane.showMessageDialog(null, "CUSTOMER ACCOUNT SUCCESFULLY CREATED!");
			
			
			firstnametext.setText("");
			lastnametext.setText("");
			addresstext.setText("");
			contactnumbertext.setText("");
			Amount.setText("");
			
			btnNewCostumer.setVisible(true);
			existingbutton.setVisible(true);
			user.setEnabled(true);
			ADDDEBT.setEnabled(true);
			VIEW.setEnabled(true);
			BILLOUT.setEnabled(true);
			ABOUTUS.setEnabled(true);
			personalinformation.setVisible(false);
			firstname.setVisible(false);
			lastname.setVisible(false);
			contactnumber.setVisible(false);
			address.setVisible(false);
			amount.setVisible(false);
			
			
			firstnametext.setVisible(false);
			lastnametext.setVisible(false);
			contactnumbertext.setVisible(false);
			addresstext.setVisible(false);
			Amount.setVisible(false);
				
			btnSubmit.setVisible(false);
			btnCancel.setVisible(false);
			personalinfopic.setVisible(false);

			
		}
	 }
	
	public void getSearchButtonExistingButtonFunction()
	{
		search.setFirstname(firstnamesearchexistingbutton.getText().toUpperCase());
		search.setLastname(lastnamesearchexistingbutton.getText().toUpperCase());
		String con1 = "false", con2 = "false", con3 = "false";
		
		if((search.getFirstname().length() != 0) && (search.getLastname().length() != 0))
		{
			con1 = "true";
		}
		else if((search.getFirstname().length() != 0) && (search.getLastname().length() == 0))
		{
			con2 = "true";
		}
		else if((search.getFirstname().length() == 0) && (search.getLastname().length() != 0))
		{
			con3 = "true";
		}
		
		
		
		if(con1 == "true" )
		{
			try
			{
				String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
						+ search.getFirstname() + "' AND lastname = '" 
						+ search.getLastname()  + "'" ;
				PreparedStatement pst2 = connection.prepareStatement(query);
				ResultSet rs2 = pst2.executeQuery();
				if(rowcount != 0)
				{
					rowcount = 0;
					while(rs2.next())
					{
					    rowcount ++;
					}
				}
				else
				{
					while(rs2.next())
					{
					    rowcount ++;
					}
				}
				
				
			}catch(SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		else if( con2 == "true")
		{
			try
			{
				String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
						+ search.getFirstname() + "'";
						 
				PreparedStatement pst2 = connection.prepareStatement(query);
				ResultSet rs2 = pst2.executeQuery();
				if(rowcount != 0)
				{
					rowcount = 0;
					while(rs2.next())
					{
					    rowcount ++;
					}
				}
				else
				{
					while(rs2.next())
					{
					    rowcount ++;
					}
				}
				
				
			}catch(SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		
		else if(con3 == "true")
		{
			try
			{
				String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE lastname = '" 
						+ search.getLastname() + "'";
				PreparedStatement pst2 = connection.prepareStatement(query);
				ResultSet rs2 = pst2.executeQuery();
				if(rowcount != 0)
				{
					rowcount = 0;
					while(rs2.next())
					{
					    rowcount ++;
					}
				}
				else
				{
					while(rs2.next())
					{
					    rowcount ++;
					}
				}
				
				
			}catch(SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		else
		{
			try
			{
				String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
				PreparedStatement pst2 = connection.prepareStatement(query);
				ResultSet rs2 = pst2.executeQuery();
				
				if(rowcount != 0)
				{
					rowcount = 0;
					while(rs2.next())
					{
					    rowcount ++;
					}
				}
				else
				{
					while(rs2.next())
					{
					    rowcount ++;
					}
				}
				
				
			}catch(SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		
		
		
		
		
		table.setModel(new DefaultTableModel(
				new Object[rowcount][rowcount] ,
				new String[] {
					"FIRSTNAME", "LASTNAME", "ADDRESS", "CONTACTNO."
				}
			) {
				Class[] columnTypes = new Class[] {
					Object.class, Object.class, String.class, Object.class
				};
				public Class getColumnClass(int columnIndex) {
					return columnTypes[columnIndex];
				}
				boolean[] columnEditables = new boolean[] {
					false, false, false
				};
				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
			});
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setPreferredWidth(200);
		table.getColumnModel().getColumn(1).setResizable(false);
		table.getColumnModel().getColumn(2).setResizable(false);
		table.getColumnModel().getColumn(2).setPreferredWidth(300);
		table.getColumnModel().getColumn(3).setResizable(false);
		
		try
		{
		
			if(con1 == "true")
			{
				String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
						+ search.getFirstname() + "' AND lastname = '" 
						+ search.getLastname()  + "'" ;
		
		
				PreparedStatement pst3 = connection.prepareStatement(query);
				ResultSet rs3 = pst3.executeQuery();
		
				if(!rs3.next())
				{
					JOptionPane.showMessageDialog(null, "NO COSTUMER FOUND!");
				}
				else
				{
					String query1 = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
									+ search.getFirstname() + "' AND lastname = '" 
									+ search.getLastname()  + "'"  ;
	
	
					PreparedStatement pst4 = connection.prepareStatement(query1);
					ResultSet rs4 = pst4.executeQuery();
					int i=0;
					while(rs4.next()) 
					{
				           
				         
			            Object firstname = rs3.getString("FIRSTNAME");
			            Object lastname = rs3.getString("LASTNAME");
			            Object address = rs3.getString("ADDRESS");
			            Object cantctnum = rs3.getString("CONTACTNO"); 
			             
			            table.getModel().setValueAt(firstname, i, 0);
			            table.getModel().setValueAt(lastname, i, 1);
			           table.getModel().setValueAt(address, i, 2);
			          table.getModel().setValueAt(cantctnum, i, 3); 
			          
			         
			        i++;
					}

				}
			}	
			else if(con2 == "true")
			{
				String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
						+ search.getFirstname() + "'";
						 
				PreparedStatement pst3 = connection.prepareStatement(query);
				ResultSet rs3 = pst3.executeQuery();
				
				if(!rs3.next())
				{
					JOptionPane.showMessageDialog(null, "NO COSTUMER FOUND!");
				}
				else
				{
					String query1 = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
									+ search.getFirstname() + "'"  ;
	
	
					PreparedStatement pst4 = connection.prepareStatement(query1);
					ResultSet rs4 = pst4.executeQuery();
					int i=0;
					while(rs4.next()) 
					{
				           
				         
			            Object firstname = rs4.getString("FIRSTNAME");
			            Object lastname = rs4.getString("LASTNAME");
			            Object address = rs4.getString("ADDRESS");
			            Object cantctnum = rs4.getString("CONTACTNO"); 
			        
			            table.getModel().setValueAt(firstname, i, 0);
			            table.getModel().setValueAt(lastname, i, 1);
			           table.getModel().setValueAt(address, i, 2);
			          table.getModel().setValueAt(cantctnum, i, 3); 
			          
			         
			        i++;
					}
				}
			}	
			else if(con3 == "true")
			{
				String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE lastname = '" 
						+ search.getLastname() + "'";
				PreparedStatement pst3 = connection.prepareStatement(query);
				ResultSet rs3 = pst3.executeQuery();
				
				if(!rs3.next())
				{
					JOptionPane.showMessageDialog(null, "NO COSTUMER FOUND!");
				}
				else
				{
					String query1 = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE lastname = '" 
									+ search.getLastname() + "'"  ;
	
	
					PreparedStatement pst4 = connection.prepareStatement(query1);
					ResultSet rs4 = pst4.executeQuery();
					int i=0;
					while(rs4.next()) 
					{
				           
				         
			            Object firstname = rs4.getString("FIRSTNAME");
			            Object lastname = rs4.getString("LASTNAME");
			            Object address = rs4.getString("ADDRESS");
			            Object cantctnum = rs4.getString("CONTACTNO"); 
			             
			            table.getModel().setValueAt(firstname, i, 0);
			            table.getModel().setValueAt(lastname, i, 1);
			           table.getModel().setValueAt(address, i, 2);
			          table.getModel().setValueAt(cantctnum, i, 3); 
			          
			         
			        i++;
					}
				}
			}
			else
			{
				try
				{
				
					
					String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
					PreparedStatement pst2 = connection.prepareStatement(query);
					ResultSet rs2 = pst2.executeQuery();
					int i=0;
					while(rs2.next()) {
			           
			         
			            Object firstname = rs2.getString("FIRSTNAME");
			            Object lastname = rs2.getString("LASTNAME");
			            Object address = rs2.getString("ADDRESS");
			            Object contactnum = rs2.getString("CONTACTNO"); 
			             
			            table.getModel().setValueAt(firstname, i, 0);
			            table.getModel().setValueAt(lastname, i, 1);
			           table.getModel().setValueAt(address, i, 2);
			          table.getModel().setValueAt(contactnum, i, 3); 
			          
			         
			        i++;
			        }
					
				}catch(SQLException e1)
				{
					e1.printStackTrace();
				}
			}
							
				
			
		}catch(SQLException e1)
		{
			e1.printStackTrace();
		}
	}
	
	public void getexistingbuttonFunction()
	{
		btnNewCostumer.setVisible(false);
		existingbutton.setVisible(false);
		user.setEnabled(false);
		ADDDEBT.setEnabled(false);
		VIEW.setEnabled(false);	
		BILLOUT.setEnabled(false);	
		ABOUTUS.setEnabled(false);
		adddebitpanel.getRootPane().setDefaultButton(Search);
		
		scrollPane.setVisible(true);
		Search.setVisible(true);
		DONE.setVisible(true);
		RESET.setVisible(true);
		CANCEL.setVisible(true);
		LISTOFCUSTOMERS.setVisible(true);
		firstnamesearchexistingbutton.setVisible(true);
		lastnamesearchexistingbutton.setVisible(true);
		firstnameexistingbutton.setVisible(true);
		lastnamexistingbutton.setVisible(true);
		saerchcustomerexisting.setVisible(true);
			
		
		try
		{
			String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
			PreparedStatement pst2 = connection.prepareStatement(query);
			ResultSet rs2 = pst2.executeQuery();
			
			if(rowcount != 0)
			{
				rowcount = 0;
				while(rs2.next())
				{
				    rowcount ++;
				}
			}
			else
			{
				while(rs2.next())
				{
				    rowcount ++;
				}
			}
			
			
		}catch(SQLException e1)
		{
			e1.printStackTrace();
		}
		
		table.setModel(new DefaultTableModel(
				new Object[rowcount][rowcount] ,
				new String[] {
					"FIRSTNAME", "LASTNAME", "ADDRESS", "CONTACTNO."
				}
			) {
				Class[] columnTypes = new Class[] {
					Object.class, Object.class, String.class, Object.class
				};
				public Class getColumnClass(int columnIndex) {
					return columnTypes[columnIndex];
				}
				boolean[] columnEditables = new boolean[] {
					false, false, false
				};
				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
			});
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setPreferredWidth(200);
		table.getColumnModel().getColumn(1).setResizable(false);
		table.getColumnModel().getColumn(2).setResizable(false);
		table.getColumnModel().getColumn(2).setPreferredWidth(300);
		table.getColumnModel().getColumn(3).setResizable(false);
		
	
		
			
			try
			{
			
				
				String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
				PreparedStatement pst2 = connection.prepareStatement(query);
				ResultSet rs2 = pst2.executeQuery();
				int i=0;
				while(rs2.next()) {
		           
		         
		            Object firstname = rs2.getString("FIRSTNAME");
		            Object lastname = rs2.getString("LASTNAME");
		            Object address = rs2.getString("ADDRESS");
		            Object contactnum = rs2.getString("CONTACTNO"); 
		             
		            table.getModel().setValueAt(firstname, i, 0);
		            table.getModel().setValueAt(lastname, i, 1);
		           table.getModel().setValueAt(address, i, 2);
		          table.getModel().setValueAt(contactnum, i, 3); 
		          
		         
		        i++;
		        }
				
			}catch(SQLException e1)
			{
				e1.printStackTrace();
			}
	}
	
	public void getResetButtonExistingbutton()
	{
		firstnamesearchexistingbutton.setText("");
		lastnamesearchexistingbutton.setText("");
		
		
		try
		{
			String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
			PreparedStatement pst2 = connection.prepareStatement(query);
			ResultSet rs2 = pst2.executeQuery();
			
			if(rowcount != 0)
			{
				rowcount = 0;
				while(rs2.next())
				{
				    rowcount ++;
				}
			}
			else
			{
				while(rs2.next())
				{
				    rowcount ++;
				}
			}
			
			
		}catch(SQLException e1)
		{
			e1.printStackTrace();
		}
		
		table.setModel(new DefaultTableModel(
				new Object[rowcount][rowcount] ,
				new String[] {
					"FIRSTNAME", "LASTNAME", "ADDRESS", "CONTACTNO."
				}
			) {
				Class[] columnTypes = new Class[] {
					Object.class, Object.class, String.class, Object.class
				};
				public Class getColumnClass(int columnIndex) {
					return columnTypes[columnIndex];
				}
				boolean[] columnEditables = new boolean[] {
					false, false, false
				};
				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
			});
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setPreferredWidth(200);
		table.getColumnModel().getColumn(1).setResizable(false);
		table.getColumnModel().getColumn(2).setResizable(false);
		table.getColumnModel().getColumn(2).setPreferredWidth(300);
		table.getColumnModel().getColumn(3).setResizable(false);
		
	
		
			
			try
			{
			
				
				String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
				PreparedStatement pst2 = connection.prepareStatement(query);
				ResultSet rs2 = pst2.executeQuery();
				int i=0;
				while(rs2.next()) {
		           
		         
		            Object firstname = rs2.getString("FIRSTNAME");
		            Object lastname = rs2.getString("LASTNAME");
		            Object address = rs2.getString("ADDRESS");
		            Object contactnum = rs2.getString("CONTACTNO"); 
		             
		            table.getModel().setValueAt(firstname, i, 0);
		            table.getModel().setValueAt(lastname, i, 1);
		           table.getModel().setValueAt(address, i, 2);
		          table.getModel().setValueAt(contactnum, i, 3); 
		          
		         
		        i++;
		        }
				
			}catch(SQLException e1)
			{
				e1.printStackTrace();
			}
	}
	
	public void getDONEbuttonexistingbutton()
	{
		firstnamesearchexistingbutton.setText("");
		lastnamesearchexistingbutton.setText("");
		
		btnNewCostumer.setVisible(true);
		existingbutton.setVisible(true);
		user.setEnabled(true);
		ADDDEBT.setEnabled(true);
		VIEW.setEnabled(true);	
		BILLOUT.setEnabled(true);	
		ABOUTUS.setEnabled(true);

		scrollPane.setVisible(false);
		Search.setVisible(false);
		RESET.setVisible(false);
		DONE.setVisible(false);
		CANCEL.setVisible(false);
		LISTOFCUSTOMERS.setVisible(false);
		firstnamesearchexistingbutton.setVisible(false);
		lastnamesearchexistingbutton.setVisible(false);
		firstnameexistingbutton.setVisible(false);
		lastnamexistingbutton.setVisible(false);
		saerchcustomerexisting.setVisible(false);
	}
	
	public void getCANCELbuttonexistingbutton()
	{
		firstnamesearchexistingbutton.setText("");
		lastnamesearchexistingbutton.setText("");
		
		btnNewCostumer.setVisible(true);
		existingbutton.setVisible(true);
		user.setEnabled(true);
		ADDDEBT.setEnabled(true);
		VIEW.setEnabled(true);	
		BILLOUT.setEnabled(true);	
		ABOUTUS.setEnabled(true);
		
		scrollPane.setVisible(false);
		Search.setVisible(false);
		DONE.setVisible(false);
		RESET.setVisible(false);
		CANCEL.setVisible(false);
		LISTOFCUSTOMERS.setVisible(false);
		firstnamesearchexistingbutton.setVisible(false);
		lastnamesearchexistingbutton.setVisible(false);
		firstnameexistingbutton.setVisible(false);
		lastnamexistingbutton.setVisible(false);
		saerchcustomerexisting.setVisible(false);
	}
	
	public void getaddebitcancelAction()
	{
		scrollPane.setVisible(true);
		Search.setVisible(true);
		DONE.setVisible(true);
		RESET.setVisible(true);
		CANCEL.setVisible(true);
		LISTOFCUSTOMERS.setVisible(true);
		firstnamesearchexistingbutton.setVisible(true);
		lastnamesearchexistingbutton.setVisible(true);
		firstnameexistingbutton.setVisible(true);
		lastnamexistingbutton.setVisible(true);
		saerchcustomerexisting.setVisible(true);
		
		
		adddebitpersonalinfo.setVisible(false);
		adddebitfirstname.setVisible(false);
		adddebitlastname.setVisible(false);
		adddebitaddress.setVisible(false);
		adddebitcontact.setVisible(false);
		addebitfirstnametext.setVisible(false);
		adddebitlastnametext.setVisible(false);
		adddebitaddresstext.setVisible(false);
		adddebitcontacttext.setVisible(false);
	 	adddebitdebtinformation.setVisible(false);
	 	addebitcancel.setVisible(false);
	 	ADDDEBITREFRESH.setVisible(false);
	 	ADDDEBITDEBTINFOTABLE.setVisible(false);
	 	YESBUTTONFORADDINGDEBT.setVisible(false);
	 	QUESTIONFORADDINGDEBT.setVisible(false);
	}
	
	public void getViewSearchActionButton()
	{
		search.setFirstname(Viewfirstnamefield.getText().toUpperCase());
		search.setLastname(Viewlastnamefield.getText().toUpperCase());
		String con1 = "false", con2 = "false", con3 = "false";
		
		if((search.getFirstname().length() != 0) && (search.getLastname().length() != 0))
		{
			con1 = "true";
		}
		else if((search.getFirstname().length() != 0) && (search.getLastname().length() == 0))
		{
			con2 = "true";
		}
		else if((search.getFirstname().length() == 0) && (search.getLastname().length() != 0))
		{
			con3 = "true";
		}
		
		
		
		if(con1 == "true" )
		{
			try
			{
				String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
						+ search.getFirstname() + "' AND lastname = '" 
						+ search.getLastname()  + "'" ;
				PreparedStatement pst2 = connection.prepareStatement(query);
				ResultSet rs2 = pst2.executeQuery();
				if(rowcount != 0)
				{
					rowcount = 0;
					while(rs2.next())
					{
					    rowcount ++;
					}
				}
				else
				{
					while(rs2.next())
					{
					    rowcount ++;
					}
				}
				
				
			}catch(SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		else if( con2 == "true")
		{
			try
			{
				String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
						+ search.getFirstname() + "'";
						 
				PreparedStatement pst2 = connection.prepareStatement(query);
				ResultSet rs2 = pst2.executeQuery();
				if(rowcount != 0)
				{
					rowcount = 0;
					while(rs2.next())
					{
					    rowcount ++;
					}
				}
				else
				{
					while(rs2.next())
					{
					    rowcount ++;
					}
				}
				
				
			}catch(SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		
		else if(con3 == "true")
		{
			try
			{
				String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE lastname = '" 
						+ search.getLastname() + "'";
				PreparedStatement pst2 = connection.prepareStatement(query);
				ResultSet rs2 = pst2.executeQuery();
				if(rowcount != 0)
				{
					rowcount = 0;
					while(rs2.next())
					{
					    rowcount ++;
					}
				}
				else
				{
					while(rs2.next())
					{
					    rowcount ++;
					}
				}
				
				
			}catch(SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		else
		{
			try
			{
				String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
				PreparedStatement pst2 = connection.prepareStatement(query);
				ResultSet rs2 = pst2.executeQuery();
				
				if(rowcount != 0)
				{
					rowcount = 0;
					while(rs2.next())
					{
					    rowcount ++;
					}
				}
				else
				{
					while(rs2.next())
					{
					    rowcount ++;
					}
				}
				
				
			}catch(SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		
		
		
		
		
		Viewtable.setModel(new DefaultTableModel(
				new Object[rowcount][rowcount] ,
				new String[] {
					"FIRSTNAME", "LASTNAME", "ADDRESS", "CONTACTNO."
				}
			) {
				Class[] columnTypes = new Class[] {
					Object.class, Object.class, String.class, Object.class
				};
				public Class getColumnClass(int columnIndex) {
					return columnTypes[columnIndex];
				}
				boolean[] columnEditables = new boolean[] {
					false, false, false
				};
				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
			});
		Viewtable.getColumnModel().getColumn(0).setResizable(false);
		Viewtable.getColumnModel().getColumn(0).setPreferredWidth(200);
		Viewtable.getColumnModel().getColumn(1).setResizable(false);
		Viewtable.getColumnModel().getColumn(2).setResizable(false);
		Viewtable.getColumnModel().getColumn(2).setPreferredWidth(300);
		Viewtable.getColumnModel().getColumn(3).setResizable(false);
		
		try
		{
		
			if(con1 == "true")
			{
				String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
						+ search.getFirstname() + "' AND lastname = '" 
						+ search.getLastname()  + "'" ;
		
		
				PreparedStatement pst3 = connection.prepareStatement(query);
				ResultSet rs3 = pst3.executeQuery();
		
				if(!rs3.next())
				{
					JOptionPane.showMessageDialog(null, "NO COSTUMER FOUND!");
				}
				else
				{
					String query1 = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
									+ search.getFirstname() + "' AND lastname = '" 
									+ search.getLastname()  + "'"  ;
	
	
					PreparedStatement pst4 = connection.prepareStatement(query1);
					ResultSet rs4 = pst4.executeQuery();
					int i=0;
					while(rs4.next()) 
					{
				           
				         
			            Object firstname = rs4.getString("FIRSTNAME");
			            Object lastname = rs4.getString("LASTNAME");
			            Object address = rs4.getString("ADDRESS");
			            Object cantctnum = rs4.getString("CONTACTNO"); 
			             
			            Viewtable.getModel().setValueAt(firstname, i, 0);
			            Viewtable.getModel().setValueAt(lastname, i, 1);
			           Viewtable.getModel().setValueAt(address, i, 2);
			          Viewtable.getModel().setValueAt(cantctnum, i, 3); 
			          
			         
			        i++;
					}

				}
			}	
			else if(con2 == "true")
			{
				String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
						+ search.getFirstname() + "'";
						 
				PreparedStatement pst3 = connection.prepareStatement(query);
				ResultSet rs3 = pst3.executeQuery();
				
				if(!rs3.next())
				{
					JOptionPane.showMessageDialog(null, "NO COSTUMER FOUND!");
				}
				else
				{
					String query1 = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE firstname = '" 
									+ search.getFirstname() + "'"  ;
	
	
					PreparedStatement pst4 = connection.prepareStatement(query1);
					ResultSet rs4 = pst4.executeQuery();
					int i=0;
					while(rs4.next()) 
					{
				           
				         
			            Object firstname = rs4.getString("FIRSTNAME");
			            Object lastname = rs4.getString("LASTNAME");
			            Object address = rs4.getString("ADDRESS");
			            Object cantctnum = rs4.getString("CONTACTNO"); 
			             
			            Viewtable.getModel().setValueAt(firstname, i, 0);
			            Viewtable.getModel().setValueAt(lastname, i, 1);
			           Viewtable.getModel().setValueAt(address, i, 2);
			          Viewtable.getModel().setValueAt(cantctnum, i, 3); 
			          
			         
			        i++;
					}
				}
			}	
			else if(con3 == "true")
			{
				String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE lastname = '" 
						+ search.getLastname() + "'";
				PreparedStatement pst3 = connection.prepareStatement(query);
				ResultSet rs3 = pst3.executeQuery();
				
				if(!rs3.next())
				{
					JOptionPane.showMessageDialog(null, "NO COSTUMER FOUND!");
				}
				else
				{
					String query1 = "SELECT firstname,lastname,address,contactno FROM CUSTOMER WHERE lastname = '" 
									+ search.getLastname() + "'"  ;
	
	
					PreparedStatement pst4 = connection.prepareStatement(query1);
					ResultSet rs4 = pst4.executeQuery();
					int i=0;
					while(rs4.next()) 
					{
				           
				         
			            Object firstname = rs4.getString("FIRSTNAME");
			            Object lastname = rs4.getString("LASTNAME");
			            Object address = rs4.getString("ADDRESS");
			            Object cantctnum = rs4.getString("CONTACTNO"); 
			             
			            Viewtable.getModel().setValueAt(firstname, i, 0);
			            Viewtable.getModel().setValueAt(lastname, i, 1);
			           Viewtable.getModel().setValueAt(address, i, 2);
			          Viewtable.getModel().setValueAt(cantctnum, i, 3); 
			          
			         
			        i++;
					}
				}
			}
			else
			{
				try
				{
				
					
					String query = "SELECT firstname,lastname,address,contactno FROM CUSTOMER" ;
					PreparedStatement pst2 = connection.prepareStatement(query);
					ResultSet rs2 = pst2.executeQuery();
					int i=0;
					while(rs2.next()) {
			           
			         
			            Object firstname = rs2.getString("FIRSTNAME");
			            Object lastname = rs2.getString("LASTNAME");
			            Object address = rs2.getString("ADDRESS");
			            Object contactnum = rs2.getString("CONTACTNO"); 
			             
			            Viewtable.getModel().setValueAt(firstname, i, 0);
			            Viewtable.getModel().setValueAt(lastname, i, 1);
			           Viewtable.getModel().setValueAt(address, i, 2);
			          Viewtable.getModel().setValueAt(contactnum, i, 3); 
			          
			         
			        i++;
			        }
					
				}catch(SQLException e1)
				{
					e1.printStackTrace();
				}
			}
							
				
			
		}catch(SQLException e1)
		{
			e1.printStackTrace();
		}
	}
}

		

