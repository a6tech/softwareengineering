package controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;

import Model.AdministratorModel;
import Model.CustomerModel;
import Model.LoginModel;
import Model.customer.Customer;
import database.SqliteConnection;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class LoginController 
{
	@FXML private Button loginExitButton;
	@FXML private AnchorPane loginPanel;
	@FXML private AnchorPane customerloginPanel;
	@FXML private AnchorPane administratorloginPanel;
	@FXML private Button adminloginchoosebutton;
	@FXML private Button customerloginchoosebutton;
	@FXML private Button adminloginbutton;
	@FXML private Button customerloginbutton;
	@FXML private Button admincancelbutton;
	@FXML private Button customercancelbutton;
	@FXML private Button scanqrcodebutton;
	@FXML private TextField usernamelogintxtfield;
	@FXML private PasswordField passwordlogintxtfield;
	@FXML private ImageView scanqrcodeimage;
	AdministratorModel admin = new AdministratorModel();
	LoginModel login = new LoginModel();
	CustomerController customercontrol = new CustomerController();
	CustomerModel customerframe = new CustomerModel();
	Customer customer = new Customer();
	private int counter = 0;
	private String qrcodepathfile;
	private Connection connection;
	private String adminusername = "";
	private String adminpassword = "";
	private String admininputusername = "";
	private String admininputpassword = "";
	private File QRcodefile;
	
	public void AdminLoginChooseButtonClicked() 
	{
		 loginPanel.setVisible(false);
		 administratorloginPanel.setVisible(true);
	
    }
	
	public void CustomerLoginChooseButtonClicked() 
	{
		loginPanel.setVisible(false);
	    customerloginPanel.setVisible(true);
	    
    }
	
	public void AdminCancelButtonClicked()
	{
		 administratorloginPanel.setVisible(false);
		 loginPanel.setVisible(true);
		 usernamelogintxtfield.clear();
		 passwordlogintxtfield.clear();
    }

	public void CustomerCanceleButtonClicked() 
	{
		customerloginPanel.setVisible(false);
		 loginPanel.setVisible(true);
		 QRcodefile = null;
		 Image imagereset = new Image("/images/searcjQRcode.png");
		 scanqrcodeimage.setImage(imagereset);
    }
	
	@FXML
	public void AdminLoginButtonClicked() 
	{
		connection = SqliteConnection.Connector();
		String missinginput = "true";
		if(usernamelogintxtfield.getText().length() == 0 || passwordlogintxtfield.getText().length() == 0 )
		{
			 missinginput = "true";
		}
		else if(usernamelogintxtfield.getText().length() != 0 && passwordlogintxtfield.getText().length() != 0)
		{
			missinginput = "false";
		}
		
		
		if(missinginput.equals("false"))
		{
		 try
			{
				String query1 = "SELECT UserName,Password FROM AdminLogin";
				PreparedStatement pst = connection.prepareStatement(query1);
				ResultSet resultset = pst.executeQuery();
				while(resultset.next()) 
				{	         
			        			adminusername = resultset.getString("UserName");
			        			adminpassword = resultset.getString("Password");
				}
				 pst.close();
				 resultset.close();
				connection.close();
			}catch(SQLException e1)
			{
				e1.printStackTrace();
			}
		 	
		 admininputusername = usernamelogintxtfield.getText();
		 admininputpassword = passwordlogintxtfield.getText();
		 
		 if(admininputusername.equals(adminusername) && admininputpassword.equals(adminpassword))
		 {
			 usernamelogintxtfield.clear();
			 passwordlogintxtfield.clear();
			 if(counter == 0)
			 {
			 login.closeLogin();
			 admin.start(AdministratorModel.adminStage);
			 counter++;
			 }
			 else
			 {
				 login.closeLogin();
				 admin.openAdmin();
			 }
		 }
		 else
		 {
			 Alert alert = new Alert(AlertType.WARNING);
			 alert.setTitle("Invalid Username and Password");
			 alert.setHeaderText(null);
			 alert.setContentText("Invalid Username and Password");
			 alert.initStyle(StageStyle.UTILITY);
			 alert.showAndWait();
		 }
		}
		else
		{
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Missing Input");
			alert.setHeaderText(null);
			alert.setContentText("Please Type both Username and Password");
			alert.initStyle(StageStyle.UTILITY);
			alert.showAndWait();
		}
    }
	
	public void loginExitButtonClicked(){
		login.closeLogin();
	} 
	
	@FXML
	private void adminLoginPressedEnter(KeyEvent event){
	    if(event.getCode() == KeyCode.ENTER)
	    {
	    	connection = SqliteConnection.Connector();
			String missinginput = "true";
			if(usernamelogintxtfield.getText().length() == 0 || passwordlogintxtfield.getText().length() == 0 )
			{
				 missinginput = "true";
			}
			else if(usernamelogintxtfield.getText().length() != 0 && passwordlogintxtfield.getText().length() != 0)
			{
				missinginput = "false";
			}
			
			
			if(missinginput.equals("false"))
			{
			 try
				{
					String query1 = "SELECT UserName,Password FROM AdminLogin";
					PreparedStatement pst = connection.prepareStatement(query1);
					ResultSet resultset = pst.executeQuery();
					while(resultset.next()) 
					{	         
				        			adminusername = resultset.getString("UserName");
				        			adminpassword = resultset.getString("Password");
					}
					 pst.close();
					 resultset.close();
					connection.close();
				}catch(SQLException e1)
				{
					e1.printStackTrace();
				}
			 	
			 admininputusername = usernamelogintxtfield.getText();
			 admininputpassword = passwordlogintxtfield.getText();
			 
			 if(admininputusername.equals(adminusername) && admininputpassword.equals(adminpassword))
			 {
				 usernamelogintxtfield.clear();
				 passwordlogintxtfield.clear();
				 if(counter == 0)
				 {
				 login.closeLogin();
				 admin.start(AdministratorModel.adminStage);
				 counter++;
				 }
				 else
				 {
					 login.closeLogin();
					 admin.openAdmin();
				 }
			 }
			 else
			 {
				 Alert alert = new Alert(AlertType.WARNING);
				 alert.setTitle("Invalid Username and Password");
				 alert.setHeaderText(null);
				 alert.setContentText("Invalid Username and Password");
				 alert.initStyle(StageStyle.UTILITY);
				 alert.showAndWait();
			 }
			}
			else
			{
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Missing Input");
				alert.setHeaderText(null);
				alert.setContentText("Please Type both Username and Password");
				alert.initStyle(StageStyle.UTILITY);
				alert.showAndWait();
			}
	    }
	}
	
	public void ScanQRCodeButtonClicked()
	{

		customerloginbutton.setDisable(true);
		scanqrcodebutton.setDisable(true);
		customercancelbutton.setDisable(true);
		File fileholder = null;
		FileChooser chooser = new FileChooser();
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.jpg");
		chooser.getExtensionFilters().add(extFilter);
	    chooser.setTitle("Open QRCustomerCode");
	    QRcodefile = chooser.showOpenDialog(new Stage());
		customerloginbutton.setDisable(false);
		scanqrcodebutton.setDisable(false);
		customercancelbutton.setDisable(false);
	    if (fileholder != QRcodefile) 
	    {
	    	try {
				Image image = new Image(QRcodefile.toURI().toURL().toString());
				scanqrcodeimage.setImage(image);
				String qrcodepathfileholder = QRcodefile.getAbsolutePath();
				qrcodepathfile = qrcodepathfileholder.replace("\\", "\\\\");
				fileholder = QRcodefile;
				
			} catch (MalformedURLException e) {
				
				e.printStackTrace();
			}
	    }
	   
	    
	   
	}
	
	public void LoginQRCodeButtonClicked() throws NullPointerException
	{
		if(QRcodefile == null)
		{
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Invalid Scan QRCode");
			alert.setHeaderText(null);
			alert.setContentText("Please Scan QRCODE first");
			alert.showAndWait();
		}
		else
		{
		
			try
			{
				String qrcode = readQRCode(qrcodepathfile);
			    customer.setCustomerid(Integer.parseInt(qrcode));
			    
			    connection = SqliteConnection.Connector();
				try
				{
					String query = "SELECT CUSTOMERID FROM CUSTOMER where CUSTOMERID = '" + customer.getCustomerid() + "'";
					PreparedStatement pst2 = connection.prepareStatement(query);
					ResultSet rs2 = pst2.executeQuery();
					
					if(!rs2.next())
					{
						Alert alert = new Alert(AlertType.WARNING);
						alert.setTitle("Not Existing Customer");
						alert.setHeaderText(null);
						alert.setContentText("Customer not Exist");
						alert.showAndWait();
			        }
					else
					{
							if(counter == 0)
							 {
							 login.closeLogin();
							 customerframe.start(CustomerModel.customerStage);
							 counter++;
							 }
							 else
							 {
								 login.closeLogin();
								 customerframe.openCustomer();
							 }
					}
					 pst2.close();
					 connection.close();
				}catch(SQLException e1)
				{
					e1.printStackTrace();
				}
			}
			catch(RuntimeException e)
			{
				    Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Invalid Selected QRCode");
					alert.setHeaderText(null);
					alert.setContentText("Please select valid QRCODE");
					alert.showAndWait();
			}
		}
		
	}
	
	private static String readQRCode(String fileName) 
	{
	    File file = new File(fileName);
	    BufferedImage image = null;
	    BinaryBitmap bitmap = null;
	    Result result = null;
	    
	    try {
	        image = ImageIO.read(file);
	        int[] pixels = image.getRGB(0, 0, image.getWidth(), image.getHeight(), null, 0, image.getWidth());
	        RGBLuminanceSource source = new RGBLuminanceSource(image.getWidth(), image.getHeight(), pixels);
	        bitmap = new BinaryBitmap(new HybridBinarizer(source));
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	 
	    if (bitmap == null)
	        return null;
	 
	    QRCodeReader reader = new QRCodeReader();   
	    try {
	        result = reader.decode(bitmap);
	        return result.getText();
	    } catch (NotFoundException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    } catch (ChecksumException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    } catch (FormatException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }
	 
	    return null;
	}
	
}
