package GUI;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import GUI.UI.JTextFieldLimit;
import controller.Controler;
import controller.db2connection;
import entities.ADMINISTRATOR;

public class loginFrame 
{
	public JFrame logInFrame = new JFrame();
	public JTextField textlog = new JTextField();
	public JLabel lblLog_user = new JLabel("Username:");
	public JLabel lblLog_pass = new JLabel("Password:");
	public JPasswordField passwordField = new JPasswordField();
	public JButton btnLogIn = new JButton("LOGIN");
	public JButton btncancel = new JButton("EXIT");
	public JLabel lblLogin_welcome = new JLabel("");
	UI u = new UI();
	ADMINISTRATOR a = new ADMINISTRATOR();
	
	public loginFrame()
	{
		logInFrame.setTitle("LogIn");
		logInFrame.getContentPane().setLayout(null);
		logInFrame.setBounds(100, 100, 368, 254);
		logInFrame.getRootPane().setDefaultButton(btnLogIn);
		logInFrame.setLocationRelativeTo(null);
		logInFrame.setUndecorated(true);
		logInFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		logInFrame.setBackground(SystemColor.desktop);
		logInFrame.setForeground(SystemColor.desktop);
		
		
		textlog.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		textlog.setForeground(SystemColor.desktop);
		textlog.setToolTipText("");
		textlog.setBounds(97, 100, 244, 34);
		textlog.setBackground(Color.WHITE);
		logInFrame.getContentPane().add(textlog);
		textlog.setColumns(10);
		textlog.setDocument(new JTextFieldLimit(10));
		
		
		lblLog_user.setForeground(SystemColor.desktop);
		lblLog_user.setBounds(28, 111, 69, 14);
		lblLog_user.setBackground(SystemColor.desktop);
		lblLog_user.setFont(new Font("Franklin Gothic Book", Font.BOLD, 11));
		logInFrame.getContentPane().add(lblLog_user);
		
		lblLog_pass.setFont(new Font("Franklin Gothic Book", Font.BOLD, 11));
		lblLog_pass.setForeground(SystemColor.desktop);
		lblLog_pass.setBackground(SystemColor.controlLtHighlight);
		lblLog_pass.setBounds(30, 156, 77, 14);
		logInFrame.getContentPane().add(lblLog_pass);
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 13));
		passwordField.setForeground(SystemColor.desktop);
		passwordField.setBackground(Color.WHITE);
		passwordField.setColumns(10);
		passwordField.setBounds(97, 145, 244, 34);
		logInFrame.getContentPane().add(passwordField);
		passwordField.setDocument(new JTextFieldLimit(10));
		
	
		btnLogIn.setForeground(SystemColor.desktop);
		btnLogIn.setFont(new Font("Franklin Gothic Medium Cond", Font.BOLD, 12));
		btnLogIn.setBounds(28, 201, 114, 34);
		logInFrame.getContentPane().add(btnLogIn);

		btncancel.setForeground(SystemColor.desktop);
		btncancel.setFont(new Font("Franklin Gothic Medium Cond", Font.BOLD, 12));
		btncancel.setBounds(227, 201, 114, 34);
		logInFrame.getContentPane().add(btncancel);
	
		
		
		lblLogin_welcome.setIcon(new ImageIcon("C:\\Users\\doms\\Documents\\need files\\dszabalajr\\GUI\\images\\FB_IMG_1456030201858.jpg"));
		lblLogin_welcome.setBounds(116, 18, 150, 71);
		logInFrame.getContentPane().add(lblLogin_welcome);
		
		btnLogIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				a.setUser(textlog.getText());
				a.setPassword(passwordField.getText());
				Connection connection = db2connection.dbConnection();
				
				if(a.isConnectioncondition() == true)
				{
					
					u.frame.setVisible(true);
					logInFrame.setVisible(false);
					Controler c = new Controler();
					
				}
					
					else{
						Component controllingFrame = null;
						JOptionPane.showMessageDialog(controllingFrame, 
								"Invalid Password or Username",
								"Error Message",
								JOptionPane.ERROR_MESSAGE);
						textlog.setText(null);
						passwordField.setText(null);
					}
			}
		});
		
		btncancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				System.exit(0);

			}
		});
		
		
	}
	
	public void runloginFrame()
	{
		logInFrame.setVisible(true);
	}
	
	
	public class JTextFieldLimit extends PlainDocument {
		  private int limit;
		  JTextFieldLimit(int limit) {
		    super();
		    this.limit = limit;
		  }

		  JTextFieldLimit(int limit, boolean upper) {
		    super();
		    this.limit = limit;
		  }

		  public void insertString(int offset, String str, javax.swing.text.AttributeSet attr) throws BadLocationException {
		    if (str == null)
		      return;
	
		    
		    if ((getLength() + str.length()) <= limit) {
		      super.insertString(offset, str, attr);
		    }
		  }
		}
}
