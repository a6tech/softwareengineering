package entities;

public class ADMINISTRATOR 
{
	private String user;
	private String password;
	private boolean connectioncondition  = true;
	public String getUser() {
		return user;
	}
	public boolean isConnectioncondition() {
		return connectioncondition;
	}
	public void setConnectioncondition(boolean connectioncondition) {
		this.connectioncondition = connectioncondition;
	}
	public void setUser(String user) 
	{
		this.user = user;
	}
	public String getPassword() 
	{
		return password;
	}
	public void setPassword(String password) 
	{
		this.password = password;
	}
	
}
